<?php
/// Set the slider

$slider_page_id = $post->ID;
if(is_home() && !is_front_page()){
	$slider_page_id = get_option('page_for_posts');
}

if(get_post_meta($slider_page_id, 'rd_slider_type', true) == 'layer' && (get_post_meta($slider_page_id, 'rd_slider', true) || get_post_meta($slider_page_id, 'rd_slider', true) != 0)){

	function add_revolution_slider(){
		echo '<div>';
	    echo do_shortcode('[rev_slider '.get_post_meta(get_the_ID(), 'rd_slider', true).']');
		echo '</div>';
	}

	if(	get_post_meta($slider_page_id, 'rd_slider_position', true) == 'above'){
		add_action( '__before_header' , 'add_revolution_slider');
	}
	else{
		add_action( '__after_page_title' , 'add_revolution_slider');
	}


}
if(get_post_meta($slider_page_id, 'rd_slider_type', true) == 'layerslider' && (get_post_meta($slider_page_id, 'rd_layerslider', true) || get_post_meta($slider_page_id, 'rd_layerslider', true) != 0)){

	function add_layer_slider(){
		echo '<div>';
	    echo do_shortcode('[layerslider  id="'.get_post_meta(get_the_ID(), 'rd_layerslider', true).'"]');
		echo '</div>';
	}

	if(	get_post_meta($slider_page_id, 'rd_slider_position', true) == 'above'){
		add_action( '__before_header' , 'add_layer_slider');
	}
	else{
		add_action( '__after_page_title' , 'add_layer_slider');
	}


}


get_header();
global $rd_data;
$header_top_bar = get_post_meta( $post->ID, 'rd_top_bar', true );
$header_transparent = get_post_meta( $post->ID, 'rd_header_transparent', true );
$title = get_post_meta($post->ID, 'rd_title', true);
$title_height = get_post_meta($post->ID, 'rd_title_height', true);
$title_color = get_post_meta($post->ID, 'rd_title_color', true);
$titlebg_color = get_post_meta($post->ID, 'rd_titlebg_color', true);
	$ctbg = get_post_meta($post->ID, 'rd_ctbg', true);
	$bc = get_post_meta($post->ID, 'rd_bc', true);
	$home = __('Home', 'thefoxwp'); // text for the 'Home' link
	$mc_bg_color = $rd_data['rd_content_bg_color'];
	$mc_heading_color = $rd_data['rd_content_heading_color'];
	$mc_text_color = $rd_data['rd_content_text_color'];
	$mc_hl_color = $rd_data['rd_content_hl_color'];
	$mc_hover_color = $rd_data['rd_content_hover_color'];
	$mc_light_hover_color = $rd_data['rd_content_light_hover_color'];
	$mc_border_color = $rd_data['rd_content_border_color'];

$home = __('Home', 'thefoxwp'); // text for the 'Home' link
$generated_section = get_post_meta($post->ID, 'rd_generated_section', true);
$facebook = get_post_meta($post->ID, 'rd_facebook', true);
$twitter = get_post_meta($post->ID, 'rd_twitter', true);
$linkedin = get_post_meta($post->ID, 'rd_linkedin', true);
$tumblr = get_post_meta($post->ID, 'rd_tumblr', true);
$gplus = get_post_meta($post->ID, 'rd_gplus', true);
$mail = get_post_meta($post->ID, 'rd_mail', true);
$skype = get_post_meta($post->ID, 'rd_skype', true);
$Pinterest= get_post_meta($post->ID, 'rd_pinterest', true);
$vimeo = get_post_meta($post->ID, 'rd_vimeo', true);
$youtube = get_post_meta($post->ID, 'rd_youtube', true);
$dribbble = get_post_meta($post->ID, 'rd_dribbble', true);
$deviantart = get_post_meta($post->ID, 'rd_deviantart', true);
$reddit = get_post_meta($post->ID, 'rd_reddit', true);
$behance = get_post_meta($post->ID, 'rd_behance', true);
$digg = get_post_meta($post->ID, 'rd_digg', true);
$flickr = get_post_meta($post->ID, 'rd_flickr', true);
$instagram = get_post_meta($post->ID, 'rd_instagram', true);
$position = get_post_meta($post->ID, 'rd_position', true);
$member_desc = get_post_meta($post->ID, 'rd_small_desc', true);
$real_name = get_post_meta($post->ID, 'rd_real_name', true);
$phone = get_post_meta($post->ID, 'rd_phone', true);
$member_url = get_post_meta($post->ID, 'rd_member_url', true);
$skills = get_post_meta($post->ID, 'rd_skills', true);




/// Check if need to hide header top bar

if ($header_top_bar == 'yes' ){

 echo '<style>#top_bar {display:none;}</style>';

}



/// Check if header is transparent

if( ( $rd_data['rd_nav_type'] == 'nav_type_1' && $header_transparent == "yes" || $rd_data['rd_nav_type'] == 'nav_type_2' && $header_transparent == "yes" || $rd_data['rd_nav_type'] == 'nav_type_3' && $header_transparent == "yes" || $rd_data['rd_nav_type'] == 'nav_type_8' && $header_transparent == "yes" || $rd_data['rd_nav_type'] == 'nav_type_9' || $rd_data['rd_nav_type'] == 'nav_type_9_c' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 90;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 133;
}
}
if( ( $rd_data['rd_nav_type'] == 'nav_type_10' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 91;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 134;
}
}

if($rd_data['rd_nav_type'] == 'nav_type_4' && $header_transparent == "yes" ){


if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 101;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 144;
}

}
if(($rd_data['rd_nav_type'] == 'nav_type_5' || $rd_data['rd_nav_type'] == 'nav_type_6' || $rd_data['rd_nav_type'] == 'nav_type_7' || $rd_data['rd_nav_type'] == 'nav_type_12'  ) && $header_transparent == "yes"){


if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 100;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 143;
}
}


if( ( $rd_data['rd_nav_type'] == 'nav_type_10' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 91;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 134;
}
}


if( ( $rd_data['rd_nav_type'] == 'nav_type_11' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 110;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 153;
}
}


if( ( $rd_data['rd_nav_type'] == 'nav_type_13' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 62;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 105;
}
}


if( ( $rd_data['rd_nav_type'] == 'nav_type_14' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 65;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 108;
}
}

if( ( $rd_data['rd_nav_type'] == 'nav_type_15' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 140;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 183;
}
}if( ( $rd_data['rd_nav_type'] == 'nav_type_16' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 160;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 203;
}
}if( ( $rd_data['rd_nav_type'] == 'nav_type_17' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 159;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 202;
}
}if( ( $rd_data['rd_nav_type'] == 'nav_type_18' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 162;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 205;
}
}
if( ( $rd_data['rd_nav_type'] == 'nav_type_19' ) && $header_transparent == "yes"){

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom + 162;

}else{
		$title_padding_bottom = 43;
	$title_padding_top = 43;
}
}


if($header_transparent == "yes" && $rd_data['rd_nav_type'] !== 'nav_type_19' && $rd_data['rd_nav_type'] !== 'nav_type_19_f' ){
 ?>
<script type="text/javascript" charset="utf-8">
		var j$ = jQuery;
		j$.noConflict();
		"use strict";


		j$('#header_container').css('position', 'absolute');
		j$('#header_container').css('width', '100%');
		j$('header').addClass('transparent_header');
		j$('.header_bottom_nav').addClass('transparent_header');

</script>

<?php

}else {

if($title_height !== ''){

	$title_padding_bottom = $title_height/2;
	$title_padding_top = $title_padding_bottom;
}else{

	$title_padding_bottom = $title_padding_top = 43;

}
}

/// Set title height
echo '<style>.page_title_ctn {padding-top:'.$title_padding_top.'px; padding-bottom:'.$title_padding_bottom.'px;}</style>';

/// Set the title color
if($title_color !== ''){
	$rgba= rd_hex_to_rgb_array($title_color);
	echo '<style>.page_title_ctn h1,.page_title_ctn h2,#crumbs,#crumbs a{color:'.$title_color.';}.page_t_boxed h1,.page_t_boxed h1{border-color:rgba('. $rgba[0].','.$rgba[1].','.$rgba[2] .',0.5); }#crumbs span{color:rgba('. $rgba[0].','.$rgba[1].','.$rgba[2] .',0.8);}</style>';
}
/// Set the title background
if($titlebg_color !== ''){
	echo '<style>.page_title_ctn {background:'.$titlebg_color.'; }</style>';
}
if($ctbg !== ''){
	echo '<style>.page_title_ctn{background:url('.$ctbg.') top center; background-size: cover; border-bottom:1px solid '.$content_border_color.'; }</style>';
}
/// Check title style
/// Check title style
if($title !== 'no'){  ?>
<div class="page_title_ctn">
  <div class="wrapper table_wrapper">
  <h1><?php the_title(); ?></h1>
  <?php if($bc !== 'no') { echo wpb_list_child_pages();  ?>
<div id="breadcrumbs">
    <div id="crumbs"><a href="<?php echo home_url(''); ?>">
      <?php echo !empty( $home ) ? $home : ''; ?>
      </a><i class="fa-angle-right crumbs_delimiter"></i><a href="<?php if($rd_data['rd_bc_stafflink']){echo esc_url($rd_data['rd_bc_stafflink']);} ?>">
      <?php if($rd_data['rd_bc_porttext']){echo !empty( $rd_data['rd_bc_stafftext'] ) ? $rd_data['rd_bc_stafftext'] : '';} ?>
      </a><i class="fa-angle-right crumbs_delimiter"></i><span>
      <?php the_title(); ?>
      </span></div>
  </div>
<?php } ?>
</div>
</div>
<?php }

do_action( '__after_page_title' );

?>






<div class="section def_section">
<div class="wrapper <?php if($generated_section !== 'no'){ echo 'staff_single_page'; } ?> section_wrapper">
    <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post();

if($generated_section !== 'no'){ ?>


	<?php
	$post_id = get_the_ID();
	$user_id = get_post_field('post_author', $post_id);
	$user_obj = get_user_by( 'id', $user_id );
	$metadatas = get_post_custom($tanarok_cpt_post_id);
	$tel = get_user_meta($user_obj->ID, 'phone', true);

	$hany_perces_orakat_tartasz = $metadatas['hany_perces_orakat_tartasz'][0];
	$mennyibe_kerul_egy_ilyen_alkalom = $metadatas['mennyibe_kerul_egy_ilyen_alkalom'][0];
	$keres_eseten_hazhoz_mesz = $metadatas['keres_eseten_hazhoz_mesz'][0];
	$tartasz_orat_online = $metadatas['tartasz_orat_online'][0];


	// Tags
	$args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
	$helysegek_kateg = wp_get_post_terms($post_id, 'helysegek_kateg', $args);
	$tantargyak_kateg = wp_get_post_terms($post_id, 'tantargyak_kateg', $args);

	$tags_html = "";
	$helysegek_array = array();
	$tantargyak_array = array();
	foreach ($helysegek_kateg as $key => $term_obj) {
		$helysegek_array []= '<span class="tag '. $term_obj->taxonomy .' ">'. $term_obj->name .'</span>';
	}
	foreach ($tantargyak_kateg as $key => $term_obj) {
		$tantargyak_array []= '<span class="tag '. $term_obj->taxonomy .' ">'. $term_obj->name .'</span>';
	}

	$helysegek_html = "";
	if ( !empty($helysegek_array) ) {
		$helysegek_html = '<div class="tags">'. implode('', $helysegek_array). '</div>';
	}

	$tantargyak_html = "";
	if ( !empty($tantargyak_array) ) {
		$tantargyak_html = '<div class="tags">'. implode('', $tantargyak_array). '</div>';
	}
	?>


	<div class="columns">
	  <div class="column is-4">
			<div class="mt_staff_profile">
				<?php
					if ( has_post_thumbnail($post_id) ) {
						echo wp_get_attachment_image( get_post_thumbnail_id(), 'large', false, array( 'class' => 'attachment-staff_tn size-staff_tn wp-post-image', 'width' => '570', 'height' => '570' ) );
					}
				?>
			</div>
	  </div>

	  <div class="column">
			<div class="mt_staff_generated_info">
			   <div class="staff_name_position">
			      <h2 class="single_staff_name"><?php echo $user_obj->display_name; ?></h2>
						<div class="rating" data-rate-value="<?php echo get_post_meta( $post_id, 'rating_current', true ); ?>" ></div>
			   </div>

			   <div class="single_staff_social">
						<div id="member_email">
							<a href="#" class="modal-button" data-target="modal-member_email"><i class="fa fa-envelope-o"></i></a>
						</div>
			   </div>

				 <div class="single_staff_meta">

						<div class="columns">

							<div class="column">
								<?php if (!empty($user_obj->user_url)) { ?>
								 <div class="staff_meta_first">Honlap:</div>
								 <div class="staff_meta_last"><a href="tel:<?php echo $user_obj->user_url; ?>"><?php echo $user_obj->user_url; ?></a></div>
								<?php } ?>

								<?php if (!empty($tel)) { ?>
						      <div class="staff_meta_first">Telefonszám:</div>
						      <div class="staff_meta_last"><a href="tel:<?php echo $tel; ?>"><?php echo $tel; ?></a></div>
								<?php } ?>

								<?php if (!empty($keres_eseten_hazhoz_mesz)) { ?>
									<div class="staff_meta_first">Házhoz megy?:</div>
									<div class="staff_meta_last"><?php if ( intval($keres_eseten_hazhoz_mesz) == 1 ) { echo 'igen'; } else { echo 'nem'; } ?></div>
								<?php } ?>

								<?php if (!empty($mennyibe_kerul_egy_ilyen_alkalom)) { ?>
									<div class="staff_meta_first">Óradíj:</div>
									<div class="staff_meta_last"><?php echo mt_money_format($mennyibe_kerul_egy_ilyen_alkalom); ?> / <?php echo intval($hany_perces_orakat_tartasz); ?> perc</div>
								<?php } ?>
							</div>

							<div class="column">
								<div class="staff_meta_first">Tárgyak:</div>
					      <div class="staff_meta_last">
									<?php echo $helysegek_html; ?>
					      </div>

								<div class="staff_meta_first">Ahol tanít:</div>
								<div class="staff_meta_last">
									<?php echo $tantargyak_html; ?>
								</div>
							</div>
						</div>

			   </div>

				<div class="clearfix"></div>
				<h3 class="hirdetes_cim"><?php echo get_the_title($post_id); ?></h3>
				<div class="single_staff_excerpt"><?php echo htmlspecialchars_decode(get_the_excerpt_by_id(), ENT_NOQUOTES); ?></div>
				<div class="single_staff_desc"><?php echo htmlspecialchars_decode(get_the_content_by_id(), ENT_NOQUOTES); ?></div>

			</div>
	  </div>
	</div>


	<?php
	$args = array(
		'post_id' => $post_id,
	);
	$user_comments = get_comments( $args );

	foreach ($user_comments as $key => $value) {
		if ( empty($value->comment_content) ) {
			unset($user_comments[$key]);
		}
	}

	if ( !empty($user_comments) ) {
		?>
		<div class="single_port_comments">
			<h3>Rólam írták</h3>

			<div class="comments">

				<?php
				$user_comments = array_chunk($user_comments, 2);
				foreach ($user_comments as $key => $array) {
					echo '<div class="columns">';
					foreach ($user_comments[$key] as $key => $comment_obj) {

						echo
							'<div class="column">
								<div class="card">
								  <div class="card-content">
								    <div class="content">
											<div class="comment-text">'. htmlspecialchars_decode($comment_obj->comment_content, ENT_NOQUOTES) .'</div>
								    </div>
								  </div>
									<footer class="card-footer">
										<p class="card-footer-item">
								      <span>'. date('Y.m.d. H:i', strtotime( $comment_obj->comment_date )) .'</span>
								    </p>
									</footer>
								</div>
							</div>';
					}
					echo '</div>';
				}

				?>

			</div>
		</div>
		<?php
	}
	?>


<?php } ?>

<div <?php if($generated_section == 'no'){ echo 'id="fw_c"'; } ?> class="single_staff_content clearfix"></div>

<?php endwhile; ?>
      <?php else : ?>
      <div id="notfound">
        <h2>Not Found</h2>
        <p>Sorry, but you are looking for something that isn't here.</p>
      </div>
      <?php endif; ?>

  </div>

  <!-- #page END -->

</div>


<div id="modal-member_email" class="modal">
	<div class="modal-background"></div>
	<div class="modal-card">
		<header class="modal-card-head">
			<p class="modal-card-title">Üzenet küldése</p>
			<button class="delete" aria-label="close"></button>
		</header>
		<section class="modal-card-body">

			<!--
			<div class="field is-horizontal">
			  <div class="field-label is-normal">
			    <label class="label">Neved:</label>
			  </div>
			  <div class="field-body">
			    <div class="field">
			      <p class="control">
			        <input type="text" value="<?php echo $user_obj->display_name; ?>" name="fullname" readonly disabled />
			      </p>
			    </div>
			  </div>
			</div>
		-->

			<div class="field is-horizontal">
			  <div class="field-label is-normal">
			    <label class="label">Tárgy:</label>
			  </div>
			  <div class="field-body">
			    <div class="field">
			      <p class="control">
			        <input type="text" value="" name="subject">
			      </p>
			    </div>
			  </div>
			</div>

			<div class="field is-horizontal">
			  <div class="field-label is-normal">
			    <label class="label">Üzeneted:</label>
			  </div>
			  <div class="field-body">
			    <div class="field">
			      <p class="control">
							<textarea required name="msg"></textarea>
			      </p>
			    </div>
			  </div>
			</div>

			<div class="alert hidden rd_small_alert rd_success_alert"><div class="rd_alert_content"></div></div>
		</section>
		<footer class="modal-card-foot">
			<button class="button is-success"><i class="fa fa-paper-plane" aria-hidden="true"></i> Elküldöm</button>
			<button class="button cancel"><i class="fa fa-ban" aria-hidden="true"></i> Mégsem</button>
		</footer>
	</div>
</div>

<div id="modal-rating" class="modal">
	<div class="modal-background"></div>
	<div class="modal-card">
		<header class="modal-card-head">
			<p class="modal-card-title">Értékelés elküldése</p>
			<button class="delete" aria-label="close"></button>
		</header>
		<section class="modal-card-body">
			<div class="score"><b>Értékelésed:</b> <span>3</span></div>

			<div class="desc">Ezen a felületen értékelheted írásban is az adott tanárt.
			Ha nem szeretnéd írásban akkor a megfelelő gombra kattintva kiválaszthatod hogy melyik értékelési formát szeretnéd elküldeni.
			Fontos: Az értékelés név nélkül megy.</div>
			<textarea name="review"></textarea>

			<div class="alert hidden rd_small_alert"><div class="rd_alert_content">Success Message! Your message here</div></div>
		</section>
		<footer class="modal-card-foot">
			<button class="button is-success"><i class="fa fa-paper-plane" aria-hidden="true"></i> Értékelés elküldése</button>
			<button class="button cancel"><i class="fa fa-ban" aria-hidden="true"></i> Mégsem</button>
		</footer>
	</div>
</div>


<!-- wrapper END -->
<?php get_footer(); ?>
