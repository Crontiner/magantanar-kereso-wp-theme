jQuery(function($) {


	// add reglog_page_style class to html
	if ( $('body.page-id-7460, body.page-id-7577, body.page-id-7582').length ) {
		$('html').addClass('reglog_page_style');
	}


	$('div.navigation').each(function(){
		var $this = $(this);
		var cont = $this.html();
		cont = cont.replace(/\s/g,'');
		if ( cont == "" ) { $this.addClass('hidden'); }
	});


	$('div.vc_row.elofizetesek_section .pt_col_nb_0').attr('class', 'pricetable-column pricetable-standard pricetable-after-featured pt_col_nb_0');


	/* parent menu bejelölés */
	$('header nav ul.menu li').each(function(){
		var $this = $(this);
		if ( $this.closest('ul.sub-menu').length ) {  }
		else {
			$this.find('a').first().addClass('main_menu_link');
		}
	});


	if($("input[type='checkbox'], input[type='radio']").length){
		$("input[type='checkbox'], input[type='radio']").iCheck({
			checkboxClass: 'icheckbox_minimal-blue',
			radioClass: 'iradio_minimal-blue',
			increaseArea: '20%'
		});
	}


	/*
	if ( $('.hirdetes_letrehozasa_sc input[name="profile_img"]').length ) {
		$('.hirdetes_letrehozasa_sc input[name="profile_img"]').dropzone({
																																				paramName: "file", // The name that will be used to transfer the file
																																				maxFilesize: 2, // MB
																																				url: "/file/post",
																																				accept: function(file, done) {
																																					console.dir(file.name);
																																				}
																																			});
	}*/


	if ( $('textarea[name="hirdetes_teljes_szoveg"]').length ) {
		tinymce.init({
			selector: 'textarea[name="hirdetes_teljes_szoveg"]',
			height: 300,
			resize: false,
		  menubar: false,
		  plugins: [
		    'advlist autolink lists link image charmap print preview anchor textcolor',
		    'searchreplace visualblocks code fullscreen',
		    'insertdatetime media table contextmenu paste code help wordcount'
		  ],
		  toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
		});
	}


	$( '.hirdetes_letrehozasa_sc .profile_img a' ).hover(
	  function() {
	    $( this ).parent().addClass('hover');
	  }, function() {
	    $( this ).parent().removeClass('hover');
	  }
	);


	// Bejelölt helységek és tantárgyak egyforma magasságra állítás
	if ( $('.hirdetes_letrehozasa_sc ul.bejelolt_helysegek').length ) {
		bejelolt_helysegek_tantargyak_egyforma_magasra();
	}

	// Bejelölt helységek és tantárgyak kijelölése
	$('.hirdetes_letrehozasa_sc ul.bejelolt_helysegek li a').click(function(e){
		e.preventDefault();
		var $this = $(this);
		var termID = $this.closest('.tags').attr('data-term');

		$this.closest('.column').find('ul.helyek_lista li input[value="'+ termID +'"]').iCheck('uncheck');
		$this.closest('li').fadeOut(function(){
			bejelolt_helysegek_tantargyak_egyforma_magasra();
		});
	});
	$('.hirdetes_letrehozasa_sc ul.bejelolt_tantargyak li a').click(function(e){
		e.preventDefault();
		var $this = $(this);
		var termID = $this.closest('.tags').attr('data-term');

		$this.closest('.column').find('ul.tantargyak_lista li input[value="'+ termID +'"]').iCheck('uncheck');
		$this.closest('li').fadeOut(function(){
			bejelolt_helysegek_tantargyak_egyforma_magasra();
		});
	});

	function bejelolt_helysegek_tantargyak_egyforma_magasra() {
		var $bejelolt_helysegek = $('.hirdetes_letrehozasa_sc ul.bejelolt_helysegek');
		var $bejelolt_tantargyak = $('.hirdetes_letrehozasa_sc ul.bejelolt_tantargyak');

		$bejelolt_tantargyak.css('height', 'auto');
		$bejelolt_helysegek.css('height', 'auto');

		if ( $bejelolt_helysegek.height() > $bejelolt_tantargyak.height() ) {
			$bejelolt_tantargyak.height( $bejelolt_helysegek.height() );
		} else {
			$bejelolt_helysegek.height( $bejelolt_tantargyak.height() );
		}
	}


	// A tanári hirdetés boxok egyforma magasságra állítás

	if ( $('.teacher_short_information_box').length ) {

		function teacher_short_information_box_same_height() {
			if ( $('.teacher_short_information_box_col').length == 0 ) {

				$('.teacher_short_information_box').each(function() {
					var $this = $(this);
					if ($this.parent().parent().hasClass('columns')) {
						$this.parent().addClass('teacher_short_information_box_col');
						$this.parent().parent().addClass('teacher_short_information_box_row');
					}
				});


				$(".teacher_short_information_box_row").each(function() {
					var div_heights = [];

					$(this).find('div.teacher_short_information_box').each(function(){
						$(this).find('.member_desc .empty_space').height('auto');
						div_heights.push( $(this).height() );
					});

					var div_height = Math.max.apply(Math, div_heights);

					$(this).find('div.teacher_short_information_box').each(function() {
						if ( (div_height - $(this).height()) > 0 ) {
							$(this).find('.member_desc .empty_space').height( div_height - $(this).height() );
						}
					});
				});
			}

			$('.teacher_short_information_box_row').removeClass('teacher_short_information_box_row');
			$('.teacher_short_information_box_col').removeClass('teacher_short_information_box_col');
		}

		$(window).bind("load resize", function() {
			var b = $('body');
			var w= b.innerWidth();

			teacher_short_information_box_same_height();
		});
	}


	$('div.elofizetes_form_sc .elofiz_opciok_fields .radio input[type="radio"]').on('ifChanged', function(event){
		var prices = [];
		var $osszeg = $(this).closest('.elofizetes_form_sc').find('#osszeg');

		$('div.elofizetes_form_sc .elofiz_opciok_fields .radio input[type="radio"]:checked').each(function(){
			var $this = $(this);
			var price = $this.closest('.radio').find('li.ar').attr('data-ar');

			if ( price > 0 ) { prices.push( price ); }
			else { prices.push( 0 ); }
		});

		prices = arraySum( prices );

		if ( $('div.hirdeteshossz_kedv input').length ) {
			prices = minusPercent(parseInt(prices),10);
		}

		if ( parseInt(prices) ) {
			$osszeg.removeClass('hidden');
			prices = new Intl.NumberFormat('hu-HU', { style: 'currency', currency: 'HUF' }).format(prices);
			$osszeg.find('strong').html(prices);
		} else {
			$osszeg.addClass('hidden');
			$osszeg.find('strong').html("");
		}
	});


	// Tanar Hits kezelés
	if ( $('body.single-tanarok_cpt') ) {
		var hash = Math.floor((Math.random() * 99999) + 1);

		jQuery.post(
				mt.ajaxurl,
				{
					post_data : $('body').attr('class'),
					hash : hash,
					action : 'add_user_hits'
				},
				function(data) {
						if (data.hash == hash) {
							/* console.log(data.html); */
						}
				},'json'
		);
	}


	// magantanarok_terulet_szerint_sc Widget: üres elemek törlése

	$('div.magantanarok_terulet_szerint_sc li.layer-2 .db').each(function(){
		var $this = $(this);
		if ( $this.find('b').html() > 0 ) { }
		else { $this.closest('li.layer-2').remove(); }
	});


	// magantanarok_terulet_szerint_sc Widget kezelése

	$('div.magantanarok_terulet_szerint_sc ul.layer-2').each(function(){
		var $this = $(this);
		$this.closest('li').find('a').first().addClass('has_child_terms');
	});


	$('div.magantanarok_terulet_szerint_sc ul.layer-0 li.layer-0.closed').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		$this.closest('ul.layer-0').find('li.layer-0').addClass('closed');
		var selected_item_html = $this[0].outerHTML;

		$this.closest('ul.layer-0').find('li.layer-0.active').remove();
		$this.closest('ul.layer-0').find('li.layer-0.hidden').removeClass('hidden');

		$this.closest('ul.layer-0').prepend( selected_item_html );
		$this.closest('ul.layer-0').find('li.layer-0').first().addClass('active').removeClass('closed');
		$this.addClass('hidden');

		$(window).scrollTo('.magantanarok_terulet_szerint_sc', {
							duration: 800,
							axis: 'y',
							offset: -150,
					});
	});


	/*
	$('body.single-tanarok_cpt .staff_single_page p').each(function(){
		var $this = $(this);

		if ( $this.html() == "&nbsp;" ) {
			//$this.remove();
		}
	});*/


	/* modal */
	$('.modal-button').click(function(e){
		e.preventDefault();
		var target = $(this).attr('data-target');
		$('#'+ target).addClass('is-active');
	});

	$('.modal .modal-background, .modal button.delete, .modal button.cancel').click(function(e){
		$(this).closest('.modal').removeClass('is-active');
	});

	/* */

	/* Rating */

	$('div.rating').each(function(){
		var $this = $(this);

		var readOnly_val = true;
		if ( $('body.single-tanarok_cpt.logged-in.administrator, body.single-tanarok_cpt.logged-in.subscriber').length ) {
			readOnly_val = false;
		}

		var options = {
				max_value: 5,
				step_size: 1,
				readonly: readOnly_val,

		}
		$this.rate(options);
	});

	$("body.single-tanarok_cpt.logged-in .rating").on("afterChange", function(ev, data){
		if ( $('body').hasClass('administrator') || $('body').hasClass('subscriber') ) {
			$('#modal-rating').addClass('is-active');
			$('#modal-rating').find('.score span').html(data.to);
		}
	});


	$('#modal-rating button.is-success').on('click', function(e) {
		e.preventDefault();
		var $this = $(this);
		var $modal = $this.closest('.modal');
		var score_val = $modal.find('.score span').html();
		var hash = Math.floor((Math.random() * 99999) + 1);
		var comment_text = $modal.find('textarea[name="review"]').val();

		$modal.find('div.alert').addClass('hidden').removeClass('rd_success_alert').removeClass('rd_error_alert');
		$modal.find('div.alert .rd_alert_content').html("");

		jQuery.post(
				mt.ajaxurl,
				{
					post_data : $('body').attr('class'),
					score : score_val,
					comment : comment_text,
					hash : hash,
					action : 'rating_submission'
				},
				function(data) {
						if (data.hash == hash) {
							if ( data.error === false ) {
								$modal.find('div.alert').addClass('rd_success_alert');
								$modal.find('div.alert .rd_alert_content').html( data.msg );
								$modal.find('div.alert').removeClass('hidden');
							} else {
								$modal.find('div.alert').addClass('rd_error_alert');
								$modal.find('div.alert .rd_alert_content').html( data.msg );
								$modal.find('div.alert').removeClass('hidden');
							}
						}
				},'json'
		);
	});

	/* */


	/* Send message to teacher */

	$('#modal-member_email button.is-success').on('click', function(e) {
		e.preventDefault();
		var $this = $(this);
		var $modal = $this.closest('.modal');
		var hash = Math.floor((Math.random() * 99999) + 1);
		var subject_text = $modal.find('input[name="subject"]').val();
		var comment_text = $modal.find('textarea[name="msg"]').val();

		$modal.find('div.alert').addClass('hidden').removeClass('rd_success_alert').removeClass('rd_error_alert');
		$modal.find('div.alert .rd_alert_content').html("");

		jQuery.post(
				mt.ajaxurl,
				{
					post_data : $('body').attr('class'),
					subject : subject_text,
					comment : comment_text,
					hash : hash,
					action : 'send_message_to_the_teacher'
				},
				function(data) {
						if (data.hash == hash) {
							if ( data.error === false ) {
								$modal.find('div.alert').addClass('rd_success_alert');
								$modal.find('div.alert .rd_alert_content').html( data.msg );
								$modal.find('div.alert').removeClass('hidden');

								$modal.find('input[name="subject"]').val("");
								$modal.find('textarea[name="msg"]').val("");
							} else {
								$modal.find('div.alert').addClass('rd_error_alert');
								$modal.find('div.alert .rd_alert_content').html( data.msg );
								$modal.find('div.alert').removeClass('hidden');
							}
						}
				},'json'
		);
	});


	/* Send message */

	$('#modal-send_message button.is-success').on('click', function(e) {
		e.preventDefault();
		var $this = $(this);
		var $modal = $this.closest('.modal');
		var hash = Math.floor((Math.random() * 99999) + 1);

		var msg_id = $modal.attr('data-msg-id');
		var subject_text = $modal.find('input[name="subject"]').val();
		var comment_text = $modal.find('textarea[name="msg"]').val();

		$modal.find('div.alert').addClass('hidden').removeClass('rd_success_alert').removeClass('rd_error_alert');
		$modal.find('div.alert .rd_alert_content').html("");

		jQuery.post(
				mt.ajaxurl,
				{
					msg_post_id : msg_id,
					subject : subject_text,
					comment : comment_text,
					hash : hash,
					action : 'send_message'
				},
				function(data) {
						if (data.hash == hash) {
							if ( data.error === false ) {
								$modal.find('div.alert').addClass('rd_success_alert');
								$modal.find('div.alert .rd_alert_content').html( data.msg );
								$modal.find('div.alert').removeClass('hidden');

								$modal.find('input[name="subject"]').val("");
								$modal.find('textarea[name="msg"]').val("");
							} else {
								$modal.find('div.alert').addClass('rd_error_alert');
								$modal.find('div.alert .rd_alert_content').html( data.msg );
								$modal.find('div.alert').removeClass('hidden');
							}
						}
				},'json'
		);
	});


	// Add new_messages_count to menu

	if ( $('input#new_messages_count').length ) {
		var $new_messages_count = $('input#new_messages_count');

		if ( $new_messages_count.val() > 0 ) {
			$('ul li.uzenetek.menu-item').each(function(){
				var $this = $(this);
				$this.addClass('has_new_msg');
				$this.find('a').append('<span class="msg_count">'+ $new_messages_count.val() +'</span>');
			});
		}
	}


	// Read selected message

	if ( $('#modal-read_message').length ) {
		$('#modal-read_message').addClass('is-active');
	}

	// Send reply

	if ( $('#modal-send_message').length ) {
		$('#modal-send_message').addClass('is-active');
	}


	// Fix footer to bottom of the screen if the screen height is bigger than actual page height

	$(window).bind('load resize', function() {
		if ( $(window).height() > $('body').height() ) {
			$('body').addClass('fixed_footer');
		} else {
			$('body').removeClass('fixed_footer');
		}
	});


	$('div.uzenetek_kezelese_sc .pagination_content').each(function(){
		if ( $(this).html() == "" ) {
			$(this).closest('tfoot').remove();
		}
	});


	$('div.reszletes_kereso_sc select[name="tanar_id"]').select2();


});


function arraySum(array) {
	var total = 0,
			len = array.length;

	for (var i = 0; i < len; i++){
		total += parseInt(array[i]);
	}

	return total;
};

function minusPercent(n,p) {
  return n - (n * (p/100));
}
