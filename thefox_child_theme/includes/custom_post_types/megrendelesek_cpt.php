<?php

/************************/
/*       METABOX        */
/************************/

add_action( 'add_meta_boxes', 'mt_megrendeles_adatok_metabox' );
function mt_megrendeles_adatok_metabox($post) {
	add_meta_box('megrendeles_adatok_metabox', 'Leadott megrendelési adatok', 'megrendeles_adatok_metabox_function', 'megrendelesek_cpt', 'normal' , 'default');
}

function megrendeles_adatok_metabox_function($post) {
	$all_meta = get_post_meta($post->ID);

	if ( !empty($all_meta['mettol_ervenyes'][0]) ) {
		$all_meta['mettol_ervenyes'][0] = date('Y-m-d', $all_meta['mettol_ervenyes'][0]);
	}
	if ( !empty($all_meta['meddig_ervenyes'][0]) ) {
		$all_meta['meddig_ervenyes'][0] = date('Y-m-d', $all_meta['meddig_ervenyes'][0]);
	}

	$ervenyes_class = "";
	$ervenyes_text = "";
	if ( megrendeles_ervenyesseg($post->ID) == 'ervenyes' ) {
		$ervenyes_class = "ervenyes";
		$ervenyes_text = "Érvényes hirdetés!";
	} else if ( megrendeles_ervenyesseg($post->ID) == 'lejart' ) {
		$ervenyes_class = "lejart";
		$ervenyes_text = "Lejárt hirdetés!";
	}

	echo "
		<br>
		<table>
			<tr>
				<th>Hirdetés: </th>
				<td><a target='_blank' href='". get_edit_post_link($all_meta['hirdetes_id'][0]) ."'><b>". get_the_title($all_meta['hirdetes_id'][0]) ."</b></a></td>
			</tr>
		</table>

		<table style='border: 1px solid #c1c1c1; padding: 15px 0;'>
			<tr>
				<td colspan='2' class=' megrendeles_ervenyesseg ". $ervenyes_class ." '>". $ervenyes_text ."</td>
			</tr>
			<tr>
				<th>Érvényesség kezdete: </th>
				<td><input required type='date' name='mettol_ervenyes' value='". $all_meta['mettol_ervenyes'][0] ."' /></td>
			</tr>
			<tr>
				<th>Érvényesség vége: </th>
				<td><input required type='date' name='meddig_ervenyes' value='". $all_meta['meddig_ervenyes'][0] ."' /></td>
			</tr>
		</table>

		<table>
			<tr>
				<th>Ár összesen: </th>
				<td><input readonly type='text' name='ar_osszesen' value='". mt_money_format($all_meta['ar_osszesen'][0]) ."' /></td>
			</tr>
			<tr>
				<th>Kedvezmény felhasználva: </th>
				<td><input disabled='disabled' type='checkbox' name='kedvezmeny_felhasznalva' value='1' ". checked($all_meta['kedvezmeny_felhasznalva'][0], TRUE, false) ." /></td>
			</tr>
			<tr>
				<th>Kedvezmény összege: </th>
				<td><input type='text' name='kedvezmeny_erteke' value='". $all_meta['kedvezmeny_erteke'][0] ."' /></td>
			</tr>
		</table>

		<hr><br>

		<table>
			<tr>
			  <th>Számlázási név: </th>
			  <td><input type='text' name='szamlazasi_nev' value='".$all_meta['szamlazasi_nev'][0]."' /></td>
			</tr>
			<tr>
			  <th>Irányítószám: </th>
			  <td><input type='text' name='iranyitoszam' value='".$all_meta['iranyitoszam'][0]."' /></td>
			</tr>
			<tr>
			  <th>Város: </th>
			  <td><input type='text' name='varos' value='".$all_meta['varos'][0]."' /></td>
			</tr>
			<tr>
			  <th>Utca/tér neve: </th>
			  <td><input type='text' name='utca_ter_neve' value='".$all_meta['utca_ter_neve'][0]."' /></td>
			</tr>
			<tr>
			  <th>Köztér típusa: </th>
			  <td><input type='text' name='kozter_tipusa' value='".$all_meta['kozter_tipusa'][0]."' /></td>
			</tr>
			<tr>
			  <th>Házszám: </th>
			  <td><input type='text' name='hazszam' value='".$all_meta['hazszam'][0]."' /></td>
			</tr>
			<tr>
			  <th>Épület: </th>
			  <td><input type='text' name='epulet' value='".$all_meta['epulet'][0]."' /></td>
			</tr>
			<tr>
			  <th>Magánszemélyként regisztráltam: </th>
			  <td><input type='checkbox' name='maganszemelykent_regisztraltam' value='1' ". checked($all_meta['maganszemelykent_regisztraltam'][0], 1, false) ." /></td>
			</tr>
			<tr>
			  <th>Adószám: </th>
			  <td><input type='text' name='adoszam' value='".$all_meta['adoszam'][0]."' /></td>
			</tr>
			<tr>
			  <th>Rendelés időpontja: </th>
			  <td><input readonly type='text' name='rendeles_idopontja' value='". date('Y.m.d. H:i:s', $all_meta['rendeles_idopontja'][0]) ."' /></td>
			</tr>
		</table>

		<hr><br>

		<table>
			<tr>
				<th>Előfizetés: </th>
				<td>
					<input readonly type='text' name='elofizetes_nev' value='".$all_meta['elofizetes_nev'][0]."' /><br>
					<input readonly type='text' name='elofizetes_ar' value='". mt_money_format($all_meta['elofizetes_ar'][0])."' /><br>

					<label>Pénzvisszatérítési garancia:</label>
					<input disabled='disabled' type='checkbox' name='elofizetes_penzvissza' value='1' ". checked($all_meta['elofizetes_penzvissza'][0], 1, false) ." />
				</td>
			</tr>
		</table>

		<table>
			<tr>
				<th>Kiemelés a találati lista elejére: </th>
				<td>
					<input readonly type='text' name='kiemeles_nev' value='".$all_meta['kiemeles_nev'][0]."' /><br>
					<input readonly type='text' name='kiemeles_ar' value='". mt_money_format($all_meta['kiemeles_ar'][0]) ."' /><br>

					<label>Pénzvisszatérítési garancia:</label>
					<input disabled='disabled' type='checkbox' name='kiemeles_penzvissza' value='1' ". checked($all_meta['kiemeles_penzvissza'][0], 1, false) ." />
				</td>
			</tr>
		</table>

		<table>
			<tr>
				<th>Főkiemelés az oldalak tetejére a képváltóba: </th>
				<td>
					<input readonly type='text' name='fokiemeles_nev' value='".$all_meta['fokiemeles_nev'][0]."' /><br>
					<input readonly type='text' name='fokiemeles_ar' value='". mt_money_format($all_meta['fokiemeles_ar'][0]) ."' /><br>

					<label>Pénzvisszatérítési garancia:</label>
					<input disabled='disabled' type='checkbox' name='fokiemeles_penzvissza' value='1' ". checked($all_meta['fokiemeles_penzvissza'][0], 1, false) ." />
				</td>
			</tr>
		</table>";
}

/**/


/*************************************/
/*    SAVE POST / METABOX / etc...   */
/*************************************/

add_action('save_post', 'save_megrendelesek_cpt_postdata');
function save_megrendelesek_cpt_postdata($post_id) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return ""; }

	if ($_POST['post_type'] == "megrendelesek_cpt") {

		if ( isset($_POST['mettol_ervenyes']) ) {
			update_post_meta( $post_id, 'mettol_ervenyes', strtotime($_POST['mettol_ervenyes'] .' 00:00:00') );
		}
		if ( isset($_POST['meddig_ervenyes']) ) {
			update_post_meta( $post_id, 'meddig_ervenyes', strtotime($_POST['meddig_ervenyes'] .' 00:00:00') );
		}

		update_post_meta( $post_id, 'szamlazasi_nev', $_POST['szamlazasi_nev'] );
		update_post_meta( $post_id, 'iranyitoszam', $_POST['iranyitoszam'] );
		update_post_meta( $post_id, 'varos', $_POST['varos'] );
		update_post_meta( $post_id, 'utca_ter_neve', $_POST['utca_ter_neve'] );
		update_post_meta( $post_id, 'kozter_tipusa', $_POST['kozter_tipusa'] );
		update_post_meta( $post_id, 'hazszam', $_POST['hazszam'] );
		update_post_meta( $post_id, 'epulet', $_POST['epulet'] );
		update_post_meta( $post_id, 'maganszemelykent_regisztraltam', intval($_POST['maganszemelykent_regisztraltam']) );
		update_post_meta( $post_id, 'adoszam', $_POST['adoszam'] );
	}
}

/**/
