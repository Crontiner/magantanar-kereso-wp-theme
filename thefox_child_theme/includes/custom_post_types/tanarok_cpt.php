<?php

/************************/
/*       METABOX        */
/************************/

add_action( 'add_meta_boxes', 'mt_tanarok_cpt_metabox' );
function mt_tanarok_cpt_metabox($post) {
	add_meta_box('tanarok_cpt_metabox', 'Hirdetés adatai', 'tanarok_cpt_metabox_function', 'tanarok_cpt', 'normal' , 'default');
}

function tanarok_cpt_metabox_function($post) {
	$all_meta = get_post_meta($post->ID);
	$author_id = get_post_field ('post_author', $post->ID);

	if ( intval($author_id) > 0 ) {
		$megrendelesek_cpt = new WP_Query(array( 	'post_type' => 'megrendelesek_cpt',
																							'fields' => 'ids',
																							'posts_per_page' => -1,
																							'post_status' => 'any',
																							'author' => $author_id,
																						));
		$megrendelesek_cpt_posts_array = $megrendelesek_cpt->posts;

		$megrendelesek_tr = "";
		foreach ($megrendelesek_cpt_posts_array as $key => $megrendeles_id) {
			$hirdetes_id = (int) get_post_meta( $megrendeles_id, 'hirdetes_id', true );
			$rendeles_idopont = get_the_date('Y.m.d H:i', $megrendeles_id);

			if ( intval($hirdetes_id) > 0 ) {
				$megrendelesek_tr .=
					'<tr>
						<td><a target="_blank" href="'. get_edit_post_link($megrendeles_id) .'">'. $rendeles_idopont .'</a></td>
					</tr>';
			}
		}

		echo "
			<table>
				<tr>
					<th>Hány perces órákat tart: </th>
					<td><input type='text' name='hany_perces_orakat_tartasz' value='". $all_meta['hany_perces_orakat_tartasz'][0] ."' /></td>
				</tr>
				<tr>
					<th>Mennyibe kerül egy ilyen alkalom: </th>
					<td><input type='text' name='mennyibe_kerul_egy_ilyen_alkalom' value='". $all_meta['mennyibe_kerul_egy_ilyen_alkalom'][0] ."' /></td>
				</tr>
				<tr>
					<th>Kérés esetén házhoz mész? </th>
					<td><input type='checkbox' name='keres_eseten_hazhoz_mesz' value='1' ". checked($all_meta['keres_eseten_hazhoz_mesz'][0], 1, false) ." /></td>
				</tr>
				<tr>
					<th>Tartasz órát online?: </th>
					<td><input type='checkbox' name='tartasz_orat_online' value='1' ". checked($all_meta['tartasz_orat_online'][0], 1, false) ." /></td>
				</tr>
			</table>

			<div class='clearfix'></div><br>

			<table>
				<tr>
					<td>Megrendelések: <br></td>
				</tr>
				". $megrendelesek_tr ."
			</table>

			<div class='clearfix'></div><br>
			";
	}
}

/**/


/*************************************/
/*    SAVE POST / METABOX / etc...   */
/*************************************/

add_action('save_post', 'save_tanarok_cpt_postdata');
function save_tanarok_cpt_postdata($post_id) {
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return ""; }

	if ($_POST['post_type'] == "tanarok_cpt") {
		update_post_meta($post_id, 'hany_perces_orakat_tartasz', intval($_POST['hany_perces_orakat_tartasz']));
		update_post_meta($post_id, 'mennyibe_kerul_egy_ilyen_alkalom', intval($_POST['mennyibe_kerul_egy_ilyen_alkalom']));
		update_post_meta($post_id, 'keres_eseten_hazhoz_mesz', intval($_POST['keres_eseten_hazhoz_mesz']));
		update_post_meta($post_id, 'tartasz_orat_online', intval($_POST['tartasz_orat_online']));
	}
}

/**/
