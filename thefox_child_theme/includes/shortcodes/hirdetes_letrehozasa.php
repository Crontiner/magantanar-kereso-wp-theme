<?php

add_shortcode('hirdetes_letrehozasa', 'hirdetes_letrehozasa_sc_function');
function hirdetes_letrehozasa_sc_function($atts) {
	global $form_info, $post;
	$result_html = "";
	$current_page_url = get_permalink($post->ID);
	$user_info = get_userdata(get_current_user_id());

	$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																			'fields' => 'ids',
																			'posts_per_page' => 1,
																			'post_status' => 'any',
																			'author' => $user_info->ID,
	));
	$tanarok_posts_array = $tanarok_cpt->posts;

	$tanarok_cpt_post_id = 0;
	$hirdetes_cime = "";
	$hirdetes_rovid_szoveg = "";
	$hirdetes_teljes_szoveg = "";

	$helysegek_kateg = "";
	$tantargyak_kateg = "";
	$hany_perces_orakat_tartasz = "";
	$mennyibe_kerul_egy_ilyen_alkalom = "";
	$keres_eseten_hazhoz_mesz = "";
	$tartasz_orat_online = "";
	$hirdetes_elonezete = do_shortcode('[teacher_short_information_box show_sample_data="true"]');

	if ( intval($tanarok_posts_array[0]) > 0 ) {
		$tanarok_cpt_post_id = intval($tanarok_posts_array[0]);

		$hirdetes_cime = get_the_title($tanarok_cpt_post_id);
		$hirdetes_rovid_szoveg = get_the_excerpt_by_id($tanarok_cpt_post_id);
		$hirdetes_teljes_szoveg = get_the_content_by_id($tanarok_cpt_post_id);

		$metadatas = get_post_custom($tanarok_cpt_post_id);
		$helysegek_kateg = wp_get_post_terms($tanarok_cpt_post_id, 'helysegek_kateg', array("fields" => "ids"));
		$tantargyak_kateg = wp_get_post_terms($tanarok_cpt_post_id, 'tantargyak_kateg', array("fields" => "ids"));
		$hany_perces_orakat_tartasz = $metadatas['hany_perces_orakat_tartasz'][0];
		$mennyibe_kerul_egy_ilyen_alkalom = $metadatas['mennyibe_kerul_egy_ilyen_alkalom'][0];
		$keres_eseten_hazhoz_mesz = $metadatas['keres_eseten_hazhoz_mesz'][0];
		$tartasz_orat_online = $metadatas['tartasz_orat_online'][0];

		$hirdetes_elonezete = do_shortcode('[teacher_short_information_box show_sample_data="false" tanari_hirdetes_id="'. $tanarok_cpt_post_id .'"]');
	}


	// ----


	if ( isset($form_info['error_msg']) ) {
		$form_info = '<p>'. $form_info['error_msg'] .'</p>';
	} else {
		$form_info = "";
	}


	// ----


	$categories = get_terms('helysegek_kateg', array('hide_empty' => false));
	$categoryHierarchy = array();
	sort_terms_hierarchicaly($categories, $categoryHierarchy);

	$li = "";
	foreach ($categoryHierarchy as $key => $value) {
		$parent_checked = "";
		if ( in_array($value->term_id, $helysegek_kateg) ) { $parent_checked = "checked"; }

		$li .= '<li>';
		$li .= 		'<input type="checkbox" name="helysegek_kateg[]" value="'. $value->term_id .'" id="'. $value->slug .'" '. $parent_checked .' >';
		$li .= 		'<label for="'. $value->slug .'">'. $value->name .'</label>';

		$sub_li = "";
		if ( !empty($value->children) && (count($value->children) > 0) ) {
			foreach ($value->children as $key2 => $value2) {
				$child_checked = "";
				if ( in_array($value2->term_id, $helysegek_kateg) ) { $child_checked = "checked"; }

				$sub_li .= '<li>';
				$sub_li .= 		'<input type="checkbox" name="helysegek_kateg[]" value="'. $value2->term_id .'" id="'. $value2->slug .'" '. $child_checked .' >';
				$sub_li .= 		'<label for="'. $value2->slug .'">'. $value2->name .'</label>';
				$sub_li .= '</li>';
			}
			$sub_li = '<ul>'. $sub_li .'</ul>';
		}

		$li .= $sub_li;
		$li .= '</li>';
	}

	$helyek_lista = '<ul class="main-ul helyek_lista">'. $li .'</ul>';


	// Megjelölt helységek
	$bejelolt_helysegek = "";
	if ( $tanarok_cpt_post_id > 0 ) {
		$terms = wp_get_post_terms( $tanarok_cpt_post_id, 'helysegek_kateg' );

		$li = "";
		foreach ($terms as $key => $term) {
			$li .= '<li>'.
							'<div class="tags has-addons" data-term="'. $term->term_id .'">
								<span class="tag is-danger">'. $term->name .'</span>
								<a class="tag is-delete"></a>
							</div>'.
						'</li>';
		}
		if (!empty($li)) {
			$bejelolt_helysegek = '<ul class="bejelolt_helysegek">'. $li .'</ul>';
		}
	}

	// ----


	$categories = get_terms('tantargyak_kateg', array('hide_empty' => false));
	$categoryHierarchy = array();
	sort_terms_hierarchicaly($categories, $categoryHierarchy);

	$li = "";
	foreach ($categoryHierarchy as $key => $value) {
		$parent_checked = "";
		if ( in_array($value->term_id, $tantargyak_kateg) ) { $parent_checked = "checked"; }

		$li .= '<li>';
		$li .= 		'<input type="checkbox" name="tantargyak_kateg[]" value="'. $value->term_id .'" id="'. $value->slug .'" '. $parent_checked .' >';
		$li .= 		'<label for="'. $value->slug .'">'. $value->name .'</label>';

		$sub_li = "";
		if ( !empty($value->children) && (count($value->children) > 0) ) {
			foreach ($value->children as $key2 => $value2) {
				$child_checked = "";
				if ( in_array($value2->term_id, $tantargyak_kateg) ) { $child_checked = "checked"; }

				$sub_li .= '<li>';
				$sub_li .= 		'<input type="checkbox" name="tantargyak_kateg[]" value="'. $value2->term_id .'" id="'. $value2->slug .'" '. $child_checked .' >';
				$sub_li .= 		'<label for="'. $value2->slug .'">'. $value2->name .'</label>';
				$sub_li .= '</li>';
			}
			$sub_li = '<ul>'. $sub_li .'</ul>';
		}

		$li .= $sub_li;
		$li .= '</li>';
	}
	$tantargyak_lista = '<ul class="main-ul tantargyak_lista">'. $li .'</ul>';

	// Megjelölt tantárgyak
	$bejelolt_tantargyak = "";
	if ( $tanarok_cpt_post_id > 0 ) {
		$terms = wp_get_post_terms( $tanarok_cpt_post_id, 'tantargyak_kateg' );

		$li = "";
		foreach ($terms as $key => $term) {
			$li .= '<li>'.
							'<div class="tags has-addons" data-term="'. $term->term_id .'">
								<span class="tag is-danger">'. $term->name .'</span>
								<a class="tag is-delete"></a>
							</div>'.
						'</li>';
		}
		if (!empty($li)) {
			$bejelolt_tantargyak = '<ul class="bejelolt_tantargyak">'. $li .'</ul>';
		}
	}


	// GET MAIN POST THUMBNAIL

	if ( has_post_thumbnail($tanarok_cpt_post_id) ) {
		$profile_img = '<div class="profile_img">
											<a href="'. $current_page_url .'?profilkep_torles=default"><i class="far fa-times-circle" title="Kép törlése"></i></a>
											'. wp_get_attachment_image( get_post_thumbnail_id($tanarok_cpt_post_id), 'medium_large' ) .'
										</div>';
	} else {
		$profile_img = wp_get_attachment_image( MEDIA_ID_EMPTY_PROFILE_IMG, 'medium_large', false);
	}

	// ----

	// GET SQUARE POST THUMBNAIL

	$square_profile_img_id = MultiPostThumbnails::get_post_thumbnail_id('tanarok_cpt', 'square_profile_img', $tanarok_cpt_post_id);

	if ( intval($square_profile_img_id) > 0 ) {
		$square_profile_img = '<div class="profile_img">
														<a href="'. $current_page_url .'?profilkep_torles=square"><i class="far fa-times-circle" title="Kép törlése"></i></a>
														'. wp_get_attachment_image( $square_profile_img_id, 'medium_large' ) .'
													</div>';
	} else {
		$square_profile_img = wp_get_attachment_image( MEDIA_ID_EMPTY_PROFILE_IMG, 'portfolio_squared', false);
	}

	// ----

	$result_html .=
	'<form action="" method="post" enctype="multipart/form-data">
		<div class="columns">
			<div class="column is-4">
				<h3>Milyen tárgyakból tartasz órákat?</h3><br>
				'. $bejelolt_tantargyak .'
				<div class="tanarok_cpt_terms_list">
					'. $tantargyak_lista .'
				</div>
			</div>

			<div class="column is-4">
				<h3>Hol tartod az óráidat?</h3><br>
				'. $bejelolt_helysegek .'
				<div class="tanarok_cpt_terms_list">
					'. $helyek_lista .'
				</div>
			</div>

			<div class="column is-4">
				<h3>Add meg a következő adatokat a hirdetésedhez:</h3><br>

				<div class="field">
					<label class="label" for="hany_perces_orakat_tartasz">Hány perces órákat tartasz?</label>
					<div class="control">
						<input type="text" value="'. $hany_perces_orakat_tartasz .'" name="hany_perces_orakat_tartasz" id="hany_perces_orakat_tartasz" />
					</div>
				</div>

				<div class="field">
					<label class="label" for="mennyibe_kerul_egy_ilyen_alkalom">Mennyibe kerül egy ilyen alkalom?	</label>
					<div class="control">
						<input type="text" value="'. $mennyibe_kerul_egy_ilyen_alkalom .'" name="mennyibe_kerul_egy_ilyen_alkalom" id="mennyibe_kerul_egy_ilyen_alkalom" />
					</div>
				</div>

				<div class="field">
					<div class="checkbox">
						<input type="checkbox" id="keres_eseten_hazhoz_mesz" name="keres_eseten_hazhoz_mesz" value="1" '. checked( $keres_eseten_hazhoz_mesz, '1', false ) .' >
						<label class="label" for="keres_eseten_hazhoz_mesz">Kérés esetén házhoz mész?</label>
					</div>
				</div>

				<div class="field">
					<div class="checkbox">
						<input type="checkbox" id="tartasz_orat_online" name="tartasz_orat_online" value="1" '. checked( $tartasz_orat_online, '1', false ) .'>
						<label class="label" for="tartasz_orat_online">Tartasz órát online?</label>
					</div>
				</div>
			</div>
		</div><br>

		<!-- new row -->

		<div class="columns">
			<div class="column is-8">

				<div class="columns">
					<div class="column is-6">
						<div class="field">
							<h3>Add meg hirdetésed címsorát: <small>(Legfeljebb 55 karakter)</small></h3>
							<div class="control">
								<input type="text" value="'. $hirdetes_cime .'" name="hirdetes_cime" />
							</div>
						</div>
						<br>
						<div class="field">
							<h3>Add meg hirdetésed figyelemfelkeltő szövegét a főoldalra: <small>(Legfeljebb 150 karakter)</small></h3>
							<div class="control">
								<textarea name="hirdetes_rovid_szoveg">'. $hirdetes_rovid_szoveg .'</textarea>
							</div>
						</div>
					</div>

					<div class="column is-6 upload_profile_images_box">
						<h3>Töltsd fel ide fényképedet:</h3>
						<small>A legjobb eredmény érdekében egy álló, 1:1 képarányú képet tölts fel, amelynek mérete legalább 768x768 pixel!</small>
						<div class="clearfix"></div><br>

						<div class="columns">
							<div class="column">

								<div class="field">
									<label class="label" for="profile_img">Profilkép</label>
									<div class="control">
										<input type="file" name="profile_img" id="profile_img" />
									</div>
								</div>

								<div class="clearfix"></div>
								'. $profile_img .'

							</div>
							<div class="column">

								<div class="field">
									<label class="label" for="square_profile_img">Négyzet alakú profilkép</label>
									<div class="control">
										<input type="file" name="square_profile_img" id="square_profile_img" />
									</div>
								</div>

								<div class="clearfix"></div>
								'. $square_profile_img .'
							</div>
						</div>
					</div>
				</div>

				<br><br><h3>Alkosd meg hirdetésed teljes szövegét a saját oldaladra:</h3>
				<textarea name="hirdetes_teljes_szoveg">'. $hirdetes_teljes_szoveg .'</textarea>

			</div>
			<div class="column is-4">

				<div id="hirdetes_elonezete">
					<h3 class="box-title">Hirdetés előnézete</h3>
					'. $hirdetes_elonezete .'
				</div>

			</div>
		</div>


		<div class="form-info">'. $form_info .'</div>
		<div class="clearfix"></div>

		<div class="columns">
			<div class="column"></div>
			<div class="column">
				<input type="submit" name="hirdetes_letrehozasa_sc_submit" value="Hirdetés mentése">
			</div>
		</div>
	</form>';

	return '<div class="hirdetes_letrehozasa_sc">'. $result_html .'</div>';
}


// Tanár: profilkép törlése
add_action('init', 'mt_remove_profile_img');
function mt_remove_profile_img() {
	if ( isset($_GET['profilkep_torles']) && is_user_logged_in() ) {
		$user_info = get_userdata(get_current_user_id());

		$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																				'fields' => 'ids',
																				'posts_per_page' => 1,
																				'post_status' => 'any',
																				'author' => $user_info->ID,
		));
		$tanarok_posts_array = $tanarok_cpt->posts;

		if ( intval($tanarok_posts_array[0]) > 0 ) {

			if ( $_GET['profilkep_torles'] == 'square' ) {
				$post_thumbnail_id = MultiPostThumbnails::get_post_thumbnail_id('tanarok_cpt', 'square_profile_img', $tanarok_posts_array[0]);

				$post_type = "tanarok_cpt";
				$thumbnail_id = "square_profile_img";

				if ( intval($post_thumbnail_id) > 0 ) {
					delete_post_meta($tanarok_posts_array[0], "{$post_type}_{$thumbnail_id}_thumbnail_id");
					wp_delete_attachment( $post_thumbnail_id, true );
				}
			} else {
				$post_thumbnail_id = get_post_thumbnail_id( intval($tanarok_posts_array[0]) );
				delete_post_thumbnail(intval($tanarok_posts_array[0]));
				wp_delete_attachment( $post_thumbnail_id, true );
			}

			wp_redirect( get_permalink(PAGE_HIRDETESEM) ); exit;
		}
	}
}


//**********************************/
// --------- SAVE INPUTS --------- //
//**********************************/

add_action('init', 'mt_save_hirdetes_letrehozasa_sc');
function mt_save_hirdetes_letrehozasa_sc() {
	if ( isset($_POST['hirdetes_letrehozasa_sc_submit']) ) {
		global $form_info;
		$user_info = get_userdata(get_current_user_id());

		// Ha van hírdetés post akkor abba ment, ha nincs akkor előbb létrehoz

		$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
		                                    'fields' => 'ids',
		                                    'posts_per_page' => 1,
																				'post_status' => 'any',
		                                    'author' => $user_info->ID,
		));
		$tanarok_posts_array = $tanarok_cpt->posts;

		if ( intval($tanarok_posts_array[0]) > 0 ) {
			$post_id = intval($tanarok_posts_array[0]);
		} else {
			$new_post_datas = array(
				'post_title'    => $user_info->display_name .' ('. $user_info->user_email .')',
				'post_content'  => "",
				'post_status'   => 'draft',
				'post_type' 		=> 'tanarok_cpt',
				'post_author'   => $user_info->ID,
			);
			$post_id = wp_insert_post( $new_post_datas );
		}


		$updated_post_datas = array( 'ID' => $post_id );

		if ( isset($_POST['hirdetes_cime']) ) {
			$updated_post_datas ['post_title']= $_POST['hirdetes_cime'];
		}
		if ( isset($_POST['hirdetes_rovid_szoveg']) ) {
			$updated_post_datas ['post_excerpt']= esc_attr(stripslashes(remove_links_from_text($_POST['hirdetes_rovid_szoveg'])));
		}
		if ( isset($_FILES['profile_img']) && isset($_FILES['profile_img']['size']) && ($_FILES['profile_img']['size'] > 0) ) {
			$new_attachment_id = mt_file_uploader('profile_img');

			if ( $new_attachment_id > 0 ) {
				$post_thumbnail_id = get_post_thumbnail_id( $post_id );
				wp_delete_attachment( $post_thumbnail_id, true );

				set_post_thumbnail( $post_id, $new_attachment_id );
			} else if ( isset($new_attachment_id['error_msg']) ) {
				$form_info = array('error_msg' => $new_attachment_id['error_msg']);
			}
			unset($_FILES['profile_img']);
		}
		if ( isset($_FILES['square_profile_img']) && isset($_FILES['square_profile_img']['size']) && ($_FILES['square_profile_img']['size'] > 0) ) {
			$new_attachment_id = mt_file_uploader('square_profile_img');

			if ( $new_attachment_id > 0 ) {
				$post_thumbnail_id = MultiPostThumbnails::get_post_thumbnail_id('tanarok_cpt', 'square_profile_img', $post_id);

				$post_type = "tanarok_cpt";
				$thumbnail_id = "square_profile_img";

				if ( intval($post_thumbnail_id) > 0 ) {
					delete_post_meta($post_id, "{$post_type}_{$thumbnail_id}_thumbnail_id");
					wp_delete_attachment( $post_thumbnail_id, true );
				}

				$result = MultiPostThumbnails::set_meta($post_id, $post_type, $thumbnail_id, $new_attachment_id);
			} else if ( isset($new_attachment_id['error_msg']) ) {
				$form_info = array('error_msg' => $new_attachment_id['error_msg']);
			}
			unset($_FILES['profile_img']);
		}
		if ( isset($_POST['hirdetes_teljes_szoveg']) ) {
			$updated_post_datas ['post_content']= esc_attr(stripslashes(remove_links_from_text($_POST['hirdetes_teljes_szoveg'])));
		}
		if ( isset($_POST['hany_perces_orakat_tartasz']) ) {
			update_post_meta($post_id, 'hany_perces_orakat_tartasz', intval($_POST['hany_perces_orakat_tartasz']));
		}
		if ( isset($_POST['mennyibe_kerul_egy_ilyen_alkalom']) ) {
			update_post_meta($post_id, 'mennyibe_kerul_egy_ilyen_alkalom', intval($_POST['mennyibe_kerul_egy_ilyen_alkalom']));
		}

		if ( isset($_POST['keres_eseten_hazhoz_mesz']) ) {
			update_post_meta($post_id, 'keres_eseten_hazhoz_mesz', $_POST['keres_eseten_hazhoz_mesz']);
		} else {
			update_post_meta($post_id, 'keres_eseten_hazhoz_mesz', "");
		}

		if ( isset($_POST['tartasz_orat_online']) ) {
			update_post_meta($post_id, 'tartasz_orat_online', $_POST['tartasz_orat_online']);
		} else {
			update_post_meta($post_id, 'tartasz_orat_online', "");
		}

		if ( isset($_POST['helysegek_kateg']) ) {
			$helysegek_kategoriak = array();
			if ( !empty($_POST['helysegek_kateg']) && is_array($_POST['helysegek_kateg']) ) {
				foreach ($_POST['helysegek_kateg'] as $key => $value) {
					$helysegek_kategoriak[]= (int) $value;
				}
			}
			wp_set_post_terms( $post_id, $helysegek_kategoriak, 'helysegek_kateg' );
		} else {
			wp_set_post_terms( $post_id, "", 'helysegek_kateg' );
		}

		if ( isset($_POST['tantargyak_kateg']) ) {
			$tantargyak_kategoriak = array();
			if ( !empty($_POST['tantargyak_kateg']) && is_array($_POST['tantargyak_kateg']) ) {
				foreach ($_POST['tantargyak_kateg'] as $key => $value) {
					$tantargyak_kategoriak[]= (int) $value;
				}
			}
			wp_set_post_terms( $post_id, $tantargyak_kategoriak, 'tantargyak_kateg' );
		} else {
			wp_set_post_terms( $post_id, "", 'tantargyak_kateg' );
		}


		wp_update_post( $updated_post_datas );
	}
}
