<?php

add_shortcode('elofizetes_form', 'elofizetes_form_sc_function');
function elofizetes_form_sc_function($atts) {
	global $form_info;

	$current_page_url = get_permalink($post->ID);
	$user_info = get_userdata(get_current_user_id());
	$hirdetes_hosszabbitas = FALSE;

	$megrendelesek_cpt = new WP_Query(array(
																			'post_type' => 'megrendelesek_cpt',
																			'fields' => 'ids',
																			'order' => 'DESC',
																			'orderby' => 'meta_value_num',
																			'meta_key' => 'rendeles_idopontja',
																			'posts_per_page' => 1,
																			'post_status' => 'any',
																			'author' => $user_info->ID,
																		));
	$megrendelesek_cpt_posts_array = $megrendelesek_cpt->posts;
	$last_megrendeles_cpt_post_id = intval($megrendelesek_cpt_posts_array[0]);


	$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																			'fields' => 'ids',
																			'posts_per_page' => 1,
																			'post_status' => 'any',
																			'author' => $user_info->ID,
	));
	$tanarok_posts_array = $tanarok_cpt->posts;
	$hirdetes_id = (int) $tanarok_posts_array[0];
	if ( $hirdetes_id == 0 ) { $hirdetes_id = ""; }

	if ( $hirdetes_id > 0 ) { }
	else {
		return '<p class="text-center">Az aktiválás előtt hozza létre hirdetését ezen az oldalon: <br><a href="'. get_permalink(PAGE_HIRDETESEM) .'">'. get_the_title(PAGE_HIRDETESEM) .'</a></p>';
	}


	if ( ($last_megrendeles_cpt_post_id > 0) && (get_post_status($last_megrendeles_cpt_post_id) == 'publish') ) {

		if ( megrendeles_ervenyesseg($last_megrendeles_cpt_post_id) == 'ervenyes' ) {
			// Hirdetés hosszabbítás

			$hirdetes_hosszabbitas = TRUE;

		} else {
			// Új hirdetés rendelése

			$hirdetes_hosszabbitas = FALSE;
		}

	} else if ( ($last_megrendeles_cpt_post_id > 0) && (get_post_status($last_megrendeles_cpt_post_id) != 'publish') ) {
		return '<p class="text-center">Megrendelése feldolgozás alatt.</p>';
	}


	if ( isset($form_info['error_msg']) ) {
		$form_info = '<p class="error_msg">'. $form_info['error_msg'] .'</p>';
	} if ( isset($form_info['response_msg']) ) {
		$form_info = '<p class="response_msg">'. $form_info['response_msg'] .'</p>';
	} else {
		$form_info = "";
	}


	$elofiz_opciok_cpt = new WP_Query(array( 'post_type' => 'elofiz_opciok_cpt', 'fields' => 'ids', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC' ));
	$elofiz_opciok_posts_array = $elofiz_opciok_cpt->posts;


	$elofiz_opciok_fields_html = "";
	foreach ($elofiz_opciok_posts_array as $key => $post_id) {
		$elofizopciok_data = get_post_meta( $post_id, 'elofizopciok_data', true );
		$inp_name = get_post_field( 'post_name', $post_id );
		$inp_name = str_replace('-', '_', $inp_name);

		$elofiz_opciok_fields_html .=
			'<div class="field">
				<label class="label post-title">'. get_the_title($post_id) .'</label>
				<div class="control input-group">';

		$items = "";
		foreach ($elofizopciok_data as $key => $val) {

			$table_item_ar = $val['table_item_ar'];

			$inp_val = base64_encode(serialize(
																				array(
																					'nev' => $val['table_item_nev'],
																					'honap' => $val['table_item_honap'],
																					'ar' => $val['table_item_ar'],
																					'penzvissza' => $val['table_item_penzvissza'],
																				)
																			));

			if ( $val['table_item_honap'] == 0 ) { $val['table_item_honap'] = ""; }
			else {
				$val['table_item_honap'] = '<li><b>Érvényesség: </b>'. date('Y.m.d.', strtotime("+{$val['table_item_honap']} month")) .'</li>';
			}

			if ( $val['table_item_ar'] == 0 ) { $val['table_item_ar'] = ""; }
			else {
				$val['table_item_ar'] = '<li><b>Ár: </b>'. mt_money_format($val['table_item_ar']) .'</li>';
			}

			if ( $val['table_item_penzvissza'] == 0 ) { $val['table_item_penzvissza'] = ""; }
			else {
				$val['table_item_penzvissza'] = '<li>100%-os pénzvisszatérítési garanciával!!!</li>';
			}

			$items .=
					'<div class="field">
						<div class="radio">
							<input type="radio" id="'. $inp_name .'_'. $key .'" name="'. $inp_name .'" value="'. $inp_val .'" '. checked( $keres_eseten_hazhoz_mesz, '1', false ) .' >
							<label class="label" for="'. $inp_name .'_'. $key .'">
								<span class="nev">'. $val['table_item_nev'] .'</span>
								<ul>
									<li class="honap">'. $val['table_item_honap'] .'</li>
									<li class="ar" data-ar="'. $table_item_ar .'">'. $val['table_item_ar'] .'</li>
									<li class="penzvissza">'. $val['table_item_penzvissza'] .'</li>
								</ul>
							</label>
						</div>
					</div>';
		}

		$elofiz_opciok_fields_html .= $items;
		$elofiz_opciok_fields_html .=
				'</div>
			</div>';
	}


	$hirdeteshosszabbitas_kedvezmeny_html = "";
	if ( $hirdetes_hosszabbitas === TRUE ) {
		$hirdeteshosszabbitas_kedvezmeny_html =
			'<div class="hirdeteshossz_kedv">
				<div class="field is-horizontal">
					<div class="field-label">
						<label class="label">Hirdetéshosszabbítás kedvezmény:</label>
					</div>
					<div class="field-body">
						<div class="field is-narrow">
							<div class="control">
								<input type="text" value="-10%" readonly>
							</div>
						</div>
					</div>
				</div>
			</div>';
	}


	// Előző megrendelés adatainak kitöltése

	$szamlazasi_nev = $user_info->display_name;
	$iranyitoszam = "";
	$varos = "";
	$utca_ter_neve = "";
	$kozter_tipusa = "";
	$hazszam = "";
	$epulet = "";
	$adoszam = "";

	if ( $last_megrendeles_cpt_post_id > 0 ) {
		$all_meta = get_post_meta($last_megrendeles_cpt_post_id);

		$szamlazasi_nev = $all_meta['szamlazasi_nev'][0];
		$iranyitoszam = $all_meta['iranyitoszam'][0];
		$varos = $all_meta['varos'][0];
		$utca_ter_neve = $all_meta['utca_ter_neve'][0];
		$kozter_tipusa = $all_meta['kozter_tipusa'][0];
		$hazszam = $all_meta['hazszam'][0];
		$epulet = $all_meta['epulet'][0];
		$adoszam = $all_meta['adoszam'][0];
	}

	// ---

	$result_html = '
		<div class="columns">
			<div class="column">
				<h2>Számlázási adataid <small>(ingyenes előfizetéshez nem szükséges kitölteni)<br>Előfizetés esetén van ezekre szükség, ezekkel az adatokkal fogjuk kiállítani a számlát.</small></h2>

				<div class="field">
					<label class="label" for="szamlazasi_nev">Számlázási név</label>
					<div class="control">
						<input type="text" name="szamlazasi_nev" id="szamlazasi_nev" value="'. $szamlazasi_nev .'">
					</div>
				</div>
				<div class="field">
					<label class="label" for="iranyitoszam">Irányítószám</label>
					<div class="control">
						<input type="text" name="iranyitoszam" id="iranyitoszam" value="'. $iranyitoszam .'">
					</div>
				</div>
				<div class="field">
					<label class="label" for="varos">Város</label>
					<div class="control">
						<input type="text" name="varos" id="varos" value="'. $varos .'">
					</div>
				</div>
				<div class="field">
					<label class="label" for="utca_ter_neve">Utca/tér neve</label>
					<div class="control">
						<input type="text" name="utca_ter_neve" id="utca_ter_neve" value="'. $utca_ter_neve .'">
					</div>
				</div>
				<div class="field">
					<label class="label" for="kozter_tipusa">Köztér típusa</label>
					<div class="control">
						<input type="text" name="kozter_tipusa" id="kozter_tipusa" value="'. $kozter_tipusa .'">
					</div>
				</div>
				<div class="field">
					<label class="label" for="hazszam">Házszám</label>
					<div class="control">
						<input type="text" name="hazszam" id="hazszam" value="'. $hazszam .'">
					</div>
				</div>
				<div class="field">
					<label class="label" for="epulet">Épület</label>
					<div class="control">
						<input type="text" name="epulet" id="epulet" value="'. $epulet .'">
					</div>
				</div>

				<div class="field">
					<div class="control">
				    	<label class="checkbox label">
				    		Magánszemélyként regisztráltam:
								<input type="checkbox" name="maganszemelykent_regisztraltam" value="1">
				    	</label>
					</div>
				</div>
				<div class="field last">
					<label class="label" for="adoszam">Adószám</label>
					<div class="control">
						<input type="text" name="adoszam" id="adoszam" value="'. $adoszam .'">
					</div>
				</div>

			</div>
			<div class="column elofiz_opciok_fields">
				<h2>Előfizetésed állapota <small>Itt választhatod ki előfizetésed tipusát:</small></h2>
				'. $hirdeteshosszabbitas_kedvezmeny_html .'
				'. $elofiz_opciok_fields_html .'
			</div>
		</div>


		<div class="columns">
			<div class="column"></div>
			<div class="column">

				<div id="osszeg" class="hidden">
					<h3>Összeg: <strong></strong></h3>
				</div>
				<div class="clearfix"></div>

				<div class="form-info">'. $form_info .'</div>
				<div class="clearfix"></div>

				<div class="columns">
					<div class="column"></div>
					<div class="column">
						<input type="submit" name="elofizetes_form_sc_submit" value="Küldés">
					</div>
				</div>

			</div>
		</div>';

	return '<div class="elofizetes_form_sc">'.
						'<form class="form-horizontal mt-form-style" method="post" enctype="multipart/form-data">'.
							$result_html .
						'</form>'.
					'</div>';
}


/********************/
/*       SAVE       */
/********************/

add_action('init', 'mt_save_elofizetes_form_sc');
function mt_save_elofizetes_form_sc() {
	if ( isset($_POST['elofizetes_form_sc_submit']) ) {
		global $form_info;

		$current_page_url = get_permalink($post->ID);
		$user_info = get_userdata(get_current_user_id());
		$hirdetes_hosszabbitas = FALSE;

		$megrendelesek_cpt = new WP_Query(array(
																				'post_type' => 'megrendelesek_cpt',
																				'fields' => 'ids',
																				'posts_per_page' => 1,
																				'order' => 'DESC',
																				'orderby' => 'meta_value_num',
																				'meta_key' => 'rendeles_idopontja',
																				'post_status' => 'any',
																				'author' => $user_info->ID,
																			));
		$megrendelesek_cpt_posts_array = $megrendelesek_cpt->posts;
		$last_megrendeles_cpt_post_id = intval($megrendelesek_cpt_posts_array[0]);

		if ( ($last_megrendeles_cpt_post_id > 0) && (get_post_status($last_megrendeles_cpt_post_id) == 'publish') ) {
			if ( megrendeles_ervenyesseg($last_megrendeles_cpt_post_id) == 'ervenyes' ) {
				// Hirdetés hosszabbítás
				$hirdetes_hosszabbitas = TRUE;
			} else {
				// Új hirdetés rendelése
				$hirdetes_hosszabbitas = FALSE;
			}
		}


		$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																				'fields' => 'ids',
																				'posts_per_page' => 1,
																				'post_status' => 'any',
																				'author' => $user_info->ID,
		));
		$tanarok_posts_array = $tanarok_cpt->posts;
		$hirdetes_id = (int) $tanarok_posts_array[0];
		if ( $hirdetes_id == 0 ) { $hirdetes_id = ""; }

		$szamlazasi_nev = esc_attr(stripslashes($_POST['szamlazasi_nev']));
		$iranyitoszam = esc_attr(stripslashes($_POST['iranyitoszam']));
		$varos = esc_attr(stripslashes($_POST['varos']));
		$utca_ter_neve = esc_attr(stripslashes($_POST['utca_ter_neve']));
		$kozter_tipusa = esc_attr(stripslashes($_POST['kozter_tipusa']));
		$hazszam = esc_attr(stripslashes($_POST['hazszam']));
		$epulet = esc_attr(stripslashes($_POST['epulet']));
		$maganszemelykent_regisztraltam = intval($_POST['maganszemelykent_regisztraltam']);
		$adoszam = esc_attr(stripslashes($_POST['adoszam']));

		$elofizetes_array = unserialize(base64_decode($_POST['elofizetes']));
		$elofizetes_nev = "";
		$elofizetes_honap = "";
		$elofizetes_ar = "";
		$elofizetes_penzvissza = "";

		if ( isset($elofizetes_array['nev']) ) 					{ $elofizetes_nev = $elofizetes_array['nev']; }
		if ( isset($elofizetes_array['honap']) ) 				{ $elofizetes_honap = $elofizetes_array['honap']; }
		if ( isset($elofizetes_array['ar']) ) 					{ $elofizetes_ar = $elofizetes_array['ar']; }
		if ( isset($elofizetes_array['penzvissza']) ) 	{ $elofizetes_penzvissza = $elofizetes_array['penzvissza']; }

		$kiemeles_array = unserialize(base64_decode($_POST['kiemeles_a_talalati_lista_elejere']));
		$kiemeles_nev = "";
		$kiemeles_honap = "";
		$kiemeles_ar = "";
		$kiemeles_penzvissza = "";

		if ( isset($kiemeles_array['nev']) ) 					{ $kiemeles_nev = $kiemeles_array['nev']; }
		if ( isset($kiemeles_array['honap']) ) 				{ $kiemeles_honap = $kiemeles_array['honap']; }
		if ( isset($kiemeles_array['ar']) ) 					{ $kiemeles_ar = $kiemeles_array['ar']; }
		if ( isset($kiemeles_array['penzvissza']) ) 	{ $kiemeles_penzvissza = $kiemeles_array['penzvissza']; }

		$fokiemeles_array = unserialize(base64_decode($_POST['fokiemeles_az_oldalak_tetejere_a_kepvaltoba']));
		$fokiemeles_nev = "";
		$fokiemeles_honap = "";
		$fokiemeles_ar = "";
		$fokiemeles_penzvissza = "";

		if ( isset($fokiemeles_array['nev']) ) 					{ $fokiemeles_nev = $fokiemeles_array['nev']; }
		if ( isset($fokiemeles_array['honap']) ) 				{ $fokiemeles_honap = $fokiemeles_array['honap']; }
		if ( isset($fokiemeles_array['ar']) ) 					{ $fokiemeles_ar = $fokiemeles_array['ar']; }
		if ( isset($fokiemeles_array['penzvissza']) ) 	{ $fokiemeles_penzvissza = $fokiemeles_array['penzvissza']; }

		$ar_osszesen = intval($elofizetes_ar) + intval($kiemeles_ar) + intval($fokiemeles_ar);

		if ( $hirdetes_hosszabbitas === TRUE ) {
			$ar_osszesen = minusPercent($ar_osszesen, HIRDETESHOSSZABBITAS_KEDVEZMENY);
		}


		$new_post_datas = array(
			'post_title'    => $user_info->display_name .' ('. $user_info->user_email .')',
			'post_content'  => "",
			'post_status'   => 'draft',
			'post_type' 		=> 'megrendelesek_cpt',
			'post_author'   => $user_info->ID,
			'post_name'			=> sanitize_title($user_info->display_name),

			'meta_input'		=> array(
																'hirdetes_id' => $hirdetes_id,
																'szamlazasi_nev' => $szamlazasi_nev,
																'iranyitoszam' => $iranyitoszam,
																'varos' => $varos,
																'utca_ter_neve' => $utca_ter_neve,
																'kozter_tipusa' => $kozter_tipusa,
																'hazszam' => $hazszam,
																'epulet' => $epulet,
																'maganszemelykent_regisztraltam' => $maganszemelykent_regisztraltam,
																'adoszam' => $adoszam,

																'elofizetes' => json_encode($elofizetes_array),
																	'elofizetes_nev' => $elofizetes_nev,
																	'elofizetes_honap' => $elofizetes_honap,
																	'elofizetes_ar' => $elofizetes_ar,
																	'elofizetes_penzvissza' => $elofizetes_penzvissza,

																'kiemeles' => json_encode($kiemeles_array),
																	'kiemeles_nev' => $kiemeles_nev,
																	'kiemeles_honap' => $kiemeles_honap,
																	'kiemeles_ar' => $kiemeles_ar,
																	'kiemeles_penzvissza' => $kiemeles_penzvissza,

																'fokiemeles' => json_encode($fokiemeles_array),
																	'fokiemeles_nev' => $fokiemeles_nev,
																	'fokiemeles_honap' => $fokiemeles_honap,
																	'fokiemeles_ar' => $fokiemeles_ar,
																	'fokiemeles_penzvissza' => $fokiemeles_penzvissza,

																'rendeles_idopontja' => strtotime(current_time('mysql')),
																'mettol_ervenyes' => "",
																'meddig_ervenyes' => "",
																'ar_osszesen' => $ar_osszesen,

																'kedvezmeny_felhasznalva' => (bool) $hirdetes_hosszabbitas,
																'kedvezmeny_erteke' => HIRDETESHOSSZABBITAS_KEDVEZMENY .'%',
															),
		);
		$post_id = wp_insert_post( $new_post_datas );

		if ( $post_id > 0 ) {
			$form_info = array('response_msg' => 'Megrendelését sikeresen rögzítettük!');
		} else {
			$form_info = array('error_msg' => 'Hiba történt a rendelés leadása közben.');
		}

	}
}
