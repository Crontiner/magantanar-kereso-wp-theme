<?php

add_shortcode('ertekeleseim_listazasa', 'ertekeleseim_listazasa_sc_function');
function ertekeleseim_listazasa_sc_function($atts) {
	$result_html = "";

	$args = array(
		'user_id' => array( get_current_user_id() ),
	);
	$user_comments = get_comments( $args );

	$tr = "";
	foreach ($user_comments as $key => $val) {
		$tanar_user_id = (int) get_post_field('post_author', $val->comment_post_ID);
		$user = get_user_by( 'ID', $tanar_user_id );
		$user_score = (int) get_comment_meta( $val->comment_ID, 'user_score', true );

		$tr .= 	'<tr>'.
							'<td><a target="_blank" href="'. get_permalink($val->comment_post_ID) .'">'. $user->display_name .'</a></td>'.
							'<td>'. $user_score .'</td>'.
							'<td>'. $val->comment_content .'</td>'.
							'<td>'. $val->comment_date .'</td>'.
						'</tr>';
	}


	$result_html .=
	'<div class="columns">
		<div class="column is-12">

			<h4>Tanári értékeléseim</h4><br>
			<table class="table is-bordered is-striped is-hoverable is-fullwidth">
				<thead>
					<tr>
						<th>Tanár neve</th>
						<th>Értékelés</th>
						<th>Hozzászólás</th>
						<th>Dátum</th>
					</tr>
				</thead>
				<!--
				<tfoot>
					<tr>
						<th colspan="4">
							<div class="pagination_content"></div>
						</th>
					</tr>
				</tfoot>
				-->
			  <tbody>'. $tr .'</tbody>
			</table>

		</div>
	</div>';

	return '<div class="ertekeleseim_listazasa_sc">'. $result_html .'</div>';
}
