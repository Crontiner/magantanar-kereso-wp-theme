<?php

add_shortcode('tanarok_lista', 'tanarok_lista_sc_function');
function tanarok_lista_sc_function($atts) {
	extract(shortcode_atts(array(
		'kategoria_szures' => "",
		'post_ids' => "",
		'talalatok_mutatasa' => "",
		'lapozas' => "",
	), $atts));
	global $form_info, $wp_query;
	$result_html = "";
	$kategoria_szures_array = "";
	$paged = intval($wp_query->query['paged']);

	$args = array(
		'post_type' => 'tanarok_cpt',
		'fields' => 'ids',
		'post_status' => 'publish',
		'orderby'  => array( 'meta_value_num' => 'ASC' ),
		'meta_key' => 'hits',
	);


	if ( strtolower($lapozas) == 'true' ) {
		$args['paged'] = $paged;
		$args['posts_per_page'] = get_option('posts_per_page');
	} else {
		$args['posts_per_page'] = -1;
	}

	if ( !empty($post_ids) ) {
		$args['post__in'] = unserialize($post_ids);
	}

	if ( !empty($kategoria_szures) ) {
		$args['tax_query'] = array(	'taxonomy' => 'tantargyak_kateg',
																'field'    => 'slug',
																'terms'    => $kategoria_szures,
															);
	}

	$tanarok_cpt = new WP_Query($args);
	$tanarok_cpt_posts_array = $tanarok_cpt->posts;

	$posts_array = array();
	foreach ($tanarok_cpt_posts_array as $key => $post_id) {
		$posts_array []= do_shortcode('[teacher_short_information_box tanari_hirdetes_id="'. $post_id .'"]');
	}

	$posts_array = array_chunk($posts_array, 3);

	foreach ($posts_array as $row => $posts_data) {
		$result_html .=
			'<div class="columns">
				<div class="column is-4">'. $posts_data[0] .'</div>
				<div class="column is-4">'. $posts_data[1] .'</div>
				<div class="column is-4">'. $posts_data[2] .'</div>
			</div>';
	}

	if ( strtolower($talalatok_mutatasa) == 'true' ) {
		$talalatok_mutatasa = '<div class="found_posts">'.
														'<br><h6>Találatok száma: '. intval($tanarok_cpt->found_posts) .'</h6>'.
														'<div class="clearfix"></div>'.
													'</div>';
	}

	if ( strtolower($lapozas) == 'true' ) {
		$lapozas = 	'<div class="pagination_content">'.
									wp_pagenavi(array("query" => $tanarok_cpt, 'echo' => false)).
								'</div>';
	}

	return 	'<div class="tanarok_lista_sc">'.
						$talalatok_mutatasa.
						$result_html.
						$lapozas.
					'</div>';
}
