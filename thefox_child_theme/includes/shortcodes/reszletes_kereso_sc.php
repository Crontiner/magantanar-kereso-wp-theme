<?php

add_shortcode('reszletes_kereso', 'reszletes_kereso_sc_function');
function reszletes_kereso_sc_function($atts) {
	$result_html = "";

	if ( isset($_POST['reszletes_kereso_sc_submit']) ) {
		$form_res =
			array(
				'location_ids' => $_POST['location_ids'],
				'subjects_ids' => $_POST['subjects_ids'],
				'ha_kerem_jojjon_hazhoz' => (int) $_POST['ha_kerem_jojjon_hazhoz'],
				'tartson_orat_online_is' => (int) $_POST['tartson_orat_online_is'],
				'egy_60_perces_ora_ara_legyen_legalabb' => (int) $_POST['egy_60_perces_ora_ara_legyen_legalabb'],
				'egy_60_perces_ora_ara_legyen_legfeljebb' => (int) $_POST['egy_60_perces_ora_ara_legyen_legfeljebb'],
				'tanar_id' => esc_attr(stripslashes($_POST['tanar_id'])),
			);
	} else {
		$form_res =
			array(
				'location_ids' => "",
				'subjects_ids' => "",
				'ha_kerem_jojjon_hazhoz' => "",
				'tartson_orat_online_is' => "",
				'egy_60_perces_ora_ara_legyen_legalabb' => "",
				'egy_60_perces_ora_ara_legyen_legfeljebb' => "",
				'tanar_id' => "",
			);
	}


	$categories = get_terms('helysegek_kateg', array('hide_empty' => false));
	$categoryHierarchy = array();
	sort_terms_hierarchicaly($categories, $categoryHierarchy);

	$li = "";
	foreach ($categoryHierarchy as $key => $value) {

		$checked = "";
		if ( !empty($form_res['location_ids']) && in_array($value->term_id, $form_res['location_ids']) ) { $checked = "checked"; }


		$li .= '<li>';
		$li .= 		'<input type="checkbox" name="location_ids[]" value="'. $value->term_id .'" id="'. $value->slug .'" '. $checked .' >';
		$li .= 		'<label for="'. $value->slug .'">'. $value->name .'</label>';

		$sub_li = "";
		if ( !empty($value->children) && (count($value->children) > 0) ) {
			foreach ($value->children as $key2 => $value2) {

				$checked = "";
				if ( !empty($form_res['location_ids']) && in_array($value2->term_id, $form_res['location_ids']) ) { $checked = "checked"; }


				$sub_li .= '<li>';
				$sub_li .= 		'<input type="checkbox" name="location_ids[]" value="'. $value2->term_id .'" id="'. $value2->slug .'" '. $checked .' >';
				$sub_li .= 		'<label for="'. $value2->slug .'">'. $value2->name .'</label>';
				$sub_li .= '</li>';
			}
			$sub_li = '<ul>'. $sub_li .'</ul>';
		}

		$li .= $sub_li;
		$li .= '</li>';
	}

	$helyek_lista = '<ul class="main-ul">'. $li .'</ul>';


	// ----

	$categories = get_terms('tantargyak_kateg', array('hide_empty' => false));
	$categoryHierarchy = array();
	sort_terms_hierarchicaly($categories, $categoryHierarchy);

	$li = "";
	foreach ($categoryHierarchy as $key => $value) {

		$checked = "";
		if ( !empty($form_res['subjects_ids']) && in_array($value->term_id, $form_res['subjects_ids']) ) { $checked = "checked"; }


		$li .= '<li>';
		$li .= 		'<input type="checkbox" name="subjects_ids[]" value="'. $value->term_id .'" id="'. $value->slug .'" '. $checked .' >';
		$li .= 		'<label for="'. $value->slug .'">'. $value->name .'</label>';

		$sub_li = "";
		if ( !empty($value->children) && (count($value->children) > 0) ) {
			foreach ($value->children as $key2 => $value2) {

				$checked = "";
				if ( !empty($form_res['subjects_ids']) && in_array($value2->term_id, $form_res['subjects_ids']) ) { $checked = "checked"; }


				$sub_li .= '<li>';
				$sub_li .= 		'<input type="checkbox" name="subjects_ids[]" value="'. $value2->term_id .'" id="'. $value2->slug .'" '. $checked .' >';
				$sub_li .= 		'<label for="'. $value2->slug .'">'. $value2->name .'</label>';
				$sub_li .= '</li>';
			}
			$sub_li = '<ul>'. $sub_li .'</ul>';
		}

		$li .= $sub_li;
		$li .= '</li>';
	}

	$tantargyak_lista = '<ul class="main-ul">'. $li .'</ul>';


	// Tanárok select
	$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																			'fields' => 'ids',
																			'posts_per_page' => -1,
																		));
	$tanarok_cpt_posts = $tanarok_cpt->posts;

	$tanarok_select = "";
	$tanarok_array = array();
	if ( !empty($tanarok_cpt_posts) ) {
		foreach ($tanarok_cpt_posts as $key => $post_id) {
			$author_id = get_post_field ('post_author', $post_id);
			$user = get_user_by( 'id', $author_id );

			if ( $author_id > 0 ) {
				$tanarok_array [$author_id] = $user->display_name;
			}
		}

		if ( !empty($tanarok_array) ) {
			$tanarok_array = array_unique($tanarok_array);
			asort($tanarok_array);

			$tanarok_select .= '<option value=""></option>';
			foreach ($tanarok_array as $user_id => $name) {
				$selected = "";
				if ( !empty($form_res['tanar_id']) && ($form_res['tanar_id'] == $user_id) ) { $selected = "selected"; }

				$tanarok_select .= '<option value="'. $user_id .'" '. $selected .' >'. $name .'</option>';
			}

			$tanarok_select = '<select name="tanar_id" id="tanar_id">'. $tanarok_select .'</select>';
		}
	}
	// ---


	$result_html =
		'<form class="form-horizontal mt-form-style" method="post">

				<div class="vc_row vc_row-fluid">

					<div class="vc_span4 wpb_column vc_column_container">
						<h3>Hely szerinti keresés</h3>
						<div class="tanarok_cpt_terms_list">
							'. $helyek_lista .'
						</div>
					</div>

					<div class="vc_span4 wpb_column vc_column_container">
						<h3>Tárgy szerinti keresés</h3>
						<div class="tanarok_cpt_terms_list">
							'. $tantargyak_lista .'
						</div>
					</div>

					<div class="vc_span4 wpb_column vc_column_container other-settings">
						<div class="row checkbox-content">
							<input type="checkbox" name="ha_kerem_jojjon_hazhoz" value="1" id="ha_kerem_jojjon_hazhoz" '. checked($form_res['ha_kerem_jojjon_hazhoz'], 1, false) .' >
							<label class="label" for="ha_kerem_jojjon_hazhoz">Ha kérem jöjjön házhoz</label>
						</div>

						<div class="row checkbox-content">
							<input type="checkbox" name="tartson_orat_online_is" value="1" id="tartson_orat_online_is" '. checked($form_res['tartson_orat_online_is'], 1, false) .' >
							<label class="label" for="tartson_orat_online_is">Tartson órát online is</label>
						</div>

						<div class="row inline">
							<label class="label" for="egy_60_perces_ora_ara_legyen_legalabb">Egy 60 perces óra ára legyen legalább:</label>
							<input type="text" name="egy_60_perces_ora_ara_legyen_legalabb" value="'. $form_res['egy_60_perces_ora_ara_legyen_legalabb'] .'" id="egy_60_perces_ora_ara_legyen_legalabb">
							<span>Ft</span>
						</div>

						<div class="row inline">
							<label class="label" for="egy_60_perces_ora_ara_legyen_legfeljebb">Egy 60 perces óra ára legyen legfeljebb:</label>
							<input type="text" name="egy_60_perces_ora_ara_legyen_legfeljebb" value="'. $form_res['egy_60_perces_ora_ara_legyen_legfeljebb'] .'" id="egy_60_perces_ora_ara_legyen_legfeljebb">
							<span>Ft</span>
						</div>

						<div class="row">
							<label class="label" for="tanar_id">Tanár neve:</label>
							'. $tanarok_select .'
						</div>

						<div class="submit-section">
							<input type="submit" name="reszletes_kereso_sc_submit" value="Keresés" />
						</div>

					</div>
				</div>
			</form>';

	$result_html .= reszletes_kereso_talalati_lista($form_res);

	return '<div class="reszletes_kereso_sc">'. $result_html .'</div>';
}


function reszletes_kereso_talalati_lista($form_res = "") {
	$result_html = "";
	$form_res_bool = FALSE;
	$paged = $wp_query->query_vars['paged'];

	foreach ($form_res as $key => $value) {
		if ( !empty($value) ) { $form_res_bool = TRUE; break; }
	}


	if ( $form_res_bool === TRUE ) {

		$args = array( 	'post_type' => 'tanarok_cpt',
                    'fields' 		=> 'ids',
                    'posts_per_page' => 16,
										'paged' 		=> $paged,
										'orderby'  	=> array( 'meta_value_num' => 'ASC' ),
										'meta_key' 	=> 'hits'
									);

		if ( !empty($form_res['location_ids']) || !empty($form_res['subjects_ids']) ) {
			$args['tax_query'] = array();

			if ( !empty($form_res['location_ids']) ) {
				$args['tax_query'][]= array(
																		'taxonomy' => 'helysegek_kateg',
																		'field'    => 'term_id',
																		'terms'    => $form_res['location_ids'],
																		'operator' => 'IN',
																);
			}
			if ( !empty($form_res['subjects_ids']) ) {
				$args['tax_query'][]= array(
																		'taxonomy' => 'tantargyak_kateg',
																		'field'    => 'term_id',
																		'terms'    => $form_res['subjects_ids'],
																		'operator' => 'IN',
																);
			}
		}


		if 	( (intval($form_res['ha_kerem_jojjon_hazhoz']) == 1) ||
					(intval($form_res['tartson_orat_online_is']) == 1) ||
					!empty($form_res['egy_60_perces_ora_ara_legyen_legalabb']) ||
					!empty($form_res['egy_60_perces_ora_ara_legyen_legfeljebb'])
				) {
			$args['meta_query'] = array();

			if ( intval($form_res['ha_kerem_jojjon_hazhoz']) == 1 ) {
				$args['meta_query'][] = array(
																			'key' => 'keres_eseten_hazhoz_mesz',
																			'value' => 1,
																	);
			}
			if ( intval($form_res['tartson_orat_online_is']) == 1 ) {
				$args['meta_query'][] = array(
																			'key' => 'tartasz_orat_online',
																			'value' => 1,
																	);
			}
			if ( intval($form_res['tartson_orat_online_is']) == 1 ) {
				$args['meta_query'][] = array(
																			'key' => 'tartasz_orat_online',
																			'value' => 1,
																	);
			}
			if ( !empty($form_res['egy_60_perces_ora_ara_legyen_legalabb']) ) {
				$args['meta_query'][] = array(
																	'key' => 'mennyibe_kerul_egy_ilyen_alkalom',
																	'value' => intval($form_res['egy_60_perces_ora_ara_legyen_legalabb']),
																	'compare' => '>=',
																);
			}
			if ( !empty($form_res['egy_60_perces_ora_ara_legyen_legfeljebb']) ) {
				$args['meta_query'][] = array(
																			'key' => 'mennyibe_kerul_egy_ilyen_alkalom',
																			'value' => intval($form_res['egy_60_perces_ora_ara_legyen_legfeljebb']),
																			'compare' => '<=',
																	);
			}
		}

		if ( !empty($form_res['tanar_id']) ) {
			$args['author'] = (int) $form_res['tanar_id'];
		}

		$tanarok_cpt = new WP_Query($args);
		$tanarok_cpt_posts = $tanarok_cpt->posts;


		if ( empty($tanarok_cpt_posts) ) {

			$result_html = '<br><h6>Nem találtunk eredményt a keresési feltételek alapján.</h6>';;

		} else {

			$posts_array = array();
			foreach ($tanarok_cpt_posts as $key => $post_id) {
				$posts_array []= do_shortcode('[teacher_short_information_box tanari_hirdetes_id="'. $post_id .'"]');
			}

			$posts_array = array_chunk($posts_array, 4);
			foreach ($posts_array as $row => $posts_data) {
				$result_html .=
					'<div class="columns">
						<div class="column is-3">'. $posts_data[0] .'</div>
						<div class="column is-3">'. $posts_data[1] .'</div>
						<div class="column is-3">'. $posts_data[2] .'</div>
						<div class="column is-3">'. $posts_data[3] .'</div>
					</div>';
			}

			$result_html = 	'<br><h6>Találatok száma: '. $tanarok_cpt->found_posts .'</h6>'.
											'<div class="clearfix"></div><br>'.
											$result_html .
											'<div class="pagination_content">'.
												wp_pagenavi(array("query" => $tanarok_cpt, 'echo' => false)).
											'</div>';
		}
	}

	return '<div class="reszletes_kereso_talalati_lista">'. $result_html .'</div>';
}
