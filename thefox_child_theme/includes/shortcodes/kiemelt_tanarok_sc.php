<?php

add_shortcode('kiemelt_tanarok', 'kiemelt_tanarok_sc_function');
function kiemelt_tanarok_sc_function($atts) {
	extract(shortcode_atts(array(
		'kategoria_szures' => ''
	), $atts));
	global $form_info;
	$result_html = "";

	$megrendelesek_cpt = new WP_Query(array( 	'post_type' => 'megrendelesek_cpt',
																						'fields' => 'ids',
																						'posts_per_page' => -1,
																						'post_status' => 'publish',
																						'meta_query' => array(	'relation' => 'AND',
																																		array(
																																			'key' => 'meddig_ervenyes',
																																			'value' => strtotime(current_time('mysql')),
																																			'compare' => '>',
																																		),
																																		array(
																																			'key' => 'meddig_ervenyes',
																																			'value' => '',
																																			'compare' => '!=',
																																		),
																																		array(
																																			'key' => 'meddig_ervenyes',
																																			'value' => NULL,
																																			'compare' => '!=',
																																		),
																																		array(
																																			'key' => 'kiemeles_penzvissza',
																																			'value' => 1,
																																			'compare' => '=',
																																		),
																																	),
																					));
	$megrendelesek_cpt_posts_array = $megrendelesek_cpt->posts;

	$users = array();
	foreach ($megrendelesek_cpt_posts_array as $key => $post_id) {
		$users []= (int) get_post_field('post_author', $post_id);
	}
	$users = array_values(array_filter(array_unique($users)));


	$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																			'fields' => 'ids',
																			'posts_per_page' => -1,
																			'post_status' => 'publish',
																			'author__in' => $users,
																			'orderby'  => array( 'meta_value_num' => 'ASC' ),
																			'meta_key' => 'hits',
																	));
	$tanarok_cpt_posts_array = $tanarok_cpt->posts;


	$posts_array = array();
	foreach ($tanarok_cpt_posts_array as $key => $post_id) {
		$posts_array []= do_shortcode('[teacher_short_information_box tanari_hirdetes_id="'. $post_id .'"]');
	}

	$posts_array = array_chunk($posts_array, 3);

	foreach ($posts_array as $row => $posts_data) {
		$result_html .=
			'<div class="columns">
				<div class="column is-4">'. $posts_data[0] .'</div>
				<div class="column is-4">'. $posts_data[1] .'</div>
				<div class="column is-4">'. $posts_data[2] .'</div>
			</div>';
	}

	return '<div class="kiemelt_tanarok_sc">'. $result_html .'</div>';
}
