<?php

/***********************/
/*      SHORTCODES     */
/***********************/

add_shortcode('reglog_form', 'reglog_form_sc_function');
function reglog_form_sc_function($atts) {
	extract(shortcode_atts(array(
		 'form_type' => "",
	), $atts));
	global $reglog_form_info;

	if ( isset($reglog_form_info['error_msg']) ) {
		$reglog_form_info = '<p>'. $reglog_form_info['error_msg'] .'</p>';
	} else {
		$reglog_form_info = "";
	}


	// emailben kapott linkek betöltése

	if ( isset($_GET['sikeres_jelszocsere']) ) {
		return '
			<form class="reglog_form sikeres_regisztracio" action="" method="post">
				<div class="form_content">
					<h3 class="vc_custom_heading">Jelszavadat sikeresen lecseréltük!</h3>
				</div>

				<div class="footer">
					<div class="one-half lostpass_link">
						<p><a href="'. get_permalink( PAGE_ELFELEJTETT_JELSZO ) .'">Elfelejtetted a jelszavad?</a></p>
					</div>
					<div class="one-half login_link">
						<p><a href="'. get_permalink( PAGE_BEJELENTKEZES ) .'">Bejelentkezés</a></p>
					</div>
					<div class="clear"></div>
				</div>
			</form>';
	}

	if ( isset($_GET['sikeres_regisztracio']) ) {
		return '
			<form class="reglog_form sikeres_regisztracio" action="" method="post">
				<div class="form_content">
					<h3 class="vc_custom_heading">Sikeres regisztráció!</h3>
					<h5 class="vc_custom_heading2">A bejelentkezéshez aktiválnod kell a regisztrációd <br>az email-ben kapott link segítségével.</h5>
				</div>

				<div class="footer">
					<div class="one-half lostpass_link">
						<p><a href="'. get_permalink( PAGE_ELFELEJTETT_JELSZO ) .'">Elfelejtetted a jelszavad?</a></p>
					</div>
					<div class="one-half login_link">
						<p><a href="'. get_permalink( PAGE_BEJELENTKEZES ) .'">Bejelentkezés</a></p>
					</div>
					<div class="clear"></div>
				</div>
			</form>';
	}

	if ( isset($_GET['sikeres_jelszocsere_kuldes']) ) {
		return '
			<form class="reglog_form sikeres_jelszocsere_kuldes" action="" method="post">
				<div class="form_content">
					<h5 class="vc_custom_heading2">Az új jelszó kéréséhez szükséges linket elküldtük a megadott email címre!</h5>
				</div>

				<div class="footer">
					<div class="one-half lostpass_link">
						<p><a href="'. get_permalink( PAGE_ELFELEJTETT_JELSZO ) .'">Elfelejtetted a jelszavad?</a></p>
					</div>
					<div class="one-half login_link">
						<p><a href="'. get_permalink( PAGE_BEJELENTKEZES ) .'">Bejelentkezés</a></p>
					</div>
					<div class="clear"></div>
				</div>
			</form>';
	}

	// -----

	if ( $form_type == "login" ) {
		if ( is_user_logged_in() ) { return "<div class='hide_reglog_form'></div>"; }

		// Regisztráció aktiválási link újraküldése
		if( isset($_GET['aktivalas_link_ujrakuldes']) && !empty($_GET['aktivalas_link_ujrakuldes']) ) {
			$registration_hash = $_GET['aktivalas_link_ujrakuldes'];

			$user_query = new WP_User_Query( array(
																							'meta_query' => array(
																								'relation' => 'AND',
																									array(
																										'key'     => 'registration_hash',
																										'value'   => $registration_hash,
																							 			'compare' => '='
																									),
																							)
																						) );
			$users = $user_query->get_results();

			if ( isset($users[0]->data->ID) ) {
				if ( get_user_meta($users[0]->data->ID, 'activated_account', true) != 1 ) {

					// email küldés a felhasználónak a regisztrációs adatairól
					$email_option_template = get_email_template( 'regisztracio-visszaigazolasa' );

					// Replace
					$link = get_site_url() .'?action=reg_activ' . '&h=' . $registration_hash;

					$body_option = stripslashes($email_option_template['msg']);
					$body_option = str_replace('%user_activation_link%', $link, $body_option);
					$body_option = str_replace('%nev%', $username, $body_option);
					$body_option_formatted = wpautop($body_option, 1);

					$to = $users[0]->data->user_email;
					$subject = $email_option_template['subject'];
					$body = $body_option_formatted;
					$headers = array('Content-Type: text/html; charset=UTF-8','From: '. $email_option_template['sender_name'] .' <'. $email_option_template['sender'] .'>' );
					wp_mail( $to, $subject, $body, $headers );

					//$reglog_form_info = "Az aktiválási linket elküldtük az E-mail címére.";

					wp_redirect( get_permalink( PAGE_BEJELENTKEZES ) );
					exit;

				} else {
					$reglog_form_info = "Már aktiváltad a regisztrációd. Bejelentkezhetsz.";
				}
			} else {
				$reglog_form_info = "";
			}
		}


		// Regisztráció aktiválása
		if ( isset($_GET['action']) && ($_GET['action'] == 'reg_activ') && isset($_GET['h']) && !empty($_GET['h']) ) {
			$user_registration_hash = esc_attr(stripslashes($_GET['h']));

			$args = array(
		    'role__in'   => array( 'tanar_role', 'Subscriber' ),
		    'meta_query' => array(
	        'relation' => 'AND',
	        array(
	            'key'     => 'registration_hash',
	            'value'   => $user_registration_hash,
	            'compare' => '='
	        ),
		    )
			);
			$wp_user_query = new WP_User_Query( $args );
			$users = $wp_user_query->get_results();
			$user_id = 0;
			if ( isset($users[0]->data->ID) && (intval($users[0]->data->ID) > 0) ) { $user_id = (int) $users[0]->data->ID; }

			if ( $user_id > 0 ) {
				if ( get_user_meta($user_id, 'activated_account', true) != 1 ) {

						// reg. aktiválása
						update_user_meta($user_id, 'activated_account', 1);
						$reglog_form_info = "Sikeres aktiválás!";

				} else {
					$reglog_form_info = "Regisztrációd már aktiválásra került!";
				}
			} else {
				$reglog_form_info = "Az aktivációs link nem érvényes!";
			}


			return '
				<form class="reglog_form login" action="" method="post">
					<div class="form_content">
						<h3 class="vc_custom_heading">'. $reglog_form_info .'</h3>
					</div>

					<div class="footer">
						<div class="one-half lostpass_link">
							<p><a href="'. get_permalink( PAGE_ELFELEJTETT_JELSZO ) .'">Elfelejtetted a jelszavad?</a></p>
						</div>
						<div class="one-half login_link">
							<p><a href="'. get_permalink( PAGE_BEJELENTKEZES ) .'">Bejelentkezés</a></p>
						</div>
						<div class="clear"></div>
					</div>
				</form>';
		}

		return '
			<form class="reglog_form login" action="" method="post">
				<div class="header">
					<h3 class="vc_custom_heading">Bejelentkezés</h3>
					<div class="clear"></div>
				</div>

				<div class="clear"></div>
				<div class="form_content">
					<input type="email" value="'. $_POST['login_email'] .'" name="login_email" placeholder="E-mail cím">
					<input type="password" value="'. $_POST['login_pass'] .'" name="login_pass" placeholder="Jelszó">

					<div class="form_response">'. $reglog_form_info .'</div>
					<div class="clear"></div>

					<div class="one-half">
						<div class="clear"></div>
					</div>
					<div class="one-half">
						<input type="submit" name="login_form" id="bejelentkezes" value="Bejelentkezés">
					</div>
					<div class="clear"></div>
				</div>

				<div class="footer">
					<div class="one-half lostpass_link">
						<p><a href="'. get_permalink( PAGE_ELFELEJTETT_JELSZO ) .'">Elfelejtetted a jelszavad?</a></p>
					</div>
					<div class="one-half registration_link">
						<p><a href="'. get_permalink( PAGE_REGISZTRACIO ) .'">Regisztráció</a></p>
					</div>
					<div class="clear"></div>
				</div>
			</form>';
	}

	if ( $form_type == "teacher-registration" ) {

		return '
			<form class="reglog_form registration" action="" method="post">
				<div class="header">
					<h3 class="vc_custom_heading">Regisztráció tanárként</h3>
					<div class="clear"></div>
				</div>

				<div class="clear"></div>
				<div class="form_content">

					<div class="one-half"></div>

					<input type="text" value="'. $_POST['vezeteknev'] .'" name="vezeteknev" placeholder="Vezetéknév">
					<input type="text" value="'. $_POST['keresztnev'] .'" name="keresztnev" placeholder="Keresztnév">
					<input type="text" value="'. $_POST['telefonszam'] .'" name="telefonszam" placeholder="Telefonszám">
					<input type="email" value="'. $_POST['email_cim'] .'" name="email_cim" placeholder="E-mail cím">

					<div class="empty_space"></div>
					<input type="password" value="'. $_POST['jelszo'] .'" name="jelszo" placeholder="Jelszó">
					<input type="password" value="'. $_POST['jelszo2'] .'" name="jelszo2" placeholder="Jelszó még egyszer">

					<div class="one-half"></div>
					<div class="one-half aszf">
						<input type="checkbox" value="1" name="aszf_elfogadas" id="aszf_elfogadas">
						<label for="aszf_elfogadas">elfogadom az <a target="_blank" href="#">ÁSZF</a>-et</label>
					</div>

					<div class="clear"></div>
					<div class="empty_space"></div>
					<div class="form_response">'. $reglog_form_info .'</div>
					<div class="clear"></div>

					<div class="one-half"></div>
					<div class="one-half">
						<input type="submit" name="teacher_sign_up_form" value="Regisztrálok">
					</div>
					<div class="clear"></div>
				</div>

				<div class="footer">
					<div class="one-half lostpass_link">
						<p><a href="'. get_permalink( PAGE_ELFELEJTETT_JELSZO ) .'">Elfelejtetted a jelszavad?</a></p>
					</div>
					<div class="one-half login_link">
						<p><a href="'. get_permalink( PAGE_BEJELENTKEZES ) .'">Bejelentkezés</a></p>
					</div>
					<div class="clear"></div>
				</div>
			</form>';
	}

	if ( $form_type == "student-registration" ) {

		return '
			<form class="reglog_form registration" action="" method="post">
				<div class="header">
					<h3 class="vc_custom_heading">Regisztráció diákként</h3>
					<div class="clear"></div>
				</div>

				<div class="clear"></div>
				<div class="form_content">

					<div class="one-half"></div>

					<input type="text" value="'. $_POST['vezeteknev'] .'" name="vezeteknev" placeholder="Vezetéknév">
					<input type="text" value="'. $_POST['keresztnev'] .'" name="keresztnev" placeholder="Keresztnév">
					<input type="email" value="'. $_POST['email_cim'] .'" name="email_cim" placeholder="E-mail cím">

					<div class="empty_space"></div>
					<input type="password" value="'. $_POST['jelszo'] .'" name="jelszo" placeholder="Jelszó">
					<input type="password" value="'. $_POST['jelszo2'] .'" name="jelszo2" placeholder="Jelszó még egyszer">

					<div class="one-half"></div>
					<div class="one-half aszf">
						<input type="checkbox" value="1" name="aszf_elfogadas" id="aszf_elfogadas">
						<label for="aszf_elfogadas">elfogadom az <a target="_blank" href="#">ÁSZF</a>-et</label>
					</div>

					<div class="clear"></div>
					<div class="empty_space"></div>
					<div class="form_response">'. $reglog_form_info .'</div>
					<div class="clear"></div>

					<div class="one-half"></div>
					<div class="one-half">
						<input type="submit" name="student_sign_up_form" value="Regisztrálok">
					</div>
					<div class="clear"></div>
				</div>

				<div class="footer">
					<div class="one-half lostpass_link">
						<p><a href="'. get_permalink( PAGE_ELFELEJTETT_JELSZO ) .'">Elfelejtetted a jelszavad?</a></p>
					</div>
					<div class="one-half login_link">
						<p><a href="'. get_permalink( PAGE_BEJELENTKEZES ) .'">Bejelentkezés</a></p>
					</div>
					<div class="clear"></div>
				</div>
			</form>';
	}


	if ( $form_type == "lost_password" ) {

		$jelszo_input_html = "";
		$user_email = "";
		$user_email_disable = "";
		if ( isset($_GET['action']) &&
				 ( $_GET['action'] == 'getnewpass' ) &&
				 isset($_GET['h']) &&
				 !empty($_GET['h'])
			 ) {
			$user_query = new WP_User_Query( array( 'meta_key' => 'new_password_token', 'meta_value' => esc_attr(stripslashes($_GET['h'])) ) );
			$users = $user_query->get_results();

			$user_id = 0;
			if ( !empty( $users ) ) {
				foreach ($users as $key => $user) {
					$user_id = $user->ID;
					$user_email = $user->user_email;
					$user_email_disable = 'disabled';
					break;
				}
			}

			if ( $user_id > 0 ) {
				$jelszo_input_html =
					'<div class="empty_space"></div>
					<input type="password" value="" name="new_pass" placeholder="Jelszó">
					<input type="password" value="" name="new_pass2" placeholder="Jelszó még egyszer">';
			}
		}

		return '
			<form class="reglog_form lost_password" action="" method="post">
				<div class="header">
					<h3 class="vc_custom_heading">Új jelszó kérése</h3>
					<div class="clear"></div>
				</div>

				<div class="clear"></div>
				<div class="form_content">
					<input type="email" value="'. $user_email .'" name="user_email" placeholder="E-mail cím" '. $user_email_disable .' >
					'. $jelszo_input_html .'

					<div class="form_response">'. $reglog_form_info .'</div>
					<div class="clear"></div>

					<div class="one-half"></div>
					<div class="one-half">
						<input type="submit" name="new_password_submit" value="küldés">
					</div>
					<div class="clear"></div>
				</div>

				<div class="footer">
					<div class="one-half lostpass_link">
						<p><a href="'. get_permalink( PAGE_REGISZTRACIO ) .'">Regisztráció</a></p>
					</div>
					<div class="one-half login_link">
						<p><a href="'. get_permalink( PAGE_BEJELENTKEZES ) .'">Bejelentkezés</a></p>
					</div>
					<div class="clear"></div>
				</div>
			</form>';
	}

	return "";
}


/*******************************/
/*          ADD ACTION         */
/*******************************/


	//****************************/
	// --------- LOGIN --------- //
	//****************************/

add_action('init', 'mt_login');
function mt_login() {
  if ( isset($_POST['login_form']) ) {
		global $reglog_form_info;

		$email 	= $_POST['login_email'];
		$pass 	= $_POST['login_pass'];

		$user = get_user_by('email', $email);

		if (!empty($user->user_login)) {

		    $user_info = array();
		    $user_info['user_login'] 		= $user->user_login;
		    $user_info['user_password'] = $pass;
		    $user_info['remember'] 			= false;

		    $user_activated = get_user_meta($user->ID, 'activated_account', true);

				// adminok beengedése aktiválás nélkül
				if ( in_array('administrator', $user->roles) ) { $user_activated = 1; }

		    if ($user_activated == 1) {
	        if ( !wp_check_password($pass, $user->data->user_pass, $user->ID) ) {
	  				$reglog_form_info = array('error_msg' => 'A megadott jelszó hibás');
	        } else {

						$login_user = wp_signon($user_info, false);
						// Minden ok, beengedjük.
						$reglog_form_info = '';

						wp_redirect( home_url() );
						exit;
	        }
		    } else {
					$link = get_permalink( PAGE_BEJELENTKEZES ) .'?aktivalas_link_ujrakuldes='. get_user_meta( $user->ID, 'registration_hash', true );
					$aktivalasi_link = '<a href="'. $link .'">Aktiválási link újraküldése.</a>';
	    		$reglog_form_info = array('error_msg' => 'Még nem aktiváltad a regisztrációd! <br>'. $aktivalasi_link);
		    }
		} else {
		    $reglog_form_info = array('error_msg' => 'Nem találtunk felhasználót ilyen email címmel');
		}
  }
}


	//***********************************/
	// --------- REGISTRATION --------- //
	//***********************************/

add_action('init', 'mt_regisztracio');
function mt_regisztracio (){
	if ( isset($_POST['teacher_sign_up_form']) ) {
		global $reglog_form_info;

		if ( empty($_POST['aszf_elfogadas']) ) {
			$reglog_form_info = array('error_msg' => 'Az ÁSZF elfogadása kötelező a regisztrációhoz');
		}

		// check: kötelező megadni
		if ( !isset($reglog_form_info['error_msg']) ) {
			if ( empty($_POST['vezeteknev']) ||
					 empty($_POST['keresztnev']) ||
					 empty($_POST['telefonszam']) ||
					 empty($_POST['email_cim']) ||
					 empty($_POST['jelszo']) ||
					 empty($_POST['jelszo2'])
				 ) { $reglog_form_info = array('error_msg' => 'Nincs kitöltve minden mező'); }
		 }

		// check: egyeznie kell más értékkel
		if ( !isset($reglog_form_info['error_msg']) ) {
			if ( $_POST['jelszo'] != $_POST['jelszo2'] ) {
				$reglog_form_info = array('error_msg' => 'Nem egyeznek a beírt jelszavak');
				unset($_POST['jelszo'], $_POST['jelszo2']);
			}
		}

		// check: email címek ellenőrzése
		if ( !isset($reglog_form_info['error_msg']) ) {
			if ( !is_email($_POST['email_cim']) ) {
				$reglog_form_info = array('error_msg' => 'A megadott email cím nem megfelelő');
				unset($_POST['email_cim']);
			}
		}

		// Check: jelszóerősség
		if ( !isset($reglog_form_info['error_msg']) ) {
			if ( !empty(checkPassword($_POST['jelszo'])) ) {
				$reglog_form_info = array('error_msg' => checkPassword($_POST['jelszo']) );
				unset($_POST['jelszo']);
			}
		}

		// minden adat megvan --> értékelés
		if ( !isset($reglog_form_info['error_msg']) ) {
			if( email_exists($_POST['email_cim']) == false ) {
				$rand_pass = $_POST['jelszo'];
				$vezeteknev = $_POST['vezeteknev'];
				$keresztnev = $_POST['keresztnev'];
				$username = $vezeteknev .' '. $keresztnev;

				$new_member = wp_create_user( $_POST['email_cim'], $rand_pass, $_POST['email_cim'] );
				wp_update_user( array( 	'ID' => $new_member,
																'first_name' => $keresztnev,
																'last_name' => $vezeteknev,
																'display_name' => $username,
																'role' => 'tanar_role'
															) );


				// Egyéb adatok mentése

				add_user_meta( $new_member, 'telefonszam', $_POST['telefonszam'], true );
				add_user_meta( $new_member, 'activated_account', '0', true );


				$hash = hash('sha256', wp_generate_password());

				add_user_meta( $new_member, 'registration_hash', $hash, true );

				// email küldés a felhasználónak a regisztrációs adatairól
				$email_option_template = get_email_template( 'regisztracio-visszaigazolasa' );

				// Replace
				$link = get_site_url() .'?action=reg_activ' . '&h=' . $hash;

				$body_option = stripslashes($email_option_template['msg']);
				$body_option = str_replace('%user_activation_link%', $link, $body_option);
				$body_option = str_replace('%nev%', $username, $body_option);
				$body_option_formatted = wpautop($body_option, 1);


				$to = $_POST['email_cim'];
				$subject = $email_option_template['subject'];
				$body = $body_option_formatted;
				$headers = array('Content-Type: text/html; charset=UTF-8','From: '. $email_option_template['sender_name'] .' <'. $email_option_template['sender'] .'>' );
				wp_mail( $to, $subject, $body, $headers );


				wp_redirect( get_permalink(PAGE_TANAR_REGISZTRACIO) .'?sikeres_regisztracio' ); die;
			} else {
				$reglog_form_info = array('error_msg' => 'Már létezik felhasználó a megadott email címmel');
			}
		}
	}

	if ( isset($_POST['student_sign_up_form']) ) {
		global $reglog_form_info;

		if ( empty($_POST['aszf_elfogadas']) ) {
			$reglog_form_info = array('error_msg' => 'Az ÁSZF elfogadása kötelező a regisztrációhoz');
		}

		// check: kötelező megadni
		if ( !isset($reglog_form_info['error_msg']) ) {
			if ( empty($_POST['vezeteknev']) ||
					 empty($_POST['keresztnev']) ||
					 empty($_POST['email_cim']) ||
					 empty($_POST['jelszo']) ||
					 empty($_POST['jelszo2'])
				 ) { $reglog_form_info = array('error_msg' => 'Nincs kitöltve minden mező'); }
		 }

		// check: egyeznie kell más értékkel
		if ( !isset($reglog_form_info['error_msg']) ) {
			if ( $_POST['jelszo'] != $_POST['jelszo2'] ) {
				$reglog_form_info = array('error_msg' => 'Nem egyeznek a beírt jelszavak');
				unset($_POST['jelszo'], $_POST['jelszo2']);
			}
		}

		// check: email címek ellenőrzése
		if ( !isset($reglog_form_info['error_msg']) ) {
			if ( !is_email($_POST['email_cim']) ) {
				$reglog_form_info = array('error_msg' => 'A megadott email cím nem megfelelő');
				unset($_POST['email_cim']);
			}
		}

		// Check: jelszóerősség
		if ( !isset($reglog_form_info['error_msg']) ) {
			if ( !empty(checkPassword($_POST['jelszo'])) ) {
				$reglog_form_info = array('error_msg' => checkPassword($_POST['jelszo']) );
				unset($_POST['jelszo']);
			}
		}

		// minden adat megvan --> értékelés
		if ( !isset($reglog_form_info['error_msg']) ) {
			if( email_exists($_POST['email_cim']) == false ) {
				$rand_pass = $_POST['jelszo'];
				$vezeteknev = $_POST['vezeteknev'];
				$keresztnev = $_POST['keresztnev'];
				$username = $vezeteknev .' '. $keresztnev;

				$new_member = wp_create_user( $_POST['email_cim'], $rand_pass, $_POST['email_cim'] );
				wp_update_user( array( 	'ID' => $new_member,
																'first_name' => $keresztnev,
																'last_name' => $vezeteknev,
																'display_name' => $username,
															) );


				// Egyéb adatok mentése

				add_user_meta( $new_member, 'activated_account', '0', true );


				$hash = hash('sha256', wp_generate_password());

				add_user_meta( $new_member, 'registration_hash', $hash, true );

				// email küldés a felhasználónak a regisztrációs adatairól
				$email_option_template = get_email_template( 'regisztracio-visszaigazolasa' );

				// Replace
				$link = get_site_url() .'?action=reg_activ' . '&h=' . $hash;

				$body_option = stripslashes($email_option_template['msg']);
				$body_option = str_replace('%user_activation_link%', $link, $body_option);
				$body_option = str_replace('%nev%', $username, $body_option);
				$body_option_formatted = wpautop($body_option, 1);


				$to = $_POST['email_cim'];
				$subject = $email_option_template['subject'];
				$body = $body_option_formatted;
				$headers = array('Content-Type: text/html; charset=UTF-8','From: '. $email_option_template['sender_name'] .' <'. $email_option_template['sender'] .'>' );
				wp_mail( $to, $subject, $body, $headers );


				wp_redirect( get_permalink(PAGE_DIAK_REGISZTRACIO) .'?sikeres_regisztracio' ); die;
			} else {
				$reglog_form_info = array('error_msg' => 'Már létezik felhasználó a megadott email címmel');
			}
		}
	}
}


	//****************************************/
	// --------- GEN. NEW PASSWORD --------- //
	//****************************************/


// Új jelszó kérése (2 lépéses)
add_action('init', 'mt_uj_jelszo_letrehozasa');
function mt_uj_jelszo_letrehozasa() {
	global $reglog_form_info;

	// ( 2. lépés )
	if ( isset($_POST['new_password_submit']) && isset($_GET['action']) && ( $_GET['action'] == 'getnewpass' ) ) {

		// Új jelszó beállítása

		if ( isset($_GET['h']) && !empty($_GET['h']) ) {
			$user_id = "";
			$user_email = "";

			$user_query = new WP_User_Query( array( 'meta_key' => 'new_password_token', 'meta_value' => esc_attr(stripslashes($_GET['h'])) ) );
			$users = $user_query->get_results();

			$user_id = 0;
			if ( !empty( $users ) ) {
				foreach ($users as $key => $user) {
					$user_id = $user->ID;
					$user_email = $user->user_email;
					break;
				}
			}

			if ( $user_id > 0 ) {
				if ( isset($_POST['new_pass']) && !empty($_POST['new_pass']) &&
			 			 isset($_POST['new_pass2']) && !empty($_POST['new_pass2'])
					 ) {
					if ( $_POST['new_pass'] == $_POST['new_pass2'] ) {
						if ( empty(checkPassword($_POST['new_pass'])) ) {

							wp_set_password( $_POST['new_pass'], $user_id );
							delete_user_meta($user_id, 'new_password_token');

							wp_logout();
							wp_redirect( get_permalink(PAGE_ELFELEJTETT_JELSZO) .'?sikeres_jelszocsere' ); die;

						} else {
							$reglog_form_info = array('error_msg' => checkPassword($_POST['new_pass']) );
						}
					} else {
						$reglog_form_info = array('error_msg' => 'A megadott jelszvak nem egyeznek!');
					}
				} else {
					$reglog_form_info = array('error_msg' => 'Nincs kitöltve minden mező!');
				}
			} else {
				$reglog_form_info = array('error_msg' => 'A kapott kérelem hibás vagy felhasznált.');
			}
		} else {
			$reglog_form_info = array('error_msg' => 'Nem található a felhasználó azonosító!');
		}

	} else if ( isset($_POST['new_password_submit']) ) {

		// (1. lépés) Új jelszó igénylésének linkje elkülkéde emailben

		if ( !empty($_POST['user_email']) && is_email($_POST['user_email']) ) {
			if( email_exists($_POST['sign_up_email']) == false ) {
				$token = hash('sha256', wp_generate_password());
				$user = get_user_by('email', $_POST['user_email']);

				delete_user_meta( $user->ID, 'new_password_token' );
				update_user_meta( $user->ID, 'new_password_token', $token, true );
				$email_option_template = get_email_template( 'elfelejtett-jelszo' );

				// Replace
				$link = get_site_url() .'?action=getnewpass' .'&h='. $token;

				$body_option = stripslashes($email_option_template['msg']);
				$body_option = str_replace('%elfelejtett_jelszo_link%', $link, $body_option);
				$body_option = str_replace('%nev%', $user->display_name, $body_option);
				$body_option_formatted = wpautop($body_option, 1);


				$to = $_POST['user_email'];
				$subject = $email_option_template['subject'];
				$body = $body_option_formatted;
				$headers = array('Content-Type: text/html; charset=UTF-8','From: '. $email_option_template['sender_name'] .' <'. $email_option_template['sender'] .'>' );
				wp_mail( $to, $subject, $body, $headers );

				wp_redirect( get_permalink(PAGE_ELFELEJTETT_JELSZO) .'?sikeres_jelszocsere_kuldes' ); die;

			} else {
				$reglog_form_info = array('error_msg' => 'Nem található felhasználó a megadott email címmel.');
			}
		} else {
			$reglog_form_info = array('error_msg' => 'A megadott E-mail cím nem megfelelő!');
		}
	}
}
