<?php

add_shortcode('teacher_short_information_box', 'teacher_short_information_box_sc_function');
function teacher_short_information_box_sc_function($atts) {
	extract(shortcode_atts(array(
		'tanari_hirdetes_id' => '',
		'show_sample_data' => 'false',
	), $atts));
	if (empty($tanari_hirdetes_id) && ($show_sample_data == 'false') ) { return ""; }


	$post_id = (int) $tanari_hirdetes_id;
	$user_id = get_post_field ('post_author', $post_id);
	$user_obj = get_user_by('id', $user_id);


	$thumbnail = "";
	if ( has_post_thumbnail($post_id) ) {
		$thumbnail = wp_get_attachment_image( get_post_thumbnail_id($post_id), 'portfolio_squared', false, array( 'class' => 'attachment-staff_tn wp-post-image', 'width' => '570', 'height' => '570' ) );

		$array = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'portfolio_squared' );
		$squared_thumbnail = $array[0];
	}
	$square_profile_img_id = MultiPostThumbnails::get_post_thumbnail_id('tanarok_cpt', 'square_profile_img', $post_id);
	if ( intval($square_profile_img_id) > 0 ) {
		$thumbnail = wp_get_attachment_image( $square_profile_img_id, 'portfolio_squared', false, array( 'class' => 'attachment-staff_tn wp-post-image', 'width' => '570', 'height' => '570' ) );

		$array = wp_get_attachment_image_src( $square_profile_img_id, 'portfolio_squared' );
		$squared_thumbnail = $array[0];
	}
	if ( empty($thumbnail) ) {
		$thumbnail = wp_get_attachment_image( MEDIA_ID_EMPTY_PROFILE_IMG, 'portfolio_squared', false, array( 'class' => 'attachment-staff_tn wp-post-image', 'width' => '570', 'height' => '570' ) );

		$array = wp_get_attachment_image_src( MEDIA_ID_EMPTY_PROFILE_IMG, 'portfolio_squared' );
		$squared_thumbnail = $array[0];
	}


	// Tags
	$args = array('orderby' => 'count', 'order' => 'DESC', 'fields' => 'all');
	$helysegek_kateg = wp_get_post_terms($post_id, 'helysegek_kateg', $args);
	$tantargyak_kateg = wp_get_post_terms($post_id, 'tantargyak_kateg', $args);

	$tags_html = "";
	$tags_array = array();
	foreach ($helysegek_kateg as $key => $term_obj) {
		if ( $key < 2 ) {
			$tags_array []= '<span class="tag '. $term_obj->taxonomy .' ">'. $term_obj->name .'</span>';
		}
	}
	foreach ($tantargyak_kateg as $key => $term_obj) {
		if ( $key < 2 ) {
			$tags_array []= '<span class="tag '. $term_obj->taxonomy .' ">'. $term_obj->name .'</span>';
		}
	}

	if ( !empty($tags_array) ) {
		$tags_html = '<div class="tags">'.
										implode('', $tags_array).
									'</div>';
	}
	// ---

	$post_link = get_permalink($post_id);
	$excerpt = get_the_excerpt_by_id($post_id);
	$post_title = get_the_title($post_id);


	if ( $show_sample_data != 'false' ) {
		$tags_html = '<div class="tags"><span class="tag helysegek_kateg ">Békés megye</span><span class="tag helysegek_kateg ">Baranya megye</span><span class="tag tantargyak_kateg ">Általános iskolai tárgyak</span><span class="tag tantargyak_kateg ">Programozás</span></div>';
		$post_link = '#';
		$user_obj->display_name = 'Minta Név';
		$excerpt = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.';
		$post_title = 'Hirdetés megnevezése';
	}

	return
		'<div class="teacher_short_information_box staff_post rd_staff_p01 ajax_post sf_designer">
		   <div class="staff_post_ctn">

					<div class="member-photo s_effect">
						<div class="bw-wrapper">
							<a href="'. $post_link .'" target="_blank" style="background: url('. $squared_thumbnail .');" >'. $thumbnail .'</a>
						</div>
					</div>

					<div class="member-info">
						<h3><a href="'. $post_link .'" target="_blank">'. $post_title .'</a></h3>
						<div class="rating" data-rate-value="'. get_post_meta( $post_id, 'rating_current', true ) .'"></div>

						<div class="clearfix"></div>
						'. $tags_html .'
					</div>

					<div class="member_desc">
						<h6><a href="'. $post_link .'">'. $user_obj->display_name .'</a></h6>
						<div class="desc">'. $excerpt .'</div>

						<div class="empty_space" style="width: 100%;float: left; height: 0;""></div>
						<div class="clearfix"></div>
					</div>

		   </div>
		</div>';
}
