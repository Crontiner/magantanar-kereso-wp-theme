<?php

add_shortcode('magantanarok_terulet_szerint', 'magantanarok_terulet_szerint_sc_function');
function magantanarok_terulet_szerint_sc_function($atts) {
	global $form_info;
	$result_html = "";

	if ( !isset($_GET['magantanarok_terulet_szerint_sc_cache_update']) ) {
		$result_html = get_option( 'magantanarok_terulet_szerint_sc_cache' );
		return $result_html['html'];
	}


	// Hírdetések lekérése kategóriákkal

	$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																			'fields' => 'ids',
																			'posts_per_page' => -1,
																			'post_status' => 'publish',
																		));
	$tanarok_cpt_posts_array = $tanarok_cpt->posts;


	$args = array(
		'hide_empty' => false,
	);
	$tantargyak_kateg_terms = get_terms('tantargyak_kateg', $args);



	$users_and_terms_array = array();

	foreach ($tanarok_cpt_posts_array as $key => $post_id) {

		$hiredetes_id = $post_id;
		$user_id = get_post_field('post_author', $post_id);

		//if ( ervenyes_megrendelesek_lekerese($user_id) !== FALSE ) {

			$helysegek_kateg = wp_get_post_terms($post_id, 'helysegek_kateg', array("fields" => "all"));
			$tantargyak_kateg = wp_get_post_terms($post_id, 'tantargyak_kateg', array("fields" => "all"));

			// Helységek hozzáadása
			foreach ($helysegek_kateg as $key => $helyseg_term) {
				if ( $helyseg_term->parent > 0 ) {
					$parent_category = get_term_by('id', $helyseg_term->parent, $helyseg_term->taxonomy);
					$users_and_terms_array[$hiredetes_id]['helysegek'][] = $parent_category->slug;
				} else {
					$users_and_terms_array[$hiredetes_id]['helysegek'][] = $helyseg_term->slug;
				}
			}
			$users_and_terms_array[$hiredetes_id]['helysegek'] = array_values(array_filter(array_unique($users_and_terms_array[$hiredetes_id]['helysegek'])));
			asort($users_and_terms_array[$hiredetes_id]['helysegek']);


			// Tantárgyak hozzáadása
			foreach ($tantargyak_kateg as $key => $tantargy_term) {
				$has_child_terms = FALSE;

				foreach ($tantargyak_kateg_terms as $key => $tantargyak_kateg_term) {
					if ( $tantargyak_kateg_term->parent == $tantargy_term->term_id ) {
						$users_and_terms_array[$hiredetes_id]['tantargyak'][] = $tantargyak_kateg_term->slug;

						$has_child_terms = TRUE;
					}
				}

				if ( $has_child_terms === FALSE ) {
					$users_and_terms_array[$hiredetes_id]['tantargyak'][] = $tantargy_term->slug;
				}
			}
			$users_and_terms_array[$hiredetes_id]['tantargyak'] = array_values(array_filter(array_unique($users_and_terms_array[$hiredetes_id]['tantargyak'])));
			asort($users_and_terms_array[$hiredetes_id]['tantargyak']);
		//}
	}


	// szükséges helyek és tantárgyak összeszedése


	/*
	echo "<pre>";
	var_dump($users_and_terms_array);
	echo "</pre>";
	*/


	// helységek & tantárgyak hierarhia kialakítása

	$categories = get_terms('helysegek_kateg', array('hide_empty' => false));
	$helysegek_kateg_hierarchy = array();
	sort_terms_hierarchicaly($categories, $helysegek_kateg_hierarchy);

	$categories = get_terms('tantargyak_kateg', array('hide_empty' => false));
	$tantargyak_kateg_hierarchy = array();
	sort_terms_hierarchicaly($categories, $tantargyak_kateg_hierarchy);

	$final_hieararchy_array = array();

	foreach ($helysegek_kateg_hierarchy as $key => $term) {
		$final_hieararchy_array [$term->slug] = array();

		foreach ($tantargyak_kateg_hierarchy as $key2 => $term2) {
			$final_hieararchy_array [$term->slug][$term2->slug] = array();

			if ( isset($term2->children) && !empty($term2->children) ) {
				foreach ($term2->children as $key3 => $term3) {
					$final_hieararchy_array [$term->slug][$term2->slug][$term3->slug] = array();
				}
			}
		}
	}

	// Összeszedni a hirdetések darabszámát, belerakni a $final_hieararchy_array-be

	foreach ($users_and_terms_array as $hirdetes_id => $hirdetes_data) {
		foreach ($hirdetes_data['helysegek'] as $helyseg) {
			foreach ($hirdetes_data['tantargyak'] as $tantargy) {

				if (isset( $final_hieararchy_array[$helyseg][$tantargy] )) {
					if (is_array($final_hieararchy_array[$helyseg][$tantargy])
							&& isset(is_array($final_hieararchy_array[$helyseg][$tantargy])[0])
							&& is_array(is_array($final_hieararchy_array[$helyseg][$tantargy])[0])) {
						foreach ($final_hieararchy_array[$helyseg][$tantargy] as $k => $value) {
							$final_hieararchy_array[$helyseg][$tantargy][$k] []= $hirdetes_id;
						}
					} else {
						$final_hieararchy_array[$helyseg][$tantargy] []= $hirdetes_id;
					}
				} else {
					foreach ($final_hieararchy_array[$helyseg] as $keresett_targy_key => $keresett_targy) {
						if (isset( $final_hieararchy_array[$helyseg][$keresett_targy_key][$tantargy] )) {
							$final_hieararchy_array[$helyseg][$keresett_targy_key][$tantargy] []= $hirdetes_id;
						}
					}
				}

			}
		}
	}

	// Kiszedni a "$final_hieararchy_array" tömbből a felesleges adatokat

	foreach ($final_hieararchy_array as $helyseg => $hirdetes_targyak) {
		foreach ($hirdetes_targyak as $tantargy => $hirdetesek_vagy_altargyak) {
			if (!empty($hirdetesek_vagy_altargyak)) {
				foreach ($hirdetesek_vagy_altargyak as $targy => $hirdetes_ids) {
					if (is_string($targy) && empty($hirdetesek_vagy_altargyak[$targy])) {
						unset( $final_hieararchy_array[$helyseg][$tantargy][$targy] );
					}
				}
			}
		}
	}

	foreach ($final_hieararchy_array as $helyseg => $hirdetes_targyak) {
		foreach ($hirdetes_targyak as $tantargy => $hirdetesek_vagy_altargyak) {
			if (empty($hirdetesek_vagy_altargyak)) {
				unset($final_hieararchy_array[$helyseg][$tantargy]);
			}
		}
	}

	foreach ($final_hieararchy_array as $helyseg => $hirdetes_targyak) {
		if ( empty($hirdetes_targyak) ) {
			unset($final_hieararchy_array[$helyseg]);
		}
	}


	// Budapest átrakása első helyre
	if ( isset($final_hieararchy_array['budapest']) ) {
		$final_hieararchy_array_temp = array();

		$final_hieararchy_array_temp['budapest'] = $final_hieararchy_array['budapest'];
		unset($final_hieararchy_array['budapest']);

		if ( isset($final_hieararchy_array['pest-megye']) ) {
			$final_hieararchy_array_temp['pest-megye'] = $final_hieararchy_array['pest-megye'];
			unset($final_hieararchy_array['pest-megye']);
		}

		foreach ($final_hieararchy_array as $hely => $array) {
			$final_hieararchy_array_temp [$hely]= $array;
		}

		$final_hieararchy_array = $final_hieararchy_array_temp;
		unset($final_hieararchy_array_temp);
	}


	/*
	echo "<pre>";
	var_dump($final_hieararchy_array);
	echo "</pre>";
	die;
	*/

	// Kész tömb összeállítása HTML kimenetté

	/*
	Régi oldal linkfelépítése:
	- /magantanar/budapest
	- /informatikatanar/budapest
	- /programozas-tanar/budapest
	*/

	$home_url = get_home_url();

	$li = "";
	foreach ($final_hieararchy_array as $hely => $hely_array) {
		$helysegek_kateg_obj = get_term_by('slug', $hely, 'helysegek_kateg');

		$closed_class = "closed";
		if ( $hely == 'budapest' ) { $closed_class = ""; }

		$li .= '<li class="layer-0 '. $closed_class .' " data-slug="'. $hely .'"><a href="'. $home_url .'/magantanar/'. $hely .'"><span class="hely">'. $helysegek_kateg_obj->name .'<span class="db"><b>'. darabszam_lekerese($hely) .'</b></span></span></a>';

		$sub_li = "";
		if ( is_array($hely_array) && !empty($hely_array) ) {
			foreach ($hely_array as $tantargy => $tantargy_array) {
				$tantargyak_kateg_obj = get_term_by('slug', $tantargy, 'tantargyak_kateg');

				$sub_li .= '<li class="layer-1"><a href="'. $home_url .'/'. $tantargy .'/'. $hely .'"><span class="tantargy">'. $tantargyak_kateg_obj->name .'<span class="db"><b>'. darabszam_lekerese($hely, $tantargy) .'</b></span></span></a>';


					$sub_sub_li = "";
					if ( is_array($tantargy_array) && !empty($tantargy_array) && !is_numeric($tantargy_array[0]) ) {
						foreach ($tantargy_array as $tantargy2 => $tantargy_array2) {
							$tantargyak_kateg2_obj = get_term_by('slug', $tantargy2, 'tantargyak_kateg');

							$sub_sub_li .= '<li class="layer-2"><a href="'. $home_url .'/'. $tantargy2 .'/'. $hely .'"><span class="tantargy">'. $tantargyak_kateg2_obj->name .'<span class="db"><b>'. darabszam_lekerese($hely, $tantargy, $tantargy2) .'</b></span></span></a>';
							$sub_sub_li .= '</li>';
						}

						$sub_li .= '<ul class="layer-2">'. $sub_sub_li .'</ul>';
					}


				$sub_li .= '</li>';
			}

			$sub_li = '<ul class="layer-1">'. $sub_li .'</ul>';
		}

		$li .= $sub_li;
		$li .= '</li>';
	}


	$result_html = '<div class="magantanarok_terulet_szerint_sc">'. $result_html . '<ul class="layer-0">'. $li .'</ul>' .'</div>';

	update_option( 'magantanarok_terulet_szerint_sc_cache', array( 'timestamp' => strtotime(current_time('mysql')) ,'html' => $result_html ), false );
	return $result_html;
}
