<?php

add_shortcode('adataim_beallitasa', 'adataim_beallitasa_sc_function');
function adataim_beallitasa_sc_function($atts) {
	global $form_info;
	$result_html = "";
	$user_info = get_userdata(get_current_user_id());

	if ( isset($form_info['error_msg']) ) {
		$form_info = '<p>'. $form_info['error_msg'] .'</p>';
	} else {
		$form_info = "";
	}

	$user_phone = get_user_meta( $user_info->ID, 'phone', true );

	if ( empty($user_info->last_name) && empty($user_info->first_name) ) {
		$user_info = get_userdata(get_current_user_id());
		if ( !empty($user_info->display_name) ) {
			$user_info->last_name = $user_info->display_name;
		}
	}

	$result_html .=
	'<div class="adataim_beallitasa_sc">
	   <form class="form-horizontal mt-form-style" method="post" enctype="multipart/form-data">
				<div class="columns">
					<div class="column is-7">
						<h3>Kapcsolati, információs adatok</h3>
				    <br>
				    <div class="columns">
				       <div class="column">
				          <div class="field">
				             <label class="label" for="vnev">Vezetéknév:</label>
				             <div class="control">
				                <input type="text" value="'. $user_info->last_name .'" name="vnev" id="vnev">
				             </div>
				          </div>
				       </div>
				       <div class="column">
				          <div class="field">
				             <label class="label" for="knev">Keresztnév:</label>
				             <div class="control">
				                <input type="text" value="'. $user_info->first_name .'" name="knev" id="knev">
				             </div>
				          </div>
				       </div>
				    </div>';

	if ( in_array('tanar_role', jelenleg_bejelentkezett_roles_array()) ) {
		$result_html .= '
				    <div class="columns">
				       <div class="column">
				          <div class="field">
				             <label class="label" for="telefonszam">Telefonszámod:</label>
				             <div class="control">
				                <input type="text" value="'. $user_phone .'" name="telefonszam" id="telefonszam">
				             </div>
				          </div>
				       </div>
				       <div class="column">
				          <div class="field">
				             <label class="label" for="weblapod">Weblapod (ha van):</label>
				             <div class="control">
				                <input type="text" value="'. $user_info->user_url .'" name="weblapod" id="weblapod">
				             </div>
				          </div>
				       </div>
				    </div>';
		}
	$result_html .= '
					</div>
					<div class="column">
						<h3>Amennyiben szeretnéd, itt megváltoztathatod a jelszót</h3>
				    <br>
				    <div class="columns">
				       <div class="column">
				          <div class="field">
				             <label class="label" for="regi_jelszo">Régi jelszó:</label>
				             <div class="control">
				                <input type="password" value="" name="regi_jelszo" id="regi_jelszo">
				             </div>
				          </div>
									<div class="field">
				             <label class="label" for="uj_jelszo">Új jelszó:</label>
				             <div class="control">
				                <input type="password" value="" name="uj_jelszo" id="uj_jelszo">
				             </div>
				          </div>
				          <div class="field">
				             <label class="label" for="uj_jelszo2">Új jelszó megerősítése:</label>
				             <div class="control">
				                <input type="password" value="" name="uj_jelszo2" id="uj_jelszo2">
				             </div>
				          </div>
				       </div>
				    </div>

						<div class="form-info">'. $form_info .'</div>
						<div class="clearfix"></div>

				    <div class="columns">
							<div class="column"></div>
							<div class="column">
								<input type="submit" name="adataim_beallitasa_sc_submit" value="Mentés">
							</div>
				    </div>
					</div>
				</div>
	   </form>
	</div>';

	return $result_html;
}


add_action('init', 'mt_save_adataim_beallitasa_sc');
function mt_save_adataim_beallitasa_sc() {
	if ( isset($_POST['adataim_beallitasa_sc_submit']) ) {
		global $form_info;

		$vnev = esc_attr(stripslashes($_POST['vnev']));
		$knev = esc_attr(stripslashes($_POST['knev']));
		$telefonszam = esc_attr(stripslashes($_POST['telefonszam']));
		$weblapod = esc_attr(stripslashes($_POST['weblapod']));

		$regi_jelszo = esc_attr(stripslashes($_POST['regi_jelszo']));
		$uj_jelszo = esc_attr(stripslashes($_POST['uj_jelszo']));
		$uj_jelszo2 = esc_attr(stripslashes($_POST['uj_jelszo2']));

		$updated_info_array = array( 'ID' => get_current_user_id() );

		if ( !empty($vnev) ) { $updated_info_array['last_name']= $vnev; }
		if ( !empty($knev) ) { $updated_info_array['first_name']= $knev; }
		if ( !empty($vnev) && !empty($knev) ) { $updated_info_array['display_name']= $vnev .' '. $knev; }
		if ( !empty($weblapod) ) { $weblapod = esc_url($weblapod); }

		update_user_meta(get_current_user_id(), 'phone', $telefonszam);

		$updated_info_array['user_url']= $weblapod;
		$user_id = wp_update_user($updated_info_array);

		if ( intval($user_id) > 0 ) { }
		else {
			$form_info = array('error_msg' => 'Hiba történt az adatok mentése közben!');
		}


		// Jelszó megváltoztatása

		if ( !empty($regi_jelszo) && !empty($uj_jelszo) && !empty($uj_jelszo2) ) {
			$user = get_user_by( 'id', get_current_user_id() );
			if ( $user && wp_check_password( $regi_jelszo, $user->data->user_pass, $user->ID) ) {

				if ( $uj_jelszo == $uj_jelszo2 ) {
					if ( empty(checkPassword($uj_jelszo)) ) {

						wp_update_user( array( 'ID' => $user->ID, 'user_pass' => $uj_jelszo ) );
						$form_info = array('error_msg' => 'Jelszavát sikeresen lecseréltük!');

					} else {
						$form_info = array('error_msg' => checkPassword($uj_jelszo) );
					}
				} else {
					$form_info = array('error_msg' => 'A megadott jelszvak nem egyeznek!');
				}
			} else {
				$form_info = array('error_msg' => 'A beírt régi jelszava nem megfelelő!');
			}
		}

	}
}
