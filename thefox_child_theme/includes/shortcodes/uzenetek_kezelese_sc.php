<?php

add_shortcode('uzenetek_kezelese', 'uzenetek_kezelese_sc_function');
function uzenetek_kezelese_sc_function($atts) {
	global $form_info, $wp_query;
	$paged = $wp_query->query_vars['paged'];
	$result_html = "";
	$page_uzenetek_url = get_permalink(PAGE_UZENETEK);
	$message_html = "";
	$reply_html = "";

	// Validation
	$get_msg_post_id = "";
	if ( isset($_GET['msg']) ) {
		if ( (intval($_GET['msg']) > 0) && (get_post_type(intval($_GET['msg'])) == 'uzenetek_cpt' ) ) {
			if ( 	(get_current_user_id() == get_post_meta(intval($_GET['msg']), 'msg_to_user_id', true)) ||
						(get_current_user_id() == get_post_field('post_author', intval($_GET['msg'])))
				) {
				$get_msg_post_id = intval($_GET['msg']);
			}
		}
	}

	$get_reply_post_id = "";
	if ( isset($_GET['reply']) ) {
		if ( (intval($_GET['reply']) > 0) && (get_post_type(intval($_GET['reply'])) == 'uzenetek_cpt' ) ) {
			if ( get_current_user_id() == get_post_meta(intval($_GET['reply']), 'msg_to_user_id', true) ) {
				$get_reply_post_id = intval($_GET['reply']);
			}
		}
	}


	// Get messages - tables

	$beerkezett_uzenetek_cpt = new WP_Query(array( 'post_type' => 'uzenetek_cpt',
																			'fields' => 'ids',
																			'posts_per_page' => 10,
																			'paged' => $paged,
																			'meta_query' => array(
																					'relation' => 'AND',
																					'msg_to_meta' => array(
																						'key' => 'msg_to_user_id',
																						'value' => get_current_user_id(),
																						'compare' => '=',
																					),
																			),
																		));
	$uzenetek_cpt_posts_array = $beerkezett_uzenetek_cpt->posts;


	$bejovo_uzenetek_tr = array();
	foreach ($uzenetek_cpt_posts_array as $key => $post_id) {
		$all_meta = get_post_meta($post_id);

		$subject = $all_meta['subject'][0];
		$viewed = (int) $all_meta['viewed'][0];
		$sender_user_id = (int) get_post_field('post_author', $post_id);
		$sender_user_obj = get_user_by('id', $sender_user_id);

		if ( $viewed == 1 ) { $viewed = '<i class="fa fa-eye" aria-hidden="true"></i>'; }
		else { $viewed = '<i class="fa fa-eye-slash" aria-hidden="true"></i>'; }

		$active_class = "";
		/*
		if ( $get_msg_post_id == $post_id ) { $active_class = "is-selected"; }
		else if ( $get_reply_post_id == $post_id ) { $active_class = "is-selected"; }
		*/

		$bejovo_uzenetek_tr []=
			'<tr class="'. $active_class .'">
				<td>'. get_the_date('Y.m.d H:i', $post_id ) .'</td>
				<td>'. $sender_user_obj->display_name .'</td>
				<td>'. $subject .'</td>
				<td><a href="'. add_query_arg( array( 'msg' => $post_id, ), $page_uzenetek_url ) .'" class="button is-link is-outlined">Megnézem</a></td>
				<td><a href="'. add_query_arg( array( 'reply' => $post_id, ), $page_uzenetek_url ) .'" class="button is-link is-outlined">Válasz</a></td>
				<td>'. $viewed .'</td>
			</tr>';
	}


	// Sent messages - tables

	$elkuldott_uzenetek_cpt = new WP_Query(array( 'post_type' => 'uzenetek_cpt',
																			'fields' => 'ids',
																			'paged' => $paged,
																			'posts_per_page' => 10,
																			'author' => get_current_user_id(),
																		));
	$uzenetek_cpt_posts_array = $elkuldott_uzenetek_cpt->posts;

	$kiment_uzenetek_tr = array();
	foreach ($uzenetek_cpt_posts_array as $key => $post_id) {
		$all_meta = get_post_meta($post_id);

		$subject = $all_meta['subject'][0];
		$recipient_user_id = (int) $all_meta['msg_to_user_id'][0];
		$recipient_user_obj = get_user_by('id', $recipient_user_id);

		$kiment_uzenetek_tr []=
			'<tr>
				<td>'. get_the_date('Y.m.d H:i', $post_id ) .'</td>
				<td>'. $recipient_user_obj->display_name .'</td>
				<td>'. $subject .'</td>
				<td><a href="'. add_query_arg( array( 'msg' => $post_id, ), $page_uzenetek_url ) .'" class="button is-link is-outlined">Megnézem</a></td>
			</tr>';
	}


	// Get selected message - modal

	if ( intval($get_msg_post_id) > 0 ) {

		$all_meta = get_post_meta($get_msg_post_id);
		$sender_user_id = (int) get_post_field('post_author', $get_msg_post_id);
		$sender_user_obj = get_user_by('id', $sender_user_id);

		update_post_meta( $get_msg_post_id, 'viewed', 1 );

		$message_html = '
		<div id="modal-read_message" class="modal">
			<div class="modal-background"></div>
			<div class="modal-card">
				<header class="modal-card-head">
					<p class="modal-card-title">Üzenet olvasása</p>
					<button class="delete" aria-label="close"></button>
				</header>
				<section class="modal-card-body">

					<div class="columns">
						<div class="column">
							<div class="field">
							  <label class="label">Név:</label>
							  <div class="control">
							    <input type="text" value="'. $sender_user_obj->display_name .'" disbale />
							  </div>
							</div>
						</div>
						<div class="column">
							<div class="field">
							  <label class="label">Tárgy:</label>
							  <div class="control">
							    <input type="text" value="'. $all_meta['subject'][0] .'" disbale />
							  </div>
							</div>
						</div>
					</div>

					<div class="field">
					  <label class="label">Üzenet:</label>
					  <div class="control">
					    <textarea disable>'. get_the_content_by_id($get_msg_post_id) .'</textarea>
					  </div>
					</div>

					<div class="post-date">'. get_the_date('Y.m.d H:i', $get_msg_post_id ) .'</div>
				</section>
				<footer class="modal-card-foot">
					<a href="'. add_query_arg( array( 'reply' => $get_msg_post_id, ), $page_uzenetek_url ) .'" class="button is-success"><i class="fa fa-paper-plane" aria-hidden="true"></i> Válasz</a>
					<button class="button cancel"><i class="fa fa-ban" aria-hidden="true"></i> Bezárás</button>
				</footer>
			</div>
		</div>
		';
	}


	// Send message - modal

	if ( intval($get_reply_post_id) > 0 ) {

		$all_meta = get_post_meta($get_reply_post_id);
		$sender_user_id = (int) get_post_field('post_author', $get_reply_post_id);
		$sender_user_obj = get_user_by('id', $sender_user_id);

		$reply_html = '
		<div id="modal-send_message" class="modal" data-msg-id="'. $get_reply_post_id .'">
			<div class="modal-background"></div>
			<div class="modal-card">
				<header class="modal-card-head">
					<p class="modal-card-title">Üzenet küldése</p>
					<button class="delete" aria-label="close"></button>
				</header>
				<section class="modal-card-body">

				<div class="field is-horizontal">
				  <div class="field-label is-normal">
				    <label class="label">Címzett:</label>
				  </div>
				  <div class="field-body">
				    <div class="field">
				      <p class="control">
				        <input type="text" value="'. $sender_user_obj->display_name .'" name="sender_name" readonly disabled />
				      </p>
				    </div>
				  </div>
				</div>

				<div class="field is-horizontal">
				  <div class="field-label is-normal">
				    <label class="label">Tárgy:</label>
				  </div>
				  <div class="field-body">
				    <div class="field">
				      <p class="control">
				        <input type="text" value="" name="subject">
				      </p>
				    </div>
				  </div>
				</div>

				<div class="field is-horizontal">
				  <div class="field-label is-normal">
				    <label class="label">Üzeneted:</label>
				  </div>
				  <div class="field-body">
				    <div class="field">
							<textarea required name="msg"></textarea>
				    </div>
				  </div>
				</div>

				<div class="alert hidden rd_small_alert"><div class="rd_alert_content"></div></div>
			</section>
			<footer class="modal-card-foot">
				<button class="button is-success"><i class="fa fa-paper-plane" aria-hidden="true"></i> Elküldöm</button>
				<button class="button cancel"><i class="fa fa-ban" aria-hidden="true"></i> Mégsem</button>
			</footer>
		</div>
	</div>';
	}


	$result_html .=
	'<div class="columns">
		<div class="column is-12">

			<h4>Bejövő üzenetek</h4>
			<table class="table beerkezett_uzenetek is-bordered is-striped is-hoverable is-fullwidth">
			  <thead>
			    <tr>
			      <th>Dátum</th>
			      <th>Feladó</th>
			      <th>Tárgy</th>
			      <th>Üzenet megtekintése</th>
						<th>Válasz írása</th>
			      <th>Megnézett</th>
			    </tr>
			  </thead>
				<tfoot>
					<tr>
						<th colspan="6">
							<div class="pagination_content">'. wp_pagenavi(array("query" => $beerkezett_uzenetek_cpt, 'echo' => false)) .'</div>
						</th>
					</tr>
				</tfoot>
			  <tbody>
					'. implode('', $bejovo_uzenetek_tr) .'
			  </tbody>
			</table>

		</div>
	</div>';

	$result_html .=
	'<div class="columns">
		<div class="column is-12">

			<h4>Elküldött üzenetek</h4>
			<table class="table kiment_uzenetek is-bordered is-striped is-hoverable is-fullwidth">
			  <thead>
			    <tr>
			      <th>Dátum</th>
			      <th>Címzett</th>
			      <th>Tárgy</th>
			      <th>Üzenet megtekintése</th>
			    </tr>
			  </thead>
				<tfoot>
					<tr>
						<th colspan="6">
							<div class="pagination_content">'. wp_pagenavi(array("query" => $elkuldott_uzenetek_cpt, 'echo' => false)) .'</div>
						</th>
					</tr>
				</tfoot>
			  <tbody>
					'. implode('', $kiment_uzenetek_tr) .'
			  </tbody>
			</table>

		</div>
	</div>';


	$result_html .= $message_html;
	$result_html .= $reply_html;

	return '<div class="uzenetek_kezelese_sc">'. $result_html .'</div>';
}
