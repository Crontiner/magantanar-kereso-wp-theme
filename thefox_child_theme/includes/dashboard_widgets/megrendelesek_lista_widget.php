<?php

function nem_jovahagyott_megrendelesek_lista() {
	$megrendelesek_cpt = new WP_Query(array(
																			'post_type' => 'megrendelesek_cpt',
																			'fields' => 'ids',
																			'posts_per_page' => -1,
																			'order' => 'DESC',
																			//'orderby' => 'meta_value_num',
																			//'meta_key' => 'rendeles_idopontja',
																			'post_status' => 'draft',
																		));
	$megrendelesek_posts_array = $megrendelesek_cpt->posts;

	$excel_body_array = array();
	foreach ($megrendelesek_posts_array as $key => $post_id) {

		$post_author_id = get_post_field( 'post_author', $post_id ); // <-- megrendelő USER ID
		$user_obj = get_user_by('id', $post_author_id);
		$rendeles_idopontja_timestamp = get_post_meta( $post_id, 'rendeles_idopontja', true );


		// Hirdetés lekérése
		$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																				'fields' => 'ids',
																				'posts_per_page' => 1,
																				'post_status' => 'any',
																				'author' => $user_obj->ID,
		));
		$tanarok_posts_array = $tanarok_cpt->posts;
		$hirdetes_id = (int) $tanarok_posts_array[0];

		$hirdetes_link = get_edit_post_link($hirdetes_id);
		$megrendeles_link = get_edit_post_link($post_id);
		$user_profile_link = get_edit_user_link($user_obj->ID);

		if ( !empty($hirdetes_link) ) {
			$hirdetes_link = '<a target="_blank" class="button button-small" href="'. $hirdetes_link .'">Hirdetési adatok</a>';
		}
		if ( !empty($megrendeles_link) ) {
			$megrendeles_link = '<a target="_blank" class="button button-small" href="'. $megrendeles_link .'">Megrendelési adatok</a>';
		}
		if ( !empty($user_profile_link) ) {
			$user_profile_link = '<a target="_blank" class="button button-small" href="'. $user_profile_link .'">Profil oldal</a>';
		}

		$excel_body_array []=
			array(
					$user_obj->display_name,
					$user_obj->user_email,
					date('Y-m-d H:i:s', $rendeles_idopontja_timestamp),
					$hirdetes_link,
					$megrendeles_link,
					$user_profile_link,
				);
	}

	$excel_header_array =
		array(
				'Név',
				'E-mail cím',
				'Utolsó megrendelés dátuma',
				'Hirdetési adatok',
				'Előfizetési adatok',
				'Felhasználói adatlap'
			);

	$final_data = array();
	$final_data []= $excel_header_array;

	foreach ($excel_body_array as $key => $jelentkezes_adatok) {
		$final_data []= $jelentkezes_adatok;
	}
	return $final_data;
}


function nem_jovahagyott_megrendelesek_lista_widget_function() {
	$final_data = nem_jovahagyott_megrendelesek_lista();

	$tr = "";
	foreach ($final_data as $key => $row_array) {
		$tr .= '<tr>';

		if ( $key == 0 ) {
			foreach ($row_array as $key2 => $value2) {
				$tr .= '<th>'. $value2 .'</th>';
			}
		} else {
			foreach ($row_array as $key2 => $value2) {
				$tr .= '<td>'. $value2 .'</td>';
			}
		}

		$tr .= '</tr>';
	}

	echo '<table>'. $tr .'</table>';
}
