<?php

date_default_timezone_set("Europe/Budapest");

/*******************************/
/*      GLOBAL VARIABLES       */
/*******************************/

define( 'PAGE_BEJELENTKEZES', 7460 );
define( 'PAGE_REGISZTRACIO', 7463 );
define( 'PAGE_TANAR_REGISZTRACIO', 7579 );
define( 'PAGE_DIAK_REGISZTRACIO', 7582 );
define( 'PAGE_ELFELEJTETT_JELSZO', 7577 );
define( 'PAGE_HIRDETESEM', 7545 );
define( 'PAGE_UZENETEK', 7543 );
define( 'PAGE_ERTEKELESEIM', 7568 );
define( 'PAGE_UJ_HELY_ES_TARGY', 7552 );
define( 'PAGE_SAJAT_ADATLAPOM', 7549 );
define( 'PAGE_HIRDETES_AKTIVALASA', 7547 );
define( 'PAGE_SAJAT_ADATLAPOM', 7549 );

define( 'HIRDETESHOSSZABBITAS_KEDVEZMENY', 10 ); // 10%

define( 'POST_ID_ELOFIZETES', 7710 );
define( 'POST_ID_KIEMELES_A_TALALATI_LISTA_ELEJERE', 7711 );
define( 'POST_ID_FOKIEMELES_AZ_OLDALAK_TETEJERE_A_KEPVALTOBA', 7712 );

define( 'MEDIA_ID_EMPTY_PROFILE_IMG', 10357 );

define( 'TEMPLATE_DIRECTORY_URI', get_template_directory_uri() );
define( 'TEMPLATE_DIRECTORY', get_template_directory() );
define( 'GET_STYLESHEET_DIR', get_stylesheet_directory() );
define( 'CHILD_TEMPLATE_DIRECTORY_URI', get_stylesheet_directory_uri() ); // http://mt.auretto.works/wp-content/themes/thefox_child_theme

/**/



/************************/
/*       INCLUDES       */
/************************/


$mt_includes_array = array(

	// Shortcodes
	'/includes/shortcodes/kiemelt_tanarok_sc.php',
	'/includes/shortcodes/teacher_short_information_box_sc.php',
	'/includes/shortcodes/tanarok_lista_sc.php',
	'/includes/shortcodes/reglog_form_sc.php',
	'/includes/shortcodes/adataim_beallitasa_sc.php',
	'/includes/shortcodes/reszletes_kereso_sc.php',
	'/includes/shortcodes/hirdetes_letrehozasa.php',
	'/includes/shortcodes/magantanarok_terulet_szerint_sc.php',
	'/includes/shortcodes/elofizetes_form_sc.php',
	'/includes/shortcodes/uzenetek_kezelese_sc.php',
	'/includes/shortcodes/ertekeim_listazasa.php',
	'/includes/shortcodes/sajat_adatlapom.php',

	// Custom Post Types
	'/includes/custom_post_types/megrendelesek_cpt.php',
	'/includes/custom_post_types/tanarok_cpt.php',

	// WP Dashboard
	'/includes/dashboard_widgets/megrendelesek_lista_widget.php',

);
foreach ($mt_includes_array as $key => $file_location) {
	$file_location = GET_STYLESHEET_DIR . $file_location;
	if (file_exists( $file_location )) { include $file_location; }
}

/**/


/**************************/
/*    SETTINGS FOR WP     */
/**************************/

//add_filter('show_admin_bar', '__return_false');

/**/


/********************/
/*    REDIRECTS     */
/********************/

add_action ('template_redirect', 'mt_page_redirects');
function mt_page_redirects() {
	global $post;


	if ( $post->ID == PAGE_SAJAT_ADATLAPOM ) {
		if (is_user_logged_in() && (in_array('tanar_role', jelenleg_bejelentkezett_roles_array()))) {

			$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																					'fields' => 'ids',
																					'posts_per_page' => 1,
																					'post_status' => 'publish',
																					'author' => get_current_user_id(),
																		));
			$tanarok_cpt_posts_array = $tanarok_cpt->posts;
			if ( intval($tanarok_cpt_posts_array[0]) > 0 ) {
				wp_redirect( get_permalink(intval($tanarok_cpt_posts_array[0])) ); exit;
			} else {
				wp_redirect( home_url() ); exit;
			}
		}
	}


	// kijelentkezés
	if ( isset($_GET['logout']) ) {
		wp_logout();
		wp_redirect( home_url() ); exit;
	}

	// Reg aktiválás átirányítása
	if ( isset($_GET['action']) && ($_GET['action'] =='reg_activ') && ($post->ID != PAGE_BEJELENTKEZES) ) {
		wp_redirect(add_query_arg( array( 'action' => 'reg_activ', 'h' => $_GET['h'] ), get_permalink(PAGE_BEJELENTKEZES) ));
		exit;
	}

	// Új jelszó aktiválás átirányítása
	if ( isset($_GET['action']) && ($_GET['action'] =='getnewpass') && ($post->ID != PAGE_ELFELEJTETT_JELSZO) ) {
		wp_redirect(add_query_arg( array( 'action' => 'getnewpass', 'h' => $_GET['h'] ), get_permalink(PAGE_ELFELEJTETT_JELSZO) ));
		exit;
	}

	// Csak tanárok nézhetik meg ezeket az oldalakat

	if (is_user_logged_in() && (in_array('subscriber', jelenleg_bejelentkezett_roles_array()))) {
		if ( 	($post->ID == PAGE_UJ_HELY_ES_TARGY) ||
					($post->ID == PAGE_SAJAT_ADATLAPOM) ||
					($post->ID == PAGE_HIRDETES_AKTIVALASA) ||
					($post->ID == PAGE_HIRDETESEM) ||
					($post->ID == PAGE_HIRDETES_AKTIVALASA)
				) { wp_redirect( home_url() ); exit; }
	}

	// Csak diákok nézhetik meg ezeket az oldalakat
	if (is_user_logged_in() && (in_array('tanar_role', jelenleg_bejelentkezett_roles_array())) ) {
		if ($post->ID == PAGE_ERTEKELESEIM) { wp_redirect( home_url() ); exit; }
	}
}

add_action('admin_init', 'mt_admin_page_redirects');
function mt_admin_page_redirects() {

	// aki nem administrator az ne lássa a WP admin-t
	if ( get_current_user_role() != 'administrator' && ! (defined( 'DOING_AJAX' ) && DOING_AJAX) ) {
		wp_redirect( home_url() ); exit;
	}
}

/**/


/**************************/
/*         Menu           */
/**************************/

add_action ('init', 'mt_create_menus');
function mt_create_menus() {
	register_nav_menus ( array(
		'main_menu_logged_in_teacher' => 'Főmenü - (Bejelentkezve tanárként)',
		'main_menu_logged_in_student' => 'Főmenü - (Bejelentkezve diákként)',
		'main_menu_logged_out' => 'Főmenü - (Kijelentkezve)',
	) );
}

/**/


/********************************/
/*         CSS & JQUERY         */
/********************************/

add_action( 'admin_enqueue_scripts', 'mt_theme_enqueue_admin_styles' );
function mt_theme_enqueue_admin_styles() {
	wp_enqueue_style( 'admin_css', get_stylesheet_directory_uri() . '/css/admin.css');
	wp_enqueue_script( 'admin_js', get_stylesheet_directory_uri() .'/js/admin.js' );
}

add_action( 'wp_enqueue_scripts', 'mt_theme_enqueue_styles' );
function mt_theme_enqueue_styles() {
  global $rd_data;

	wp_register_style('googlefonts', 'https://fonts.googleapis.com/css?family=Exo:800,900,400,300', array(),'2','all');
	wp_enqueue_style('googlefonts');

	//wp_enqueue_style( 'dropzone_css', get_stylesheet_directory_uri() .'/lib/dropzone/dropzone.css');
	wp_enqueue_style( 'select2_css', get_stylesheet_directory_uri() .'/lib/select2/dist/css/select2.min.css');
	wp_enqueue_style( 'icheck_css', get_stylesheet_directory_uri() .'/lib/icheck/skins/minimal/blue.css');
	wp_enqueue_style( 'bulma_css', get_stylesheet_directory_uri() . '/lib/bulma/bulma.min.css' );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'style_end', get_template_directory_uri() . '/style_end.css' );


	if($rd_data['rd_responsive']== true){
		if ($rd_data['rd_nav_type'] !== 'nav_type_19' && $rd_data['rd_nav_type'] !== 'nav_type_19_f' ){
			wp_enqueue_style('media-queries',  get_template_directory_uri() . '/media-queries_wide.css');
		}
		else {
			wp_enqueue_style('media-queries',  get_template_directory_uri() . '/media-queries_edge_nav.css');
		}
	}


	//wp_enqueue_script( 'dropzone_js', get_stylesheet_directory_uri() .'/lib/dropzone/dropzone.js', array('jquery') );
	wp_enqueue_script( 'select2_js', get_stylesheet_directory_uri() .'/lib/select2/dist/js/select2.min.js', array('jquery') );
	wp_enqueue_script( 'tinymce_js', get_stylesheet_directory_uri() .'/lib/tinymce/js/tinymce/tinymce.min.js', array('jquery') );
	wp_enqueue_script( 'icheck_js', get_stylesheet_directory_uri() .'/lib/icheck/icheck.min.js', array('jquery') );
	wp_enqueue_script( 'rater_js', get_stylesheet_directory_uri() .'/lib/auxiliary-rater/rater.min.js', array('jquery') );
	wp_enqueue_script( 'scrollto_js', get_stylesheet_directory_uri() .'/lib/jquery.scrollTo.min.js', array('jquery') );


	wp_enqueue_script( 'site_js', get_stylesheet_directory_uri() .'/js/site.js', array('jquery') );
	wp_localize_script( 'site_js', 'mt', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
}

add_action( 'get_footer', 'mt_add_footer_styles' );
function mt_add_footer_styles() {
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() .'/css/style.css', array( 'style' ));
};

/**/


/*************************/
/*    OTHER FUNCTIONS    */
/*************************/

function ervenyes_megrendelesek_lekerese($user_id = "") {
	if ( intval($user_id) > 0 ) { $user_id = intval($user_id); }
	else { return FALSE; }

	$megrendelesek_cpt = new WP_Query(array( 	'post_type' => 'megrendelesek_cpt',
																						'fields' => 'ids',
																						'posts_per_page' => -1,
																						'post_status' => 'publish',
																						'author' => $user_id,
																						'meta_query' => array(	'relation' => 'AND',
																																		'meddig_ervenyes' => array(
																																			'key' => 'meddig_ervenyes',
																																			'value' => strtotime(current_time('mysql')),
																																			'compare' => '>',
																																		),
																																		'meddig_ervenyes2' => array(
																																			'key' => 'meddig_ervenyes',
																																			'value' => '',
																																			'compare' => '!=',
																																		),
																																		'meddig_ervenyes3' => array(
																																			'key' => 'meddig_ervenyes',
																																			'value' => NULL,
																																			'compare' => '!=',
																																		),
																																	),
																					));
	$megrendelesek_cpt_posts_array = $megrendelesek_cpt->posts;

	if ( !empty($megrendelesek_cpt_posts_array) ) {
		return $megrendelesek_cpt_posts_array;
	}
	return FALSE;
}

function get_user_id_by_display_name( $display_name = "" ) {
	if ( empty($display_name) ) { return FALSE; }

	$users = get_users( array(
		'meta_key' => 'display_name',
		'meta_value' => $display_name
	) );

	foreach ( $users as $user ) { return $user->ID; }
	return FALSE;
}

function dimox_breadcrumbs() {
	global $rd_data;
  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show

  $delimiter = '<i class="fa-angle-right crumbs_delimiter"></i>'; // delimiter between crumbs

  $home = 'Főoldal'; // text for the 'Home' link

  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show


  global $post;

  $homeLink = home_url();


  if (is_home() || is_front_page()) {

    if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';

  } else {

    echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';

    if ( is_category() ) {

      global $wp_query;

      $cat_obj = $wp_query->get_queried_object();

      $thisCat = $cat_obj->term_id;

      $thisCat = get_category($thisCat);

      $parentCat = get_category($thisCat->parent);

      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));

      echo '<span>' . __('Archive by category', 'thefoxwp').' "' . single_cat_title('', false) . '"' . '</span>';

    } elseif ( is_search() ) {

      echo '<span>'.__('Search results for', 'thefoxwp').' "' . get_search_query() . '"' . '</span>';

    } elseif ( is_day() ) {

      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';

      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';

      echo '<span>' . get_the_time('d') . '</span>';

    } elseif ( is_month() ) {

      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';

      echo '<span>' . get_the_time('F') . '</span>';

    } elseif ( is_year() ) {

      echo '<span>' . get_the_time('Y') . '</span>';

    } elseif ( is_single() && !is_attachment() ) {

      if ( get_post_type() != 'post' ) {

        $post_type = get_post_type_object(get_post_type());

        $slug = $post_type->rewrite;

        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $slug['slug'] . '</a>';

        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . '<span>' . get_the_title() . '</span>';

      } else {

        $cat = get_the_category(); $cat = $cat[0];

		is_wp_error( $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ') ) ? '' : $cats;

        if ($showCurrent == 0) $cats = preg_replace("/^(.+)\s$delimiter\s$/", "$1", $cats);

        echo !empty( $cats ) ? $cats : '';


        if ($showCurrent == 1) echo '<span>' . get_the_title() . '</span>';

      }

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {

      $post_type = get_post_type_object(get_post_type());

      echo '<span>' . $post_type->labels->singular_name . '</span>';

    } elseif ( is_attachment() ) {

      $parent = get_post($post->post_parent);

      $cat = get_the_category($parent->ID); $cat = $cat[0];
	  if ($cat || !is_wp_error($cat) ){
	      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
	 }

      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');

      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';

      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . '<span>' . get_the_title() . '</span>';

    } elseif ( is_page() && !$post->post_parent ) {

      if ($showCurrent == 1) echo '<span>' . get_the_title() . '</span>';

    } elseif ( is_page() && $post->post_parent ) {

      $parent_id  = $post->post_parent;

      $breadcrumbs = array();

      while ($parent_id) {

      $page = get_page($parent_id);

      $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a> ' . $delimiter;

      $parent_id  = $page->post_parent;

      }

      $breadcrumbs = array_reverse($breadcrumbs);

      foreach ($breadcrumbs as $crumb) echo !empty( $crumb ) ? $crumb : '';

      if ($showCurrent == 1) echo '<span>' . get_the_title() . '</span>';

    } elseif ( is_tag() ) {

      echo '<span>' .__('Search results for', 'thefoxwp').' "' . single_tag_title('', false) . '"' . '</span>';

    } elseif ( is_author() ) {

      global $author;

      $userdata = get_userdata($author);

      echo '<span>' .__('Articles posted by', 'thefoxwp').' ' . $userdata->display_name . '</span>';

    } elseif ( is_404() ) {

      echo '<span>' .__('Error 404', 'thefoxwp').'</span>';

    }

    if ( get_query_var('paged') ) {

      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';

      echo __('Page','thefoxwp') . ' ' . get_query_var('paged');

      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
    echo '</div>';
  }
}

function strpos_arr($haystack = "", $needle = "") {
	if(!is_array($needle)) $needle = array($needle);
	foreach($needle as $what) {
		if(($pos = strpos($haystack, $what))!==false) return $pos;
	}
	return false;
}

function remove_links_from_text($text = "") {
	return preg_replace('#<a.*?>(.*?)</a>#i', '\1', $text);
}

function minusPercent($n, $p) {
	return $n - ($n * ($p / 100));
}

// Check: Még nem járt le a megrendelés
function megrendeles_ervenyesseg($megrendeles_cpt_post_id) {
	if ( ($megrendeles_cpt_post_id > 0) && (get_post_type($megrendeles_cpt_post_id) == 'megrendelesek_cpt') ) {
		//$mettol_ervenyes = get_post_meta( $post_id, 'mettol_ervenyes', true );
		$meddig_ervenyes = get_post_meta( $megrendeles_cpt_post_id, 'meddig_ervenyes', true );

		if ( $meddig_ervenyes > 0 ) {
			if ( strtotime(current_time('mysql')) < $meddig_ervenyes) {
				return "ervenyes";
			} else {
				return "lejart";
			}
		}
	}
	return "";
}

function mt_money_format($price) {
	$formatter = new NumberFormatter('hu_HU',  NumberFormatter::CURRENCY);
	return $formatter->formatCurrency($price, 'HUF');
}


function darabszam_lekerese($hely = "", $tantagy = "", $altantagy = "") {

	$helysegek_kateg = "";
	$tantargyak_kateg = "";
	$altantargyak_kateg = "";

	if ( !empty($hely) ) {

		$helysegek_kateg = array(
														'taxonomy' => 'helysegek_kateg',
														'field'    => 'slug',
														'terms'    => array( $hely ),
														'operator' => 'IN',
													);

		$term = get_term_by( 'slug', $hely, 'helysegek_kateg' );
		$term_children = get_term_children( $term, $term->taxonomy );

		foreach ( $term_children as $child_id ) {
			$term = get_term_by( 'id', $child_id, $term->taxonomy );

			$helysegek_kateg []= array(
															'taxonomy' => 'helysegek_kateg',
															'field'    => 'slug',
															'terms'    => array( $term->slug ),
															'operator' => 'IN',
														);
		}
	}
	if ( !empty($tantagy) ) {
		$tantargyak_kateg = array(
														'taxonomy' => 'tantargyak_kateg',
														'field'    => 'slug',
														'terms'    => array( $tantagy ),
														'operator' => 'IN',
													);
	}
	if ( !empty($altantagy) ) {
		$altantargyak_kateg = array(
														'taxonomy' => 'tantargyak_kateg',
														'field'    => 'slug',
														'terms'    => array( $altantagy ),
														'operator' => 'IN',
													);
	}

	$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																			'fields' => 'ids',
																			'posts_per_page' => -1,
																			'post_status' => 'publish',
																			'tax_query' => array(
																				'relation' => 'AND',
																				$helysegek_kateg,
																				$tantargyak_kateg,
																				$altantargyak_kateg,
																			),
																		));
	$tanarok_cpt_posts_array = $tanarok_cpt->posts;

	$tanarok_cpt_posts_arra_temp = array();
	foreach ($tanarok_cpt_posts_array as $key => $post_id) {
		$user_id = get_post_field('post_author', $post_id);
		//if ( ervenyes_megrendelesek_lekerese($user_id) !== FALSE ) {
			$tanarok_cpt_posts_arra_temp []= $post_id;
		//}
	}
	$tanarok_cpt_posts_array = $tanarok_cpt_posts_arra_temp;
	unset($tanarok_cpt_posts_arra_temp);

	return count($tanarok_cpt_posts_array);
}

function mt_file_uploader($file_inp_name) {
	if ( empty($file_inp_name) ) { return ""; }
	$allowed_filetype = array( 'jpg', 'jpeg', 'png' );

	$files_raw = $_FILES[$file_inp_name];
	if ( empty($files_raw) ) { return ""; }

	if($files_raw['name'] != '') {

    	$filename = sanitize_file_name(substr(md5(rand(1, 99999)), 0, 5) .'-'. $files_raw['name']);
	    $tmp_name = $files_raw['tmp_name'];
	    $filesize = $files_raw['size'];

	    if (!empty($filename)) {
        $wp_filetype = wp_check_filetype( basename($filename), null );

				if ( $filesize > 10485760) { // max 10 MB
					return array('error_msg', "A feltölteni kívánt fájl túl nagy!");
				}

				if ( !empty($allowed_filetype) ) {
					if ( !in_array($wp_filetype['ext'], $allowed_filetype) ) {
						return array('error_msg', "A megadott fájltípus nem megfelelő. <br>Használható fájlformátumok: ". implode(', ', $allowed_filetype));
					}
				}

        $wp_upload_dir = wp_upload_dir();

        // Move the uploaded file into the WordPress uploads directory
        move_uploaded_file( $tmp_name, $wp_upload_dir['path']  . '/' . $filename );

        $attachment = array(
            'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => $filename,
            'post_content' => '',
            'post_status' => 'inherit'
        );

        $filename = $wp_upload_dir['path']  . '/' . $filename;

        $attach_id = wp_insert_attachment( $attachment, $filename);
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );

        wp_update_attachment_metadata( $attach_id, array_merge($attach_data, $attachment) );

				return intval($attach_id);

    	} else {
				return array('error_msg', "A feltöltött fájl hibás.");
	    }
	} else {
		return array('error_msg', "A feltöltött fájl hibás.");
	}
}

function get_the_excerpt_by_id($post_id = "", $text_length = 55) {
	global $post;
	$save_post = $post;
	$post = get_post($post_id);
	$output = $post->post_excerpt;
	$post = $save_post;

	if($output == ''){
	    $output = get_the_content_by_id($post_id);
	    $output = strip_shortcodes( $output );
	    $output = apply_filters('the_content', $output);
	    $output = str_replace(']]>', ']]>', $output);
	    $output = strip_tags($output);
	    $output = nl2br($output);
	    $excerpt_length = apply_filters('excerpt_length', $text_length);
	    $words = explode(' ', $output, $excerpt_length + 1);
	    if (count($words) > $excerpt_length) {
	        array_pop($words);
	        array_push($words, '...');
	        $output = implode(' ', $words);
	    }
	}
	return $output;
}

function get_the_content_by_id($post_id = "", $format = false) {
	$page_data = get_page($post_id);
	if ( $page_data ) {

	    if ( $format === true ) {
	      return apply_filters('the_content',$page_data->post_content);
	    } else {
	        return $page_data->post_content;
	    }
	}
	return false;
}

function get_current_user_role() {
	if ( is_user_logged_in() ) {
		$roles = array();
		$user_obj = get_user_by('id', get_current_user_id());
		if ( isset($user_obj->roles) ) {
			foreach ($user_obj->roles as $key => $role) {
				$roles []= $role;
			}
		}
		return $roles[0];
	}
	return "";
};

function sort_terms_hierarchicaly(Array &$cats, Array &$into, $parentId = 0) {
  foreach ($cats as $i => $cat) {
    if ($cat->parent == $parentId) {
      $into[$cat->term_id] = $cat;
      unset($cats[$i]);
    }
  }

  foreach ($into as $topCat) {
    $topCat->children = array();
    sort_terms_hierarchicaly($cats, $topCat->children, $topCat->term_id);
  }
}

function jelenleg_bejelentkezett_roles_array() {
	if ( is_user_logged_in() ) {
		$user_id = get_current_user_id();
		$user_info = get_userdata($user_id);
		return $user_info->roles;
	}
	return FALSE;
}

function checkPassword($pwd) {
	$errors = array();

	if (strlen($pwd) < 8) {
    $errors[] = "A megadott jelszó túl rövid!";
	}

	if (!preg_match("#[0-9]+#", $pwd)) {
    $errors[] = "A jelszónak legalább egy számot kell tartalmaznia!";
	}

	if (!preg_match("#[a-zA-Z]+#", $pwd)) {
    $errors[] = "A jelszónak legalább egy betűt kell tartalmaznia!";
	}

	if ( empty($errors[0]) ) {
		return "";
	}
	return $errors[0];
}

/**/


/**************************/
/*         Sidebars       */
/**************************/



/**/


/**************************/
/*        Cron jobs       */
/**************************/

	// Hírdetések átvizsgálása

	if ( isset($_GET['set_expired_ads']) ) {

		$tanarok_cpt = new WP_Query(array( 	'post_type' => 'tanarok_cpt',
																				'fields' => 'ids',
																				'posts_per_page' => -1,
																				'post_status' => 'any',
																	));
		$tanarok_cpt_posts_array = $tanarok_cpt->posts;

		foreach ($tanarok_cpt_posts_array as $key => $post_id) {
			if ( intval($post_id) > 0 ) {
				$user_id = get_post_field('post_author', $post_id);

				if ( ervenyes_megrendelesek_lekerese($user_id) !== FALSE ) {
					wp_update_post( array('ID' => $post_id, 'post_status' => 'publish') );
				} else {
					wp_update_post( array('ID' => $post_id, 'post_status' => 'draft') );
				}
			}
		}
		die;
	}


/**/


/*******************/
/*      CPT        */
/*******************/

add_action( 'init', 'mt_custom_post_product' );
function mt_custom_post_product() {

	$labels = array(
	  'name'                  => 'Tanárok hirdetései',
	  'singular_name'         => 'Tanár hirdetése',
	  'add_new'               => 'Új hirdetés hozzáadása',
	  'add_new_item'          => 'Új hirdetés hozzáadása',
	  'edit_item'             => 'Tanári hirdetés szerkesztése',
	  'new_item'              => 'Új tanári hirdetés',
	  'view_item'             => 'Hirdetés megtekintése',
	  'search_items'          => 'Hirdetés keresése',
	  'not_found'             => 'Nincs ilyen tanári hirdetés',
	  'not_found_in_trash'    => 'Nincs hirdetés a lomtárban',
	  'parent_item_colon'     => ''
	);
	$args = array(
	  'labels' => $labels,
	  'public' => true,
	  'show_in_nav_menus' => true,
	  'exclude_from_search' => false,
	  'rewrite' => array( 'slug' => 'tanar' ),
	  'has_archive' => true,
	  'menu_icon' => 'dashicons-admin-users',
	  'supports' => array('title', 'editor', 'content', 'comments', 'excerpt', 'thumbnail', 'author', 'custom-fields', 'page-attributes' )
	);
	register_post_type('tanarok_cpt', $args);


	$labels = array(
	  'name'                  => 'Megrendelések',
	  'singular_name'         => 'Megrendelés',
	  'add_new'               => 'Új megrendelés hozzáadása',
	  'add_new_item'          => 'Új megrendelés hozzáadása',
	  'edit_item'             => 'Megrendelés szerkesztése',
	  'new_item'              => 'Új megrendelés',
	  'view_item'             => 'Megrendelés megtekintése',
	  'search_items'          => 'Megrendelés keresése',
	  'not_found'             => 'Nincs ilyen megrendelés',
	  'not_found_in_trash'    => 'Nincs megrendelés a lomtárban',
	  'parent_item_colon'     => ''
	);
	$args = array(
	  'labels' => $labels,
	  'public' => true,
	  'show_in_nav_menus' => true,
	  'exclude_from_search' => false,
	  'rewrite' => array( 'slug' => 'megrendelesek' ),
	  'has_archive' => true,
	  'menu_icon' => 'dashicons-cart',
	  'supports' => array('title', 'author', 'custom-fields', 'page-attributes' )
	);
	register_post_type('megrendelesek_cpt', $args);


	$labels = array(
		'name'                  => 'Előfizetési lehetőségek',
		'singular_name'         => 'Előfizetési lehetőség',
		'add_new'               => 'Új előfizetés hozzáadása',
		'add_new_item'          => 'Új előfizetés hozzáadása',
		'edit_item'             => 'Előfizetések szerkesztése',
		'new_item'              => 'Új előfizetés',
		'view_item'             => 'Előfizetés megtekintése',
		'search_items'          => 'Előfizetés keresése',
		'not_found'             => 'Nincs ilyen előfizetés',
		'not_found_in_trash'    => 'Nincs előfizetés a lomtárban',
		'parent_item_colon'     => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_in_nav_menus' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'menu_icon' => 'dashicons-cart',
		'supports' => array( 'title', 'author' )
	);
	register_post_type('elofiz_opciok_cpt', $args);


	$labels = array(
		'name'                  => 'Üzenetek (Tanárok és diákok között)',
		'singular_name'         => 'Üzenet',
		'add_new'               => 'Új üzenet hozzáadása',
		'add_new_item'          => 'Új üzenet hozzáadása',
		'edit_item'             => 'Üzenet szerkesztése',
		'new_item'              => 'Új üzenet',
		'view_item'             => 'Üzenet megtekintése',
		'search_items'          => 'Üzenet keresése',
		'not_found'             => 'Nincs ilyen üzenet',
		'not_found_in_trash'    => 'Nincs üzenet a lomtárban',
		'parent_item_colon'     => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_in_nav_menus' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'menu_icon' => 'dashicons-email-alt',
		'supports' => array( 'title', 'content', 'author', 'custom-fields' )
	);
	register_post_type('uzenetek_cpt', $args);

}


/**/


/**************************/
/*      Taxonomies        */
/**************************/

add_action( 'init', 'mt_build_taxonomies', 0 );
function mt_build_taxonomies(){

	register_taxonomy(
	    'helysegek_kateg',
	    'tanarok_cpt',
	    array(
        'hierarchical' => true,
        'label' => 'Helységek',
        'show_admin_column'  => true
	    )
	);

	register_taxonomy(
	    'tantargyak_kateg',
	    'tanarok_cpt',
	    array(
        'hierarchical' => true,
        'label' => 'Tantárgyak',
        'show_admin_column'  => true
	    )
	);

}

/**/


/*******************************/
/*          ADD ACTION         */
/*******************************/

add_action('init', 'mt_re_rewrite_rules');
function mt_re_rewrite_rules() {
	global $wp_rewrite;
	$wp_rewrite->pagination_base = 'oldal';
	$wp_rewrite->flush_rules();
}

// Magántanárok terület szerint widget linkek rewrite rule generálása
add_action( 'generate_rewrite_rules', 'mt_add_rewrite_rules' );
function mt_add_rewrite_rules() {
	global $wp_rewrite;

	$helysegek_kateg_terms_array = array();
	$args = array(
								'taxonomy' => 'helysegek_kateg',
								'hide_empty' => 0
							);
	$terms = get_terms( $args );
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		foreach ( $terms as $term ) {
			$helysegek_kateg_terms_array []= $term->slug;
		}
	}

	$tantargyak_kateg_terms_array = array();
	$args = array(
								'taxonomy' => 'tantargyak_kateg',
								'hide_empty' => 0
							);
	$terms = get_terms( $args );
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		foreach ( $terms as $term ) {
			$tantargyak_kateg_terms_array []= $term->slug;
		}
	}

	$new_rules = array();
	foreach ($tantargyak_kateg_terms_array as $tkey => $tantargy_term_slug) {
		foreach ($helysegek_kateg_terms_array as $hkey => $helyseg_term_slug) {
			$new_rules[$tantargy_term_slug.'/'. $helyseg_term_slug .'/?$']= 'index.php?post_type=tanarok_cpt&helysegek_kateg='. $helyseg_term_slug .'&tantargyak_kateg='. $tantargy_term_slug;
			$new_rules[$tantargy_term_slug.'/'. $helyseg_term_slug .'/oldal/(\d+)/?$']= 'index.php?post_type=tanarok_cpt&helysegek_kateg='. $helyseg_term_slug .'&tantargyak_kateg='. $tantargy_term_slug .'&paged=' . $wp_rewrite->preg_index(1);
		}
	}

	foreach ($helysegek_kateg_terms_array as $hkey => $helyseg_term_slug) {
		$new_rules['magantanar/'. $helyseg_term_slug .'/?$']= 'index.php?post_type=tanarok_cpt&helysegek_kateg='. $helyseg_term_slug;
		$new_rules['magantanar/'. $helyseg_term_slug .'/oldal/(\d+)/?$']= 'index.php?post_type=tanarok_cpt&helysegek_kateg='. $helyseg_term_slug .'&paged=' . $wp_rewrite->preg_index(1);
	}

	$wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
}


add_action( 'show_user_profile', 'mt_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'mt_show_extra_profile_fields' );
function mt_show_extra_profile_fields( $user ) {
	$all_meta_for_user = get_user_meta( $user->ID );

	?>
	<h3>Extra profil információ</h3>
	<table class="form-table show_extra_profile_fields">
		<tr>
			<th>Account aktiválva</th>
			<td>
				<input type="checkbox" id="activated_account" name="activated_account" value="1" <?php checked(1, get_user_meta($user->ID, 'activated_account', true), true);?> >
				<label for="activated_account">Igen</label>
			</td>
		</tr>
	</table>
	<?php
}

add_action( 'personal_options_update', 'mt_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'mt_save_extra_profile_fields' );
function mt_save_extra_profile_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_usermeta( $user_id, 'activated_account', (int) $_POST['activated_account'] );
}


add_action( 'wp_footer', 'mt_wp_footer' );
function mt_wp_footer() {

	// Get (teacher || student) new messages count
	if ( is_user_logged_in() ) {
		if ( in_array('tanar_role', jelenleg_bejelentkezett_roles_array()) || in_array('subscriber', jelenleg_bejelentkezett_roles_array()) ) {
			$uzenetek_cpt = new WP_Query(array( 'post_type' => 'uzenetek_cpt',
			                                    'fields' => 'ids',
			                                    'posts_per_page' => -1,
																					'meta_query' => array(
																			        'relation' => 'AND',
																			        'viewed_meta' => array(
																		            'key' => 'viewed',
																		            'value' => 1,
																								'compare' => '!=',
																			        ),
																							'msg_to_meta' => array(
																		            'key' => 'msg_to_user_id',
																		            'value' => get_current_user_id(),
																								'compare' => '=',
																			        ),
																			    ),
																				));
			echo '<input type="hidden" value="'. intval($uzenetek_cpt->post_count) .'" id="new_messages_count">';
		}
	}

}

add_action( 'init', 'mt_MultiPostThumbnails' );
function mt_MultiPostThumbnails() {
	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => 'Négyzet alakú profilkép',
			'id' => 'square_profile_img',
			'post_type' => 'tanarok_cpt'
		) );
	}
}

/* add new image size */
add_action( 'after_setup_theme', 'mt_add_image_size_function', 11 );
function mt_add_image_size_function() {
	//add_image_size( '300x400', 300, 400, true );
}

add_action('after_setup_theme', 'mt_remove_admin_bar');
function mt_remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}

/**/


/********************************/
/*           FILTERS            */
/********************************/

add_filter( 'send_email_change_email', '__return_false' );


//add_filter( 'pre_get_posts', 'mt_taxonomy_archive' );
function mt_taxonomy_archive($query) {
	/*
	if ( is_post_type_archive('tanarok_cpt') ) {
		if ( $query->is_main_query() && $query->is_post_type_archive('tanarok_cpt') && !is_admin() ) {
			$query->set( 'posts_per_page', -1 );
		}
	}
	*/
	return $query;
}

add_action( 'edit_terms', 'mt_do_something_or_stop_update', 10, 2 );
function mt_do_something_or_stop_update( $term_id, $taxonomy ) {
	mt_add_rewrite_rules();
	flush_rewrite_rules();
}

/* PageNavi to Bootstrap nav */
add_filter( 'wp_pagenavi', 'mt_pagination', 10, 2 );
function mt_pagination($html) {
	$out = '';

	//wrap a's and span's in li's
	$out = str_replace("<div","",$html);
	$out = str_replace("class='wp-pagenavi'>","",$out);
	$out = str_replace("<a","<li><a",$out);
	$out = str_replace("</a>","</a></li>",$out);
	$out = str_replace("<span","<li><span",$out);
	$out = str_replace("</span>","</span></li>",$out);
	$out = str_replace("</div>","",$out);

	return '<nav class="pagination">
          	<ul class="pagination">'.$out.'</ul>
	        </nav>';
}

add_filter('user_contactmethods', 'mt_user_contactmethods');
function mt_user_contactmethods($user_contactmethods){
	$user_contactmethods['phone'] = 'Telefonszám';

	return $user_contactmethods;
}

add_filter( 'the_author', 'mt_guest_author_name' );
add_filter( 'get_the_author_display_name', 'mt_guest_author_name' );
function mt_guest_author_name( $name ) {
	if ( !is_admin() ) { return ""; }
	return $name;
}

/* add new image size name to WP admin */
/*add_filter( 'image_size_names_choose', 'mt_image_size_names_choose' );
function mt_image_size_names_choose( $sizes ) {
	return array_merge( $sizes, array(
		'300x400' => 'profile-img',
	) );
}*/

// Add specific CSS class by filter
add_filter( 'body_class', 'mt_body_class_function', 1 );
function mt_body_class_function( $classes ) {
	global $wp_query;

	if ( is_user_logged_in() ) {
		$classes[] = get_current_user_role();
	}

	return $classes;
}


add_filter('sanitize_file_name', 'mt_sanitize_media_filename_on_upload', 10);
function mt_sanitize_media_filename_on_upload($filename) {
  $ext = end(explode('.',$filename));

  // Replace all weird characters
  $sanitized = sanitize_title(substr($filename, 0, -(strlen($ext)+1)));
  // Replace dots inside filename
  $sanitized = str_replace('.','-', $sanitized);

  $upload_dir = wp_upload_dir();
  $upload_path = $upload_dir['path'];

  if ( file_exists( $upload_path .'/'. $sanitized.'.'.$ext ) ) {
      $sanitized_array = array();

      for ($i=1; $i < 200; $i++) {
          if ( !file_exists( $upload_path .'/'. $sanitized .'-'. $i .'.'.$ext ) ) {
              $sanitized_array []= $sanitized .'-'. $i;
          }
      }

      if (!empty($sanitized_array[0])) {
          $sanitized = $sanitized_array[0];
          unset($sanitized_array);
      }
  }

  return strtolower($sanitized.'.'.$ext);
}

/**/


/*********************/
/*      Roles        */
/*********************/

add_action ('init', 'mt_roles_management');
function mt_roles_management() {
	add_role( 'tanar_role', 'Tanár', array( 'read' => true, 'level_0' => true ) );
}

/**/


/*********************************/
/*          WP DASHBOARD         */
/*********************************/

add_action( 'admin_init', 'mt_remove_dashboard_meta' );
function mt_remove_dashboard_meta() {
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'wordfence_activity_report_widget', 'dashboard', 'normal');
}

add_action( 'wp_dashboard_setup', 'mt_add_dashboard_widgets' );
function mt_add_dashboard_widgets() {

	wp_add_dashboard_widget(
		'nem_jovahagyott_megrendelesek_lista',         // Widget slug.
		'Nem jóváhagyott megrendelések',         // Title.
		'nem_jovahagyott_megrendelesek_lista_widget_function' // Display function.
	);
}

/**/


/*******************************/
/*          AJAX               */
/*******************************/

// Add new Hit
add_action( 'wp_ajax_add_user_hits', 'mt_add_user_hits' );
add_action( 'wp_ajax_nopriv_add_user_hits', 'mt_add_user_hits' );
function mt_add_user_hits() {
	$post_data = $_POST['post_data'];
	$post_id = "";
	session_start();

	if ( !isset($_SESSION['hit']) ) {
		if ( !empty($post_data) ) {
			$post_data = explode(' ', $post_data);

			foreach ($post_data as $key => $value) {
				if (strpos($value, 'postid-') !== false) {
					$post_id = (int) str_replace('postid-', '', $value);
					break;
				}
			}

			if ( ($post_id > 0) && (get_post_type($post_id) == 'tanarok_cpt') ) {
				$hits = (int) get_post_meta( $post_id, 'hits', true );
				update_post_meta( $post_id, 'hits', $hits + 1 );
				$_SESSION['hit'] = 1;
			}
		}
	}

	echo json_encode(array('html' => $post_id, 'hash' => $_POST['hash']));
	exit;
}

// Add rate values

add_action( 'wp_ajax_rating_submission', 'mt_rating_submission' );
add_action( 'wp_ajax_nopriv_rating_submission', 'mt_rating_submission' );
function mt_rating_submission() {
	$current_user_id = get_current_user_id();
	$post_data = $_POST['post_data'];
	$score = (int) $_POST['score'];
	$comment = $_POST['comment'];
	$post_id = "";


	if ( intval($current_user_id) > 0 ) {  }
	else { exit; }

	$response_data = array(
													'error' => TRUE,
													'msg' => "Hiba történt a leadás közben",
													'hash' => $_POST['hash'],
												);

	if ( !empty($post_data) ) {
		$post_data = explode(' ', $post_data);
		foreach ($post_data as $key => $value) {
			if (strpos($value, 'postid-') !== false) {
				$post_id = (int) str_replace('postid-', '', $value);
				break;
			}
		}
	}


	if ( 	in_array('subscriber', jelenleg_bejelentkezett_roles_array()) &&
				($post_id > 0) && ($score >= 0) && ($score < 6) ) {


		// Check: már értékelt -e a user

		$args = array(
			'number' => 1,
			'post_id' => $post_id,
			'user_id' => $current_user_id,
			'count' => true,
		);
		$user_comment_count = get_comments( $args );

		if ( intval($user_comment_count) > 0 ) {
			$response_data = array(
															'error' => TRUE,
															'msg' => "Már leadott egy értékelést, köszönjük!",
															'hash' => $_POST['hash'],
														);
			echo json_encode($response_data);
			exit;
		}

		// ---

		$data = array(
			'comment_post_ID' => $post_id,
			'comment_content' => $comment,
			'user_id' => $current_user_id,
			'comment_date' => current_time('mysql'),
			'comment_approved' => 1,
		);
		$comment_id = wp_insert_comment($data);

		if ( $comment_id > 0 ) {
			update_comment_meta( $comment_id, 'user_score', $score );

			$response_data = array(
															'error' => FALSE,
															'msg' => "Köszönjük az értékelést!",
															'hash' => $_POST['hash'],
														);


			// Update post rate value

			$args = array(
				'post_id' => $post_id,
				'fields' => 'ids',
			);
			$user_comments = get_comments( $args );

			$rates_avg = array();
			if ( !empty($user_comments) ) {
				foreach ($user_comments as $key => $comment_id) {
					$score = get_comment_meta($comment_id, 'user_score', true);
					if ( !empty($score) ) {
						$rates_avg []= (int) $score;
					}
				}
			}
			$rates_avg = array_sum($rates_avg) / count($rates_avg);
			update_post_meta( $post_id, 'rating_current', $rates_avg );
			// ---
		}
	}

	echo json_encode($response_data);
	exit;
}


// Send message to the teacher ( only teacher single page )

add_action( 'wp_ajax_send_message_to_the_teacher', 'mt_send_message_to_the_teacher' );
add_action( 'wp_ajax_nopriv_send_message_to_the_teacher', 'mt_send_message_to_the_teacher' );
function mt_send_message_to_the_teacher() {
	$current_user_id = get_current_user_id(); // <-- Diák user ID
	$post_data = $_POST['post_data'];
	$subject = $_POST['subject'];
	$comment = $_POST['comment'];
	$post_id = ""; // <-- Tanár hirdetés ID
	$teacher_user_id = "";

	if ( intval($current_user_id) > 0 ) {  }
	else { exit; }

	$response_data = array(
													'error' => TRUE,
													'msg' => "Nem sikerült elküldeni az üzenetet!",
													'hash' => $_POST['hash'],
												);

	if ( !empty($post_data) ) {
		$post_data = explode(' ', $post_data);
		foreach ($post_data as $key => $value) {
			if (strpos($value, 'postid-') !== false) {
				$post_id = (int) str_replace('postid-', '', $value);
				$teacher_user_id = (int) get_post_field('post_author', $post_id);
				break;
			}
		}
	}


	if ( 	in_array('subscriber', jelenleg_bejelentkezett_roles_array()) &&
				($post_id > 0) && !empty($comment) && (intval($teacher_user_id) > 0) ) {

		$teacher_obj = get_user_by('id', $teacher_user_id);
		$student_obj = get_user_by('id', $current_user_id);

		$my_post = array(
			'post_title'    => $student_obj->display_name .' ('. $student_obj->user_email .') → '. $teacher_obj->display_name .' ('. $teacher_obj->user_email .')',
			'post_content'  => esc_attr(stripslashes($comment)),
			'post_status'   => 'publish',
			'post_author'   => $current_user_id,
			'post_type'			=> 'uzenetek_cpt',
			'post_date'			=> current_time('mysql'),
			'meta_input'		=> array(
																'msg_to_user_id' => $teacher_user_id,
																'subject' => esc_attr(stripslashes($subject)),
																'viewed' => 0,
															),
		);
		wp_insert_post( $my_post );

		$response_data = array(
														'error' => FALSE,
														'msg' => "Üzenetét sikeresen elküldtük!",
														'hash' => $_POST['hash'],
													);
	}

	echo json_encode($response_data);
	exit;
}


// Send message on

add_action( 'wp_ajax_send_message', 'mt_send_message' );
add_action( 'wp_ajax_nopriv_send_message', 'mt_send_message' );
function mt_send_message() {
	$sender_user_id = get_current_user_id();
	$recipient_user_id = 0;
	$msg_post_id = (int) $_POST['msg_post_id'];
	$subject = $_POST['subject'];
	$comment = $_POST['comment'];

	$response_data = array(
													'error' => TRUE,
													'msg' => "Nem sikerült elküldeni az üzenetet!",
													'hash' => $_POST['hash'],
												);

	if ( intval($sender_user_id) > 0 ) {  }
	else { exit; }


	// Validation for reply
	if ( $msg_post_id > 0 ) {
		if ( get_post_type($msg_post_id) == 'uzenetek_cpt' ) {
			if ( $sender_user_id == get_post_meta($msg_post_id, 'msg_to_user_id', true) ) {
				$recipient_user_id = (int) get_post_field('post_author', $msg_post_id);
			}
		}
	}


	if ( ($recipient_user_id > 0) && ($msg_post_id > 0) && !empty($comment) ) {

		$sender_user_obj = get_user_by('id', $sender_user_id);
		$recipient_user_obj = get_user_by('id', $recipient_user_id);

		$my_post = array(
			'post_title'    => $recipient_user_obj->display_name .' ('. $recipient_user_obj->user_email .') → '. $sender_user_obj->display_name .' ('. $sender_user_obj->user_email .')',
			'post_content'  => esc_attr(stripslashes($comment)),
			'post_status'   => 'publish',
			'post_author'   => $sender_user_obj->ID,
			'post_type'			=> 'uzenetek_cpt',
			'post_date'			=> current_time('mysql'),
			'meta_input'		=> array(
																'msg_to_user_id' => $recipient_user_obj->ID,
																'subject' => esc_attr(stripslashes($subject)),
																'viewed' => 0,
															),
		);
		wp_insert_post( $my_post );

		$response_data = array(
														'error' => FALSE,
														'msg' => "Üzenetét sikeresen elküldtük!",
														'hash' => $_POST['hash'],
													);
	}

	echo json_encode($response_data);
	exit;
}

/**/
