<?php get_header();
global $wp_query;

$helysegek_kateg = $wp_query->query['helysegek_kateg'];
$tantargyak_kateg = $wp_query->query['tantargyak_kateg'];

$page_title = "";
if ( !empty($helysegek_kateg) && !empty($tantargyak_kateg) ) {
	$helysegek_kateg_obj = get_term_by('slug', $helysegek_kateg, 'helysegek_kateg');
	$tantargyak_kateg_obj = get_term_by('slug', $tantargyak_kateg, 'tantargyak_kateg');

	$page_title = $tantargyak_kateg_obj->name .' / '. $helysegek_kateg_obj->name;

} else if ( !empty($helysegek_kateg) && empty($tantargyak_kateg) ) {

	$helysegek_kateg_obj = get_term_by('slug', $helysegek_kateg, 'helysegek_kateg');
	$page_title = $tantargyak_kateg_obj->name;

} else if ( !empty($tantargyak_kateg) && empty($helysegek_kateg) ) {

	$tantargyak_kateg_obj = get_term_by('slug', $tantargyak_kateg, 'tantargyak_kateg');
	$page_title = $tantargyak_kateg_obj->name;
}
?>

<div class="page_title_ctn boxed_t_left">
  <div class="wrapper">
     <h1><?php echo $page_title; ?></h1>
     <div id="breadcrumbs">
        <div id="crumbs"><a href="<?php echo get_home_url(); ?>">Főoldal</a></div>
     </div>
  </div>
</div>

<div class="section def_section">
  <div class="wrapper section_wrapper">
     <div id="posts"  class="right_posts">
			<?php
			$posts_array = array();
			global $wpdb, $wp_query;

			if (have_posts()) {
				while (have_posts()) { the_post();
					$posts_array []= get_the_ID();
	 			}
	 		}

			echo '<div class="found_posts">'.
							'<br><h6>Találatok száma: '. intval($wp_query->found_posts) .'</h6>'.
							'<div class="clearfix"></div>'.
						'</div>';
			echo do_shortcode('[tanarok_lista post_ids="'. serialize($posts_array) .'"]');
			echo 	'<div class="pagination_content">'.
							wp_pagenavi(array('echo' => false)).
						'</div>';
	 		?>
     </div>

		 <div id="sidebar" class="left_sb  <?php  if ( $sb_style == 'business_sb'){echo " business_sidebar";} ?>" >
       <?php if ( is_active_sidebar( 'thefox_mc_sidebar' ) ) { generated_dynamic_sidebar(); }?>
     </div>

     <div class="clearfix"></div>
  </div>
</div>

<?php get_footer(); ?>
